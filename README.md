# Freeform
 Web app dùng để trích xuất thông tin từ các loại form liên quan đến nghiệp vụ ngân hàng
 - [x] Giấy phép đăng ký kinh doanh
 - [x] Hợp đồng mua bán (Sale contract)
 - [x] Hóa đơn tài chính (Hóa đơn GTGT...)
 - [ ] Hóa đơn bán lẻ 
 - [ ] Hóa đơn điện nước 
 - [x] Giấy đăng ký mã số thuế 
 ...
 
## Hướng dẫn deploy trên local
 
 - Sửa đường dẫn trong *app/static/js/app.js*
 ```
const SERVER_URL = "http://0.0.0.0:9000/"
```
 - Chạy 
 ```
cd freeform
python manage.py
```

# Road maps
## Overall
- [x] Add PICK model to extract information by GCN
- [x] Refactor code to run with many

## OCR engine
- [x] Add rotation and extend for better text recognition
- [ ] Update text detection v2.0 (v1.1 can be easily to miss box)
- [ ] Check Paddle ocr to run by GPU

## String rule-base
- [ ] Find nearest sub-string in a string
- [ ] Separate box by empty space
- [x] Separate key-value by colon
- [ ] Separate line by different text style
