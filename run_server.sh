#!/bin/bash
sleep 5
export PYTHONENCODING=UTF-8
gunicorn -w $WORKER-t 600 -b 0.0.0.0:80 \
--access-logfile /workspace/log/gunicorn_access.log \
--error-logfile /workspace/log/gunicorn_error.log main_app/manage:app