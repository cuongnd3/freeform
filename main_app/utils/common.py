import math, os, cv2, time
import numpy as np
import ast, shutil, json

type_map = {1: 'other',
            2: 'contract_no',
            3: 'exporter_name',
            4: 'exporter_add',
            5: 'importer_name',
            6: 'payment_method',
            7: 'ben_bank_name',
            8: 'ben_add',
            9: 'ben_name',
            10: 'ben_acc',
            11: 'swift_code'}
color_map = {2: (0, 255, 0),
             3: (255, 0, 0),
             4: (0, 0, 255),
             5: (0, 255, 255),
             6: (120, 0, 0),
             7: (0, 120, 0),
             8: (120, 120, 0),
             9: (120, 0, 120),
             10: (0, 120, 255)}
inv_type_map = {v: k for k, v in type_map.items()}


def get_list_file_in_folder(dir, ext=['jpg', 'png', 'JPG', 'PNG']):
    included_extensions = ext
    file_names = [fn for fn in os.listdir(dir)
                  if any(fn.endswith(ext) for ext in included_extensions)]
    return file_names


def get_list_file_in_dir_and_subdirs(folder, ext=['jpg', 'png', 'JPG', 'PNG']):
    file_names = []
    for path, subdirs, files in os.walk(folder):
        for name in files:
            extension = os.path.splitext(name)[1].replace('.', '')
            if extension in ext:
                file_names.append(os.path.join(path, name).replace(folder, '')[1:])
                # print(os.path.join(path, name).replace(folder,'')[1:])
    return file_names


def get_list_dir_in_folder(dir):
    sub_dir = [o for o in os.listdir(dir) if os.path.isdir(os.path.join(dir, o))]
    return sub_dir


def euclidean_distance(pt1, pt2):
    return math.sqrt((pt1[0] - pt2[0]) * (pt1[0] - pt2[0]) + (pt1[1] - pt2[1]) * (pt1[1] - pt2[1]))


def get_first(e):
    return e[0]


def parse_bbox_and_trans_to_text_box(anno):
    '''
    Parse from annotation of PICK to text_box
    :param anno:
    :return:
    '''
    anno = anno.rstrip('\n')
    idx = -1
    for i in range(0, 9):
        idx = anno.find(',', idx + 1)

    first_comma_idx = anno.find(',')
    coordinates = anno[first_comma_idx + 1:idx]
    val_and_type = anno[idx + 1:]
    last_comma_idx = val_and_type.rfind(',')
    type_str = val_and_type[last_comma_idx + 1:]
    val = val_and_type[:last_comma_idx]

    pol = text_box(coordinates, type=type_str, value=val)
    return pol


def parse_icdar_to_text_box(anno):
    '''
    Parse from annotation of PICK to text_box
    :param anno:
    :return:
    '''
    anno = anno.rstrip('\n')
    idx = -1
    for i in range(0, 8):
        idx = anno.find(',', idx + 1)

    coordinates = anno[:idx]
    val = anno[idx + 1:]

    pol = text_box(coordinates, value=val)
    return pol


def get_list_gt_text_box(row, add_key_to_value=False):
    '''

    :param row: get from csv file
    :return:
    '''
    list_gt_text_box = []
    boxes = ast.literal_eval(row[1])
    key, value = row[3].split('|||'), row[2].split('|||')
    num_box, score = row[4], row[5]
    if int(num_box) <= 0:
        return list_gt_text_box

    print('Num_box:', num_box, 'Score:', score)
    for idx, k in enumerate(key):
        print(idx, boxes[idx]['category_id'], k, ':', value[idx], boxes[idx]['segmentation'])
    index = {15: 0, 16: 0, 17: 0, 18: 0}

    for idx, box in enumerate(boxes):
        # print(box['segmentation'])
        coors = box['segmentation']
        total_box = 0
        for coor in coors:
            if len(coor) < 8:
                continue
            else:
                final_value = value[idx]
                if add_key_to_value and box['category_id'] in index.keys():
                    final_value += ',' + type_map[box['category_id']] + '_' + str(index[box['category_id']])
                pol = text_box(coor, type=box['category_id'], value=final_value)
                list_gt_text_box.append(pol)
                total_box += 1
        if add_key_to_value and box['category_id'] in index.keys():
            index[box['category_id']] += 1
        # print('total box', total_box)
    return list_gt_text_box


def get_list_icdar_text_box(icdar_path, ignore_kie_type=False, default_type=1):
    '''

    :param icdar_path: path of icdar txt file
    :return:
    '''
    list_icdar_text_box = []

    with open(icdar_path, 'r', encoding='utf-8') as f:
        anno_txt = f.readlines()

    for anno in anno_txt:
        anno = anno.rstrip('\n')

        idx = -1
        for i in range(0, 8):
            idx = anno.find(',', idx + 1)

        coordinates = anno[:idx]
        val = anno[idx + 1:]
        type = default_type
        if ignore_kie_type:
            last_comma_idx = val.rfind(',')
            type_str = val[last_comma_idx + 1:]
            val = val[:last_comma_idx]
            if type_str in inv_type_map.keys():
                type = inv_type_map[type_str]

        coors = [int(float(f)) for f in coordinates.split(',')]
        pol = text_box(coors, type=type, value=val)
        list_icdar_text_box.append(pol)
    return list_icdar_text_box


def split_idx_coor_val_type_from_pick_anno(anno):
    idx = -1
    for i in range(0, 9):
        idx = anno.find(',', idx + 1)
    first_comma_idx = anno.find(',')
    index = anno[:first_comma_idx]
    coordinates = anno[first_comma_idx + 1:idx]
    val = anno[idx + 1:].replace('\n', '')
    last_comma_idx = val.rfind(',')
    type_str = val[last_comma_idx + 1:]
    val = val[:last_comma_idx]
    return index, coordinates, val, type_str


def get_list_img_from_PICK_csv(PICK_csv_path):
    PICK_img_list = []
    with open(PICK_csv_path, 'r', encoding='utf-8') as f:
        train_list = f.readlines()
    for line in train_list:
        idx = -1
        for i in range(0, 2):
            idx = line.find(',', idx + 1)
        img_name = line[idx + 1:].replace('\n', '')
        PICK_img_list.append(img_name)
    return PICK_img_list

def IoU(text_box1, text_box2, debug=False):
    max_w, max_h = 0, 0
    for pts in text_box1.list_pts:
        if pts[0] > max_w:
            max_w = pts[0]
        if pts[1] > max_h:
            max_h = pts[1]
    for pts in text_box2.list_pts:
        if pts[0] > max_w:
            max_w = pts[0]
        if pts[1] > max_h:
            max_h = pts[1]

    first_bb_points = np.array(text_box1.list_pts).astype(np.int32).reshape(-1, 2)
    first_text_box_mask = np.zeros((max_h, max_w)).astype(np.int32)
    cv2.filltext_box(first_text_box_mask, [np.array(first_bb_points)], [255, 255, 255])

    second_bb_points = np.array(text_box2.list_pts).astype(np.int32).reshape(-1, 2)
    second_text_box_mask = np.zeros((max_h, max_w)).astype(np.int32)
    cv2.filltext_box(second_text_box_mask, [np.array(second_bb_points)], [255, 255, 255])

    intersection = np.logical_and(first_text_box_mask, second_text_box_mask)
    union = np.logical_or(first_text_box_mask, second_text_box_mask)
    iou_score = np.sum(intersection) / np.sum(union)

    if debug and iou_score > 0.1:
        print('IoU.', iou_score, ',max_w', max_w, ', max_h', max_h)
        first_mask = np.array(first_text_box_mask, dtype=np.uint8)
        cv2.imshow('1st', first_mask)
        second_mask = np.array(second_text_box_mask, dtype=np.uint8)
        cv2.imshow('2nd', second_mask)

        cv2.waitKey(0)
    return iou_score


def filter_data(src_dir, dst_dir, dst_anno=None):  # filter data in dst_dir by src_dir
    list_files = get_list_file_in_folder(src_dir)
    list_files2 = get_list_file_in_folder(dst_dir)
    for idx, f in enumerate(list_files2):
        if f in list_files:
            continue
        else:
            print(idx, 'filter file', f)
            os.remove(os.path.join(dst_dir, f))


def copy_files(upload_dir, dst_dir):
    list_dir = get_list_dir_in_folder(upload_dir)
    list_dir = sorted(list_dir)
    for dir in list_dir:
        print(dir)
        list_files = get_list_file_in_folder(os.path.join(upload_dir, dir), ext=['png', 'jpg', 'json', 'txt'])
        for file in list_files:
            print(file)
            src_file = os.path.join(upload_dir, dir, file)
            dst_file = os.path.join(dst_dir, dir.replace(' ', '') + '_' + file)
            shutil.copy(src_file, dst_file)


def get_file_name(img_dir):
    list_file = get_list_file_in_folder(img_dir)
    list_file = sorted(list_file)
    for idx, file in enumerate(list_file):
        # print(idx, file)
        l = len(file)
        # print(file[:l-6]+'.pdf')
        print(file)


def write_icdar_result(det_res, txts, icdar_path, default_val=None):
    res_txt = ''
    for idx, bbox in enumerate(det_res):
        value = txts[idx]
        if default_val is not None:
            value = default_val

        line = ','.join([str(bbox[0]),
                         str(bbox[1]),
                         str(bbox[2]),
                         str(bbox[3]),
                         str(bbox[4]),
                         str(bbox[5]),
                         str(bbox[6]),
                         str(bbox[7]),
                         value])
        res_txt += line + '\n'
    res_txt = res_txt.rstrip('\n')
    with open(icdar_path, mode='w', encoding='utf-8') as f:
        f.write(res_txt)


def write_sdmgr_result(img, img_idx, det_res, txts, sdmgr_img_path, map_txt):
    sdmgr_dicr = {'file_name': os.path.basename(sdmgr_img_path),
                  'height': img.shape[0],
                  'width': img.shape[1],
                  'annotations': []}
    for idx, bbox in enumerate(det_res):
        txt_no_space = txts[idx].replace(' ', '').replace(' ', '')
        if txt_no_space not in map_txt.keys():
            map_txt[txt_no_space] = txts[idx]

        mmocr_anno = {'box': bbox, 'text': txt_no_space, 'label': 0}
        sdmgr_dicr['annotations'].append(mmocr_anno)
    sdmgr_dicr_str = json.dumps(sdmgr_dicr, ensure_ascii=False)

    test_path = os.path.join(os.path.dirname(sdmgr_img_path), 'test.txt')
    if img_idx == 0:
        with open(test_path, mode='w', encoding='utf-8') as f:
            f.write(sdmgr_dicr_str)
    else:
        with open(test_path, mode='r', encoding='utf-8') as f:
            previous_anno = f.readlines()
        previous_anno.append(sdmgr_dicr_str)

        new_txt = ''
        for anno in previous_anno:
            anno = anno.rstrip('\n')
            new_txt += anno + '\n'
        new_txt = new_txt.rstrip('\n')

        with open(test_path, mode='w', encoding='utf-8') as f:
            f.write(new_txt)

    return


if __name__ == '__main__':
    first_pol = text_box('100,221,299,221,299,329,100,329')
    # second_pol = text_box('100,271,299,221,299,329,100,329')
    # print('iou', IoU(first_pol, second_pol, True))

    # print('angle', first_pol.check_box_angle())

    # src_dir='/data20.04/data/MC_OCR/output_results/EDA/train_imgs_filtered'
    # dst_dir='/data20.04/data/MC_OCR/output_results/text_classifier/train_pred_lines/viz_imgs'
    # dst_anno='/data20.04/data/MC_OCR/output_results/text_classifier/train_pred_lines/txt'
    # filter_data(src_dir=src_dir,
    #             dst_dir = dst_dir,
    #             dst_anno=dst_anno)
    # get_file_name('/home/duycuong/home_data/vvn/OCR_MAFC/SMS_1414-20210514T080621Z-001/imgs/clean')
