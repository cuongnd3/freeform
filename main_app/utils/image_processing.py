import cv2, os
import numpy as np
import math
from main_app.utils.common import euclidean_distance, get_list_file_in_folder


def resize_normalize(img, normalize_width=1654):
    w = img.shape[1]
    h = img.shape[0]
    resize_ratio = normalize_width / w
    normalize_height = round(h * resize_ratio)
    resize_img = cv2.resize(img, (normalize_width, normalize_height), interpolation=cv2.INTER_CUBIC)
    # cv2.imshow('resize img', resize_img)
    # cv2.waitKey(0)
    return resize_ratio, resize_img


def resize_max(img, max_len=1200):
    '''
    get longer edge and make sure that this edge is no longer than max_len
    :param img:
    :param max_len:
    :return:
    '''
    w = img.shape[1]
    h = img.shape[0]
    resize_ratio = max_len / max(w, h)
    res_height = round(h * resize_ratio)
    res_width = round(w * resize_ratio)
    resize_img = cv2.resize(img, (res_width, res_height), interpolation=cv2.INTER_CUBIC)
    return resize_ratio, resize_img


def circle_detection(img):
    img = cv2.resize(img, dsize=(0, 0), fx=0.3, fy=0.3)
    cimg = img.copy()
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    _, img = cv2.threshold(img, 200, 255, cv2.THRESH_BINARY)
    cv2.imshow('img circles', img)
    cv2.waitKey()
    circles = cv2.HoughCircles(img, cv2.HOUGH_GRADIENT, 25, 100)

    circles = np.uint16(np.around(circles))
    for i in circles[0, :]:
        # draw the outer circle
        cv2.circle(cimg, (i[0], i[1]), i[2], (0, 255, 0), 2)
        # draw the center of the circle
        cv2.circle(cimg, (i[0], i[1]), 2, (0, 0, 255), 3)

    cv2.namedWindow('detected circles', cv2.WINDOW_NORMAL)
    # cv2.resizeWindow('detected circles', 720, 1280)
    cv2.imshow('detected circles', cimg)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def transform_dir(src_dir, trans_matrix, dst_dir=None, suffix=''):
    list_file = get_list_file_in_folder(src_dir)
    list_file = sorted(list_file)
    for idx, file in enumerate(list_file):
        print(idx, file)
        img = cv2.imread(os.path.join(src_dir, file))
        basename, ext = file.split('.')
        transform_img(src_img=img,
                      trans_matrix=trans_matrix,
                      dst_file=os.path.join(dst_dir, basename + '_' + suffix + '.' + ext))


def transform_img(src_img, trans_matrix, dst_file=None, background_color=(255, 255, 255), debug=False):
    w = src_img.shape[1]
    h = src_img.shape[0]
    trans_img = cv2.warpAffine(src_img,
                               trans_matrix,
                               (w, h),
                               borderValue=background_color)
    if dst_file is not None:
        cv2.imwrite(dst_file, trans_img)

    if debug:
        ratio = 0.4
        src_img_res = cv2.resize(src_img, (int(src_img.shape[1] * ratio), int(src_img.shape[0] * ratio)))
        trans_img_res = cv2.resize(trans_img, (int(trans_img.shape[1] * ratio), int(trans_img.shape[0] * ratio)))
        cv2.imshow('raw', src_img_res)
        cv2.imshow('trans', trans_img_res)
        cv2.waitKey(0)


def test_transform_img():
    src_pts = [[413, 448], [1312, 696], [923, 1854]]
    dst_pts = [[398, 475], [1305, 692], [956, 1863]]
    dst_pts = [[487, 421], [1372, 716], [923, 1852]]
    dst_pts = [[422, 484], [1312, 764], [883, 1908]]
    dst_pts = [[468, 539], [1437, 788], [1041, 2037]]
    src_pts = np.asarray(src_pts, dtype=np.float32)
    dst_pts = np.asarray(dst_pts, dtype=np.float32)
    affine_trans = cv2.getAffineTransform(src_pts, dst_pts)

    # img = cv2.imread(
    #     '/home/duycuong/PycharmProjects/vvn/demo_read_document/demo_app/data/sale_contract/json/6-WINGWAY_p0.png')
    # transform_img(img, affine_trans, debug=True)
    src_dir = '/data_backup/cuongnd/Viettel_freeform/sale_contract/crawl_data_internet/imgs'
    dst_dir = '/data_backup/cuongnd/sale_contract_viettel/augmentation/trans_img4'
    transform_dir(src_dir=src_dir,
                  trans_matrix=affine_trans,
                  dst_dir=dst_dir,
                  suffix='4')


def transform_icdar_dir(icdar_dir, trans_matrix, dst_dir=None, suffix=''):
    list_file = get_list_file_in_folder(icdar_dir, ext=['.txt'])
    list_file = sorted(list_file)
    for idx, file in enumerate(list_file):
        print(idx, file)
        with open(os.path.join(icdar_dir, file), mode='r', encoding='utf-8') as f:
            list_anno = f.readlines()

        transform_txt = ''
        for anno in list_anno:
            idx = -1
            for i in range(0, 8):
                idx = anno.find(',', idx + 1)

            coordinates = anno[:idx]
            val = anno[idx + 1:]
            coors = [float(coor) for coor in coordinates.split(',')]
            list_coors = [[coors[0], coors[1]], [coors[2], coors[3]], [coors[4], coors[5]], [coors[6], coors[7]]]
            list_transform_coors = transform_point(list_coors, trans_matrix=trans_matrix)
            list_transform_coors.append(val)
            transform_anno = ','.join(list_transform_coors)
            transform_txt += transform_anno

        basename, ext = file.split('.')
        with open(os.path.join(dst_dir, basename + '_' + suffix + '.' + ext), mode='w', encoding='utf-8') as f:
            f.write(transform_txt)


def transform_point(list_pts, trans_matrix):
    src = np.array(list_pts)
    dst = cv2.transform(np.array([src]), trans_matrix)[0]

    # list_pts = []
    # for pts in dst:
    #     for axis in pts:
    #         list_pts.append(str(round(axis)))
    return dst.tolist()


def test_transform_point():
    src_pts = [[413, 448], [1312, 696], [923, 1854]]
    dst_pts = [[398, 475], [1305, 692], [956, 1863]]
    # dst_pts = [[487, 421], [1372, 716], [923, 1852]]
    # dst_pts = [[422, 484], [1312, 764], [883, 1908]]
    # dst_pts = [[468, 539], [1437, 788], [1041, 2037]]
    src_pts = np.asarray(src_pts, dtype=np.float32)
    dst_pts = np.asarray(dst_pts, dtype=np.float32)
    affine_trans = cv2.getAffineTransform(src_pts, dst_pts)

    icdar_dir = '/data_backup/cuongnd/Viettel_freeform/sale_contract/crawl_data_internet/icdar_entity'
    dst_dir = '/data_backup/cuongnd/Viettel_freeform/sale_contract/crawl_data_internet/augmentation'
    transform_icdar_dir(icdar_dir=icdar_dir,
                        trans_matrix=affine_trans,
                        dst_dir=dst_dir,
                        suffix='1')


def distance_from_pts_to_line(x0, y0, a, b, c):
    return abs(a * x0 + b * y0 + c) / math.sqrt(a * a + b * b)


def order_points(pts):
    rect = np.zeros((4, 2), dtype = "float32")
    s = pts.sum(axis = 1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]
    diff = np.diff(pts, axis = 1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]
    if euclidean_distance(rect[0],rect[1]) < euclidean_distance(rect[0],rect[3]) :
        tmp = rect[0].copy()
        rect[0] = rect[1]
        rect[1] = rect[2]
        rect[2] = rect[3]
        rect[3] = tmp
    return rect


def rotate_and_crop(img, points, debug=False, extend=True,
                    extend_x_ratio=1, extend_y_ratio=0.01,
                    min_extend_y=1, min_extend_x=2):
    '''

    :param img:
    :param points: list [4,2]
    :param debug:
    :param extend:
    :param extend_x_ratio:
    :param extend_y_ratio:
    :param min_extend_y:
    :param min_extend_x:
    :return:
    '''

    ### Warning: points must be sorted clockwise
    points = order_points(np.asarray(points))

    w = int(euclidean_distance(points[0],points[1]))
    h = int(euclidean_distance(points[1],points[2]))

    # get width and height of the detected rectangle

    if extend:
        ex = min_extend_x if (extend_x_ratio * h) < min_extend_x else (extend_x_ratio * h)
        ey = min_extend_y if (extend_y_ratio * h) < min_extend_y else (extend_y_ratio * h)
        ex = int(round(ex))
        ey = int(round(ey))
    else:
        ex, ey = 0, 0
    src_pts = points.astype("float32")
    dst_pts = np.array([
        [ex, ey],
        [w - 1 + ex, ey],
        [w - 1 + ex, h - 1 + ey],
        [ex, h - 1 + ey]
    ], dtype="float32")

    M = cv2.getPerspectiveTransform(src_pts, dst_pts)

    warped = cv2.warpPerspective(img, M, (w + 2 * ex, h + 2 * ey))
    # if warped.mean() < 145:
    #     warped = 255 - warped

    if debug:
        print('ex, ey', ex, ey)
        cv2.imshow('rotate and extend', warped)
        cv2.waitKey(0)
    return warped

def group_text_box_to_line(img, polys, txts, scores, slope_ths=0.5, ycenter_ths=0.6, height_ths=0.7, width_ths=1.0,
                           add_margin=0.05, vis=False, offset_y=None):
    """
    :param polys:
    :param slope_ths:
    :param ycenter_ths: ycenter nho -> khi merge hon
    :param height_ths: height_ths nho --> kho merge hon - so sanh chieu cao cua box
    chech lech giua 2 box < height_ths * height(box1) --> merge
    :param width_ths: distance of 2 box (in X axis) < width_ths * chracter with --> merge
    :param add_margin:
    :return: [line1, line2,...]
    linei = [box0, box1,...]
    box1 = [x1, y1, x2, y2, cy, h, text, score]
    """
    if len(txts) == 0:
        return [], img
    # poly top-left, top-right, low-right, low-left
    horizontal_list, free_list, combined_list, merged_list = [], [], [], []

    for i, poly in enumerate(polys):
        slope_up = (poly[3] - poly[1]) / np.maximum(10, (poly[2] - poly[0]))
        slope_down = (poly[5] - poly[7]) / np.maximum(10, (poly[4] - poly[6]))
        if max(abs(slope_up), abs(slope_down)) < slope_ths:
            x_max = max([poly[0], poly[2], poly[4], poly[6]])
            x_min = min([poly[0], poly[2], poly[4], poly[6]])
            y_max = max([poly[1], poly[3], poly[5], poly[7]])
            y_min = min([poly[1], poly[3], poly[5], poly[7]])
            horizontal_list.append(
                [x_min, x_max, y_min, y_max, 0.5 * (y_min + y_max), y_max - y_min, txts[i], scores[i]])
        else:
            height = np.linalg.norm([poly[6] - poly[0], poly[7] - poly[1]])
            margin = int(1.44 * add_margin * height)

            theta13 = abs(np.arctan((poly[1] - poly[5]) / np.maximum(10, (poly[0] - poly[4]))))
            theta24 = abs(np.arctan((poly[3] - poly[7]) / np.maximum(10, (poly[2] - poly[6]))))
            # do I need to clip minimum, maximum value here?
            x1 = poly[0] - np.cos(theta13) * margin
            y1 = poly[1] - np.sin(theta13) * margin
            x2 = poly[2] + np.cos(theta24) * margin
            y2 = poly[3] - np.sin(theta24) * margin
            x3 = poly[4] + np.cos(theta13) * margin
            y3 = poly[5] + np.sin(theta13) * margin
            x4 = poly[6] - np.cos(theta24) * margin
            y4 = poly[7] + np.sin(theta24) * margin

            free_list.append([[x1, y1], [x2, y2], [x3, y3], [x4, y4]])
    horizontal_list = sorted(horizontal_list, key=lambda item: item[4])
    if len(horizontal_list) > 0:
        character_width = min([poly[1] - poly[0] for poly in horizontal_list if poly[1] - poly[0] != 0])
    else:
        character_width = 0
    # combine box by "x" location and height

    # combine box in same line
    new_box = []
    for poly in horizontal_list:
        if len(new_box) == 0:
            b_height = [poly[5]]
            b_ycenter = [poly[4]]
            new_box.append(poly)
        else:
            # comparable height and comparable y_center level up to ths*height
            if (abs(np.mean(b_height) - poly[5]) < height_ths * np.mean(b_height)) and (
                    abs(np.mean(b_ycenter) - poly[4]) < ycenter_ths * np.mean(b_height)):
                b_height.append(poly[5])
                b_ycenter.append(poly[4])
                new_box.append(poly)
            else:
                b_height = [poly[5]]
                b_ycenter = [poly[4]]
                combined_list.append(new_box)
                if vis:
                    cbimg = img.copy()
                    for b in new_box:
                        cv2.rectangle(cbimg, (b[0], b[2]), (b[1], b[3]), (0, 255, 0), 3)
                    cv2.namedWindow('cbimg', cv2.WINDOW_NORMAL)
                    cv2.resizeWindow('cbimg', 1280, 720)
                    cv2.imshow('cbimg', cbimg)
                    cv2.waitKey(1)
                new_box = [poly]
    combined_list.append(new_box)

    if vis:
        colors = [(0, 255, 0), (0, 0, 255)]
        for i, list_box in enumerate(combined_list):
            x1 = min([box[0] for box in list_box])
            x2 = max([box[1] for box in list_box])
            y1 = min([box[2] for box in list_box])
            y2 = max([box[3] for box in list_box])
            cv2.rectangle(img, (x1, y1), (x2, y2), colors[i % 2], 2)

        cv2.namedWindow('line', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('line', 1280, 720)
        cv2.imshow('line', img)
        cv2.waitKey(1)
    # return box in format box = [x1, y1, x2, y2, cy, h, text, score] from [x1, x2, y1, y2, cy, h, text, score]
    for i in range(len(combined_list)):
        for j in range(len(combined_list[i])):
            combined_list[i][j] = [combined_list[i][j][0], combined_list[i][j][2], combined_list[i][j][1],
                                   combined_list[i][j][3]] + combined_list[i][j][4:]

    combined_list = sorted(combined_list, key=lambda k: k[0][4])
    for i, line in enumerate(combined_list):
        combined_list[i] = sorted(line, key=lambda k: k[0])

    # add offset by number of page
    if offset_y is not None:
        for i, line in enumerate(combined_list):
            for j, pol in enumerate(line):
                pol[1] += offset_y
                pol[3] += offset_y
                pol[4] += offset_y

    return combined_list, img


if __name__ == '__main__':

    box =np.asarray([1,2,3,4,5,6,7,8])
    res = cv2.minAreaRect(np.array(box, dtype=np.float32).reshape(4, 2))

    import math

    # angle = math.atan2(0, 0) * 57.296

    angle = np.rad2deg(np.arctan2(-2, 1))

    angle = np.rad2deg(2)
    np.deg2rad
    test_transform_point()
