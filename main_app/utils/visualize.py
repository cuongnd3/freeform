from matplotlib import pyplot as plt
import matplotlib.patches as patches
import cv2, os, random, math
from main_app.utils.common import get_list_file_in_folder, type_map, split_idx_coor_val_type_from_pick_anno
import numpy as np
from PIL import Image, ImageDraw, ImageFont
color_map = {1: 'r',
             2: 'green',
             3: 'blue',
             4: 'm',
             5: 'cyan',
             6: 'green',
             7: 'blue',
             8: 'm',
             9: 'cyan',
             10: 'blue',
             11: 'r'
             }
txt_color_map = {1: 'b',
                 2: 'r',
                 3: 'blue',
                 4: 'm',
                 5: 'cyan',
                 6: 'green',
                 7: 'blue',
                 8: 'm',
                 9: 'cyan',
                 10: 'blue',
                 11: 'r'
                 }
inv_type_map = {v: k for k, v in type_map.items()}

PALETTE = [(60,80,20),(255, 0, 0),(0, 255, 0),(0, 0, 255),(128, 64, 128), (244, 35, 232), (255, 165, 0), (0,255,255),
               (255, 255, 0), (32,178,170)]


def viz_text_box(img, list_poly, save_viz_path=None, ignor_type=[], viz_img_size=[20, 20], fontsize=20,
                 show_type=False):
    '''
    visualize polygon
    :param img: numpy image read by opencv
    :param list_poly: list of "poly" object that describe in common.py
    :param save_viz_path:
    :return:
    '''
    fig, ax = plt.subplots(1)
    fig.set_size_inches(viz_img_size[0], viz_img_size[1])
    plt.imshow(img)

    for polygon in list_poly:
        ax.add_patch(
            patches.Polygon(polygon.list_pts, linewidth=2, edgecolor=color_map[polygon.type], facecolor='none'))
        draw_value = polygon.value
        if show_type:
            draw_value = draw_value + ',' + str(type_map[polygon.type])
        if polygon.type in ignor_type:
            draw_value = ''
        max_x = max(polygon.list_pts[0][0], polygon.list_pts[1][0], polygon.list_pts[2][0])
        plt.text(max_x, polygon.list_pts[0][1], draw_value, fontsize=fontsize,
                 fontdict={"color": txt_color_map[polygon.type]})
    # plt.show()

    if save_viz_path is not None:
        print('Save visualized result to', save_viz_path)
        fig.savefig(save_viz_path, bbox_inches='tight')


def viz_page(page,
             text_box=True,  # draw text box in vis img
             key_value=True,  # draw key-value in vis img
             drop_score=0.5,
             font_path="./doc/simfang.ttf"):
    image = Image.fromarray(cv2.cvtColor(page.img, cv2.COLOR_BGR2RGB))

    h, w = image.height, image.width
    img_origin = image.copy()
    img_text_box = Image.new('RGB', (w, h), (255, 255, 255))
    img_key_value = Image.new('RGB', (w, h), (255, 255, 255))

    random.seed(0)
    draw_origin = ImageDraw.Draw(img_origin)
    draw_text_box = ImageDraw.Draw(img_text_box)
    draw_key_value = ImageDraw.Draw(img_key_value)

    final_draw = []
    font_size = int(image.height/60)
    for idx, tb in enumerate(page.list_text_boxes):
        box = tb.list_pts
        box_reshape = np.asarray(box).reshape(-1).tolist()

        txt = tb.value
        score = tb.conf_rec

        if score < drop_score:
            continue
        color = PALETTE[idx % 10]
        draw_origin.polygon(box_reshape, fill=color)
        # draw text box
        if text_box:
            # color = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
            draw_text_box.polygon(box_reshape,outline=color)
            box_height = math.sqrt((box[0][0] - box[3][0]) ** 2 + (box[0][1] - box[3][1]) ** 2)
            box_width = math.sqrt((box[0][0] - box[1][0]) ** 2 + (box[0][1] - box[1][1]) ** 2)
            if box_height > 2 * box_width:
                font_size = max(int(box_width * 0.9), 10)
                font = ImageFont.truetype(font_path, font_size, encoding="utf-8")
                cur_y = box[0][1]
                for c in txt:
                    char_size = font.getsize(c)
                    draw_text_box.text(
                        (box[0][0] + 3, cur_y), c, fill=(0, 0, 0), font=font)
                    cur_y += char_size[1]
            else:
                font_size = max(int(box_height * 0.8), 10)
                font = ImageFont.truetype(font_path, font_size, encoding="utf-8")
                draw_text_box.text([box[0][0], box[0][1]], txt, fill=(0, 0, 0), font=font)

    #draw image origin
    img_origin = Image.blend(image, img_origin, 0.5)
    final_draw.append(img_origin)

    #draw image text box
    if text_box and len(page.list_text_boxes)>0:
        final_draw.append(img_text_box)

    #draw image key value
    max_field = max(20, len(page.key_value.keys()))
    h_dist = int(image.height/max_field)
    x_align = int(image.width/100)
    font_size = int(image.height/60)
    offset_h =0
    if key_value and len(page.key_value.keys()) > 0:
        for i, key in enumerate(page.key_value.keys()):
            font = ImageFont.truetype(font_path, font_size, encoding="utf-8")
            draw_key_value.text([x_align, h_dist * (i + 1)+offset_h], key+': '+ str(page.key_value[key]), fill=(255, 0, 0), font=font)
            offset_h += str(page.key_value[key]).count('\n')*font_size
        final_draw.append(img_key_value)

    #combine to final draw
    img_show = Image.new('RGB', (w * len(final_draw), h), (255, 255, 255))
    for i in range(len(final_draw)):
        img_show.paste(final_draw[i], (w * i, 0, w * (i + 1), h))
    draw_img = np.array(img_show)
    res_img = draw_img[:, :, ::-1]
    return res_img, len(final_draw)


def viz_icdar(img_path, anno_path, save_viz_path=None, extract_kie_type=False, ignor_type=[1], viz_img_size=[20, 20],
              fontsize=20):
    if not isinstance(img_path, str):
        image = img_path
    else:
        image = cv2.imread(img_path)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    list_poly = []
    with open(anno_path, 'r', encoding='utf-8') as f:
        anno_txt = f.readlines()

    for anno in anno_txt:
        anno = anno.rstrip('\n')

        idx = -1
        for i in range(0, 8):
            idx = anno.find(',', idx + 1)

        coordinates = anno[:idx]
        val = anno[idx + 1:]
        type = 1
        if extract_kie_type:
            last_comma_idx = val.rfind(',')
            type_str = val[last_comma_idx + 1:]
            val = val[:last_comma_idx]
            if type_str in inv_type_map.keys():
                type = inv_type_map[type_str]

        coors = [int(f) for f in coordinates.split(',')]
        pol = text_box(coors, type=type, value=val)
        list_poly.append(pol)
    viz_text_box(img=image,
                 list_poly=list_poly,
                 save_viz_path=save_viz_path,
                 ignor_type=ignor_type,
                 viz_img_size=viz_img_size,
                 fontsize=fontsize)


def viz_icdar_multi(img_dir, anno_dir, save_viz_dir, extract_kie_type=False, ignor_type=[1]):
    list_files = get_list_file_in_folder(img_dir)
    for idx, file in enumerate(list_files):
        if idx < 0:
            continue
        # if 'mcocr_public_145014smasw' not in file:
        #     continue
        print(idx, file)
        img_path = os.path.join(img_dir, file)
        anno_path = os.path.join(anno_dir, file.replace('.jpg', '.txt'))
        save_img_path = os.path.join(save_viz_dir, file)
        viz_icdar(img_path, anno_path, save_img_path, extract_kie_type, ignor_type)


def viz_same_img_in_different_dirs(first_dir, second_dir, resize_ratio=1.0):
    list_files = get_list_file_in_folder(first_dir)
    list_err_images = None
    for n, file in enumerate(list_files):
        if list_err_images is not None:
            if file.replace('.jpg', '') not in list_err_images:
                continue
        print(n, file)
        # if n<160:
        #     continue
        first_img_path = os.path.join(first_dir, file)
        first_img = cv2.imread(first_img_path)
        first_img_res = cv2.resize(first_img,
                                   (int(resize_ratio * first_img.shape[1]), int(resize_ratio * first_img.shape[0])))
        cv2.imshow('first_img', first_img_res)

        second_img_path = os.path.join(second_dir, file)
        second_img = cv2.imread(second_img_path)
        second_img_res = cv2.resize(second_img,
                                    (int(resize_ratio * second_img.shape[1]), int(resize_ratio * second_img.shape[0])))
        cv2.imshow('second_img', second_img_res)
        cv2.waitKey(0)


def viz_pick_dataset(PICK_data_dir, output_viz_dir, dataset='train', ignore_classes=[]):
    boxes_and_transcripts_dir = os.path.join(PICK_data_dir, 'boxes_and_transcripts')
    images_dir = os.path.join(PICK_data_dir, 'images')
    list_img_files = get_list_file_in_folder(images_dir)
    list_img_files = sorted(list_img_files)

    print('get img list from PICK dataset')
    PICK_img_list = []
    with open(os.path.join(PICK_data_dir, '{}_list.csv'.format(dataset)), 'r', encoding='utf-8') as f:
        train_list = f.readlines()
    for line in train_list:
        idx = -1
        for i in range(0, 2):
            idx = line.find(',', idx + 1)
        img_name = line[idx + 1:].replace('\n', '')
        PICK_img_list.append(img_name)

    for idx, img_name in enumerate(list_img_files):
        if img_name not in PICK_img_list:
            continue
        print(idx, img_name)
        image = cv2.imread(os.path.join(images_dir, img_name))
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        tsv_path = os.path.join(boxes_and_transcripts_dir, img_name.split('.')[0] + '.tsv')
        if not os.path.exists(tsv_path):
            print('file not exist', tsv_path)
            continue

        list_poly = []
        with open(tsv_path, mode='r', encoding='utf-8') as f:
            tsv_annos = f.readlines()
        for anno in tsv_annos:
            idx, coordinates, val, type_str = split_idx_coor_val_type_from_pick_anno(anno)
            if type_str in ignore_classes:
                continue
            find_poly = text_box(coordinates, type=2, value=type_str)
            list_poly.append(find_poly)
        viz_text_box(img=image,
                     list_poly=list_poly,
                     save_viz_path=os.path.join(output_viz_dir, img_name),
                     show_type=False)


def viz_output_of_pick(img_dir, output_txt_dir, output_viz_dir):
    list_output_txt = get_list_file_in_folder(output_txt_dir, ext=['txt'])
    list_output_txt = sorted(list_output_txt)
    for n, file in enumerate(list_output_txt):
        # print(n, file)
        # if n <60:
        #     continue
        with open(os.path.join(output_txt_dir, file), mode='r', encoding='utf-8') as f:
            output_txt = f.readlines()

        list_poly = []
        for line in output_txt:
            coordinates, type, text = line.replace('\n', '').split('\t')
            find_poly = text_box(coordinates, type=2, value=text + ',' + type)
            list_poly.append(find_poly)

        img_name = None
        for ext in ['jpg', 'png']:
            if os.path.exists(os.path.join(img_dir, file.replace('.txt', '.' + ext))):
                img_name = file.replace('.txt', '.' + ext)
        if img_name is not None:
            image = cv2.imread(os.path.join(img_dir, img_name))
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            viz_text_box(img=image,
                         list_poly=list_poly,
                         save_viz_path=os.path.join(output_viz_dir, img_name),
                         show_type=False)
        else:
            print('viz_output_of_pick. No images!')


def viz_key_value(input_img, key_value_list, save_viz_path=None):
    '''
    visualize key_value as format of info['table_data']
    :param input_img:
    :param key_value_list:
    :return:
    '''
    h, w = input_img.shape[:2]
    vis_img = np.ones((h, int(w * 2), 3), dtype=np.uint8) * 255
    vis_img[:, :w] = input_img
    h_dist = 100

    fig, ax = plt.subplots(1)
    fig.set_size_inches(20, 20)
    plt.imshow(vis_img)

    for i, key_val in enumerate(key_value_list):
        text = key_val[0] + ': ' + key_val[1]
        plt.text(w + 30, h_dist * (i + 1), text, fontsize=15,
                 fontdict={"color": 'r'})
    # plt.show()

    if save_viz_path is not None:
        print('Save visualized result to', save_viz_path)
        fig.savefig(save_viz_path, bbox_inches='tight')


if __name__ == '__main__':
    img_path = '/data20.04/data/MC_OCR/mcocr2021_public_train_test_data/mcocr_public_train_test_shared_data/mcocr_val_data/val_images/mcocr_val_145114budzl.jpg'
    anno_path = '/home/cuongnd/PycharmProjects/aicr/PaddleOCR/inference_results/server_DB/output_txt/mcocr_val_145114budzl.txt'
    save_vix_path = 'test.jpg'
    # viz_icdar(img_path=img_path,
    #           anno_path=anno_path,
    #           save_viz_path=save_vix_path)

    # viz_icdar_multi(
    #     '/data20.04/data/data_Korea/WER_20210122/jpg',
    #     '/data20.04/data/data_Korea/WER_20210122/anno_icdar',
    #     '/data20.04/data/data_Korea/WER_20210122/viz_anno',
    #     ignor_type=[],
    #     extract_kie_type=False)

    src_dir = '/home/duycuong/home_data/vvn/OCR_MAFC/BHYT/web/imgs_output_2.0_1600'
    compare_dir = '/home/duycuong/home_data/vvn/OCR_MAFC/BHYT/web/imgs_output_2.0'
    list_path = '/home/cuongnd/PycharmProjects/mc_ocr/mc_ocr/submit/mc_ocr_private_test/check_coopmart.txt'

    viz_same_img_in_different_dirs(first_dir=src_dir,
                                   # list_path = list_path,
                                   second_dir=compare_dir,
                                   resize_ratio=0.3)

    # csv_file = '/data20.04/data/MC_OCR/output_results/EDA/mcocr_train_df_filtered_rotate_new.csv'
    # img_dir = '/data20.04/data/MC_OCR/output_results/key_info_extraction/train_combine_lines/refine/imgs'
    # viz_dir='/data20.04/data/MC_OCR/output_results/EDA/train_visualize_filtered_rotate'
    # viz_csv(csv_file=csv_file,
    #           viz_dir=viz_dir,
    #           img_dir=img_dir)

    # img_dir = '/data_backup/cuongnd/mc_ocr/output_result/rotation_corrector/SaleContract/imgs'
    # output_txt_dir = '/data_backup/cuongnd/mc_ocr/output_result/key_info_extraction/SaleContract/txt'
    # output_viz_dir =  '/data_backup/cuongnd/mc_ocr/output_result/key_info_extraction/SaleContract/viz_imgs'
    #
    # viz_output_of_pick(img_dir=img_dir,
    #                    output_txt_dir=output_txt_dir,
    #                    output_viz_dir=output_viz_dir)

    # PICK_data_dir = '/data_backup/cuongnd/finance_invoice/data_pick_invoice/data_train_pick'
    # output_viz_dir = '/data_backup/cuongnd/finance_invoice/data_pick_invoice/data_train_pick/viz_img'
    # viz_pick_dataset(PICK_data_dir=PICK_data_dir,
    #                  output_viz_dir=output_viz_dir,
    #                  dataset='train',
    #                  ignore_classes=['other'])

    # csv_file = '/data20.04/data/MC_OCR/output_results/EDA/mcocr_train_df_filtered_rotate_new.csv'
    # img_dir = '/data20.04/data/MC_OCR/output_results/box_rectify/train_pred_lines_filtered/imgs'
    # viz_csv2(csv_file=csv_file,
    #         img_dir=img_dir)

    # submit_file = '/home/cuongnd/PycharmProjects/aicr/mc_ocr/submit/private_test/results.csv'
    # img_dir = '/data20.04/data/MC_OCR/output_results/key_info_extraction/private_test_pick/output/viz_imgs'
    # list_path = '/home/cuongnd/PycharmProjects/aicr/mc_ocr/submit/check_sort_time.txt'
    # viz_submit(submit_file=submit_file,
    #            list_path=list_path,
    #            img_dir=img_dir)
