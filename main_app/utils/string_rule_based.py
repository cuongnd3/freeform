from pyxdameraulevenshtein import normalized_damerau_levenshtein_distance as ndl
import unidecode
import string, re


def norm_dist_between_2_str(first_str, second_str, lower = False):
    if lower:
        return ndl(first_str.lower(), second_str.lower())
    else:
        return ndl(first_str, second_str)

def refined_string(input_str, type='serial'):
    '''
    based on type of string --> refined
    :param input_str:
    :param type: 'serial' 'date time' ...
    :return:
    '''

    if type == 'serial':
        #eliminate all except digit and alphabet
        input_str = ''.join(e for e in input_str if e in string.digits + string.ascii_letters)

    return input_str


def insert_char(string, index, char=' '):
    return string[:index] + char + string[index:]

def reformat_string(input_str):
    list_sym =',.:'
    for sym in list_sym:
        for idx in range(0, len(input_str) - 2):
            if input_str[idx] == sym and input_str[idx + 1] != ' ':
                input_str = insert_char(input_str, idx + 1, char=' ')
    return input_str

def check_number_in_str(str, num_len=3):
    num_count = 0
    for ch in str:
        if ch.isdigit():
            num_count += 1
    if num_count >= num_len:
        return True
    else:
        return False

def check_if_a_str_in_list(current_list, input_str, distance_thres=0.1, debug=False):
    '''
    Check if a string appear in a list
    :param current_list:
    :param new_str:
    :param distance_thres:
    :return:
    '''
    min_diff = 1.0
    for str in current_list:
        norm_dist = norm_dist_between_2_str(str.lower(), input_str.lower())
        if norm_dist < min_diff and debug:
            min_diff = norm_dist
        if norm_dist < distance_thres:
            return True
    if debug:
        print('check_if_a_str_in_list. min_diff', min_diff)
    return False


alphabet_vn = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789ĂÂÊÔƠƯÁẮẤÉẾÍÓỐỚÚỨÝÀẰẦÈỀÌÒỒỜÙỪỲẢẲẨĐẺỂỈỎỔỞỦỬỶÃẴẪẼỄĨÕỖỠŨỮỸẠẶẬẸỆỊỌỘỢỤỰỴăâêôơưáắấéếíóốớúứýàằầèềìòồờùừỳảẳẩđẻểỉỏổởủửỷãẵẫẽễĩõỗỡũữỹạặậẹệịọộợụựỵ'
def strip_punctuation_from_str(input_str):
    '''
    to refine serial or number string
    :param input_str:
    :return:
    '''
    first_idx = 0
    for idx, ch in enumerate(input_str):
        if ch in alphabet_vn:
            first_idx = idx
            break

    last_idx = len(input_str) - 1
    for idx in range(len(input_str) - 1, 0, -1):
        if input_str[idx] in alphabet_vn:
            last_idx = idx
            break
    res_str = input_str[first_idx:last_idx + 1]
    return res_str


def eliminate_noise_from_str(input_str, noise_keywords):
    for key in noise_keywords:
        noise_keywords = sorted(noise_keywords, key=len, reverse=True)
        check_key = find_nearest_sub_str(input_str, key)
        check_key2 = find_nearest_sub_str_by_word(input_str, key)
        if check_key[1] != check_key2[1]:
            print('eliminate_noise_from_str. Different here', "'{}'".format(check_key[1]), "'{}'".format(check_key2[1]))
        if check_key2[0]:
            res_str = input_str[:check_key2[3]]
            res_str = strip_punctuation_from_str(res_str)
            return res_str
    return input_str


def get_value_from_str(input_str, split_char=':', keywords=None, check_key=False):
    '''
    Separate key - value from string by split_char to extract key, value from document
    :param input_str:
    :param split_char:
    :param keywords: list of recommended keywords
    :param check_key: in case spliting success, check if return key exist in a keywords
    :return:
    '''
    res_str = input_str.split(split_char)
    for st in res_str:
        if st =='':
            res_str.remove('')
            break
    if len(res_str) != 2:
        if isinstance(keywords, list):
            keywords = sorted(keywords, key=len, reverse=True)
            for key in keywords:
                check_key = find_nearest_sub_str_by_word(input_str, key)
                if check_key[0]:
                    split_idx = check_key[3] + len(check_key[1])
                    res_str = input_str[split_idx:]
                    res_str = strip_punctuation_from_str(res_str)
                    return True, check_key[1], res_str
        return False, '',''
    else:
        if check_key:
            if isinstance(keywords, list):
                check = check_if_a_str_in_list(keywords, res_str[0].strip(), distance_thres=0.21, debug=True)
                if check:
                    return True, res_str[0].strip(), res_str[1].lstrip()
                else:
                    print('get_value_from_str. Not match', input_str, 'with any key of', keywords)
                    return  False, '',''

        return True, res_str[0].strip(), res_str[1].lstrip()


def find_nearest_sub_str(txt, sub_txt, thr=0.21, lower=True, unicode = True):
    """
    compare all substring of txt --> find the most similar with sub_txt
    :param txt:
    :param sub_txt:
    :param thr:
    :return:
    """
    if lower:
        if unicode:
            txt = unidecode.unidecode(txt).lower()
            sub_txt = unidecode.unidecode(sub_txt).lower()
        else:
            txt = txt.lower()
            sub_txt = sub_txt.lower()


    if len(txt) < len(sub_txt):
        return False, '', 1, -1
    min_dis = 1
    nearest_text = ''
    start_idx = -1
    for i in range(len(txt) - len(sub_txt) + 1):
        ft = txt[i:i + len(sub_txt)]
        d = ndl(ft, sub_txt)
        if d < min_dis:
            min_dis = d
            nearest_text = ft
            start_idx = i
    if min_dis > thr:
        return False, '', 1, -1
    return True, nearest_text, min_dis, start_idx


def find_nearest_sub_str_by_word(txt, sub_txt, thr=0.21, lower=True, from_idx = 0, end_idx =-1):
    """
    compare all substring of txt --> find the most similar with sub_txt
    :param txt:
    :param sub_txt:
    :param thr:
    :return:
    """
    if lower:
        txt = txt.lower()
        sub_txt = sub_txt.lower()

    if len(txt) <= len(sub_txt):
        return False, '', 1, -1
    min_dis = 1
    nearest_text = ''
    start_idx = -1

    # filter all double or triple space
    txt = txt.replace('  ', ' ').replace('  ', ' ')
    txt_words = txt.split(' ')

    sub_txt = sub_txt.replace('  ', ' ').replace('  ', ' ')
    sub_txt_words = sub_txt.split(' ')

    total_step = len(txt_words) - len(sub_txt_words) + 1
    if end_idx !=-1:
        total_step = min(total_step, end_idx)

    if total_step > 0:
        for i in range(from_idx, total_step):
            ft = ' '.join(txt_words[i:i + len(sub_txt_words)])
            d = ndl(ft, sub_txt)
            if d < min_dis:
                min_dis = d
                nearest_text = ft
                start_idx = i
    if min_dis > thr or total_step < 1:
        # print('find_nearest_sub_str_by_word. min_dis',min_dis)
        return False, '', 1, -1
    return True, nearest_text, min_dis, start_idx


def string_contains_word(text, list_word, full=False, check_contains=True, is_number=False, is_vp_bank=True):
    """
    :param text:
    :param list_word:
    :param full: get distance < threshold --> return same string
    :param check_contains: True --> check word in list word in text or not
    :return:
    """
    if is_vp_bank:
        text = unidecode.unidecode(text).replace(' ', '').upper().split('/')[0]
        text = ''.join([c for c in text if 'A' <= c <= 'Z'])
        list_word = [unidecode.unidecode(w).replace(' ', '').upper().split('/')[0] for w in list_word]
        list_word = [''.join([c for c in w if 'A' <= c <= 'Z']) for w in list_word]
    if is_number:
        text = ''.join([c for c in text if c.isnumeric()])
        list_word = [''.join([c for c in w if c.isnumeric()]) for w in list_word]

    # check lower
    text = text.lower()

    for i, w in enumerate(list_word):
        w = w.lower()
        if check_contains:
            if text.__contains__(w):
                return i
        if not full:
            if ndl(text, w) < 0.2:
                return i
    return -1


if __name__ == '__main__':

    test= '#% 2345GNH_'
    res = re.sub(r'[^\w]', '', test)

    res2= ''.join(e for e in test if e in string.digits + string.ascii_letters)

    # print(norm_dist_between_2_str('MÁSỐ ', 'MÃ SỐ'))
    print(find_nearest_sub_str_by_word('Phường Thới Hòa, Thị xã Bến Cát, Tính Bình Dương: Nơi C3BĐ: 74147; H',
                                       'Nơi KCBBĐ:'))
    keywords=['account no', 'account number', "beneficiary's account number", 'a/c number', 'a/c no','(a/c) no']
    # keywords=['SWIFT','SWIFT CODE']
    for str in keywords:
        print(str)
        print(find_nearest_sub_str('SWIFT CODE: HVBKKRSE, Account No. : 568-250348-41-003', str))
        print(find_nearest_sub_str_by_word('Phường Thới Hòa, Thị xã Bến Cát, Tính Bình Dương: Nơi C3BĐ: 74147; H', 'Nơi KCBBĐ:'))

    # d = ndl('(a/c) no.','a/c no')

    print(get_value_from_str('USD (A/C) no. 156-00-0418873-8',keywords=keywords))