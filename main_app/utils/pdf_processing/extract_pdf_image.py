import os, fitz, time, cv2
import numpy as np
from pdf2image import convert_from_path
from main_app.utils.common import get_list_file_in_folder
from main_app.config.general import DEBUG_MODE

this_dir = os.path.dirname(__file__)


def get_num_pages(file_path):
    try:
        doc = fitz.open(file_path)
    except:
        return -1
    return len(doc)


def pix2np(pix):
    im = np.frombuffer(pix.samples, dtype=np.uint8).reshape(pix.h, pix.w, pix.n)
    im = np.ascontiguousarray(im[..., [2, 1, 0]])  # rgb to bgr
    return im


def pdf2cv2(pdf_path, max_pages=10000):
    '''
    convert pdf to list of opencv's images
    :param pdf_path:
    :return: list of opencv's imgs
    '''
    list_imgs = []
    try:
        doc = fitz.open(pdf_path)
        image_matrix = fitz.Matrix(fitz.Identity)
        image_matrix.preScale(3, 3)
        for i in range(len(doc)):
            pix = doc[i].getPixmap(alpha=False, matrix=image_matrix)
            im = pix2np(pix)
            list_imgs.append(im)
            if i >= max_pages:
                break
    except (RuntimeError, TypeError, NameError):
        print('pdf2cv. Error with pdf file!')

    return list_imgs


def get_list_img_from_pdf(pdf_path, max_pages=100000):
    list_img = []
    list_img_path = []
    file_name = os.path.basename(pdf_path)[:-4]
    save_dir = pdf_path[:-4]
    if not os.path.isdir(save_dir):
        os.mkdir(save_dir)
    else:
        if not DEBUG_MODE:
            import shutil
            shutil.rmtree(save_dir, ignore_errors=True)
            os.mkdir(save_dir)
    s = time.time()

    list_imgs = pdf2cv2(pdf_path)

    for i, im in enumerate(list_imgs):
        s1 = time.time()
        img_path = os.path.join(save_dir, "p%s.png" % (i))
        cv2.imwrite(img_path, im)
        list_img.append(im)
        list_img_path.append(img_path)
        e1 = time.time()
        if i >= max_pages:
            break
    e = time.time()
    print('pdf->img: {}(ms)'.format((e - s) * 1000))
    return list_img, save_dir, list_img_path


def convert_pdf_to_png(pdf_dir, output_img_dir):
    list_pdf_files = get_list_file_in_folder(pdf_dir, ext=['pdf', 'PDF'])
    for idx, pdf in enumerate(list_pdf_files):
        print(idx, pdf)
        pdf_path = os.path.join(pdf_dir, pdf)
        list_imgs = pdf2cv2(pdf_path)
        for i, im in enumerate(list_imgs):
            img_path = os.path.join(output_img_dir, pdf.split('.')[0] + '_' + str(i) + '.png')
            cv2.imwrite(img_path, im)


if __name__ == '__main__':
    pdf_dir = '/home/duycuong/home_data/vvn/OCR_MAFC/SMS_1414-20210514T080621Z-001/pdf'
    output_img_dir = '/home/duycuong/home_data/vvn/OCR_MAFC/SMS_1414-20210514T080621Z-001/imgs'
    convert_pdf_to_png(pdf_dir=pdf_dir,
                       output_img_dir=output_img_dir)
