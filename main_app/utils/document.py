from main_app.utils.pdf_processing.extract_pdf_image import pdf2cv2
from main_app.utils.visualize import viz_text_box, viz_page
from main_app.utils.image_processing import resize_max, distance_from_pts_to_line, order_points, rotate_and_crop, \
    transform_point
from main_app.utils.common import euclidean_distance
import time, cv2, math, os
import numpy as np


class document():
    def __init__(self, input, type='', max_pages=1000):
        '''
        class to represent document
        :param input: can be list of opencv's images or document's path
        :param type: GPLX, BHYT ...
        '''
        self.list_pages = []
        self.num_pages = 0
        self.type = type
        self.key_value = {}
        self.table_data = None  # to show in web app. It collect from key_value
        if isinstance(input, list):  # list of opencv's images
            self.num_pages = len(input)
            for idx, img in enumerate(input):
                self.list_pages.append(page(img=img,
                                            page_number=idx,
                                            type=type))
                if idx >= max_pages - 1:
                    break
        elif isinstance(input, str):  # document's path
            if not os.path.exists(input):
                print('document. file not exist!', input)
                return None
            if input.lower().endswith('.pdf'):
                # convert from pdf to images
                begin = time.time()
                list_imgs = pdf2cv2(input)
                if len(list_imgs)==0:
                    return None
                end = time.time()
                print('document. time for pdf2cv2', int(1000 * (end - begin)), 'ms')
                for idx, img in enumerate(list_imgs):
                    self.list_pages.append(page(img=img,
                                                page_number=idx,
                                                type=type))
                    if idx >= max_pages - 1:
                        break
            elif input.lower().endswith(('.png', '.jpg', '.PNG', '.JPG')):
                im = cv2.imread(input)
                self.list_pages.append(page(img=im,
                                            page_number=0,
                                            type=type))

    def visualize(self, text_box=True,  # draw text box in vis img
                  key_value=True,  # draw key-value in vis img
                  drop_score=-2,
                  font_path="./doc/simfang.ttf",
                  vis_dir='',
                  vis_name='',
                  max_len_for_each_page=1200):
        if vis_name != '': vis_name = vis_name + '_'
        for idx, page in enumerate(self.list_pages):
            save_path = os.path.join(vis_dir, vis_name + str(idx) + '.jpg')
            viz_img, num_pages = viz_page(page,
                                          text_box=text_box,
                                          key_value=key_value,
                                          drop_score=drop_score,
                                          font_path=font_path)
            res_ratio, res_img = resize_max(viz_img, max_len=num_pages * max_len_for_each_page)
            cv2.imwrite(save_path, res_img)
            print('document. visualize to', save_path)

    def save_page_images(self, save_dir, max_len=1200):
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)
        list_imgs_path = []
        for idx, page in enumerate(self.list_pages):
            save_path = os.path.join(save_dir, str(idx) + '.jpg')
            res_ratio, res_img = resize_max(page.img, max_len=max_len)
            cv2.imwrite(save_path, res_img)
            list_imgs_path.append(save_path)
            print('document. save_page_images. Save to', save_path)
        return list_imgs_path

    def merge_text_box_in_page(self):
        begin = time.time()
        for page in self.list_pages:
            page.merge_text_box()
        end = time.time()
        # print('merge_text_box_in_page. Time:',round(1000*(end-begin)),'ms')

    def fix_text_box_angle_in_page(self):
        begin = time.time()
        for page in self.list_pages:
            page.fix_text_box_angle()
        end = time.time()
        # print('fix_text_box_angle_in_page. Time:', round(1000 * (end - begin)), 'ms')


class page():
    def __init__(self, img, page_number=0, type=''):
        self.img = img
        self.page_number = page_number
        self.type = type
        self.list_text_boxes = []
        self.list_lines = []
        self.key_value = {}  # result of kie. e.g: {'name':'A','dOb':'20/04/1991}

    def get_list_imgs_from_text_boxes(self, extend_box, extend_x_ratio, extend_y_ratio, min_extend_x, min_extend_y):
        '''

        :param input:  'line' or 'text_box'
        :param extend_box:
        :param extend_x_ratio:
        :param extend_y_ratio:
        :param min_extend_x:
        :param min_extend_y:
        :return:
        '''
        list_imgs = []
        for box in self.list_text_boxes:
            box.img = rotate_and_crop(self.img, box.list_pts, debug=False,
                                      extend=extend_box,
                                      extend_x_ratio=extend_x_ratio, extend_y_ratio=extend_y_ratio,
                                      min_extend_y=min_extend_y, min_extend_x=min_extend_x)
            # cv2.imshow('img', box.img)
            # cv2.waitKey(0)
            list_imgs.append(box.img)
        return list_imgs

    def fix_text_box_angle(self):
        for tb in self.list_text_boxes:
            if tb.center == []:
                tb.calculate_width_height_center()

            angle = round(tb.get_horizontal_angle(), 1)
            if 180 - abs(angle) > 0.3:
                Mat_rotation = cv2.getRotationMatrix2D((tb.center[0], tb.center[1]), angle, 1)
                trans_pts = transform_point(tb.list_pts, Mat_rotation)
                trans_pts = np.asarray(trans_pts).astype(np.int0)
                trans_pts = order_points(trans_pts)
                tb.list_pts = trans_pts.tolist()

    def merge_text_box(self, angle_ths=6, distance_h_ths=0.3, height_ths=0.75, distance_w_ths=1.5, debug=False):
        '''
        group text box in same line
        :return:
        '''
        tb_params = []  # to store text box params [slope, b]
        tb_size = []  # to store text box [width, height]
        tb_center = []  # to store text box center [x,y]

        # for dfs ang group box
        tb_group = []
        tb_connected = []
        tb_visited = []

        tb_assign = []

        for tb in self.list_text_boxes:
            if tb.center == []:
                tb.calculate_width_height_center()
            tb_size.append([tb.width, tb.height])
            tb_center.append(tb.center)

            if tb.params == []:
                tb.get_params_of_text_box()
            tb_params.append(tb.params)
            tb_group.append(0)
            tb_connected.append([])
            tb_assign.append(False)
            tb_visited.append(False)

        # condition to merge (4 condition below)
        total_box = len(self.list_text_boxes)
        for i in range(total_box):
            for j in range(i + 1, total_box):
                # 1. check angle of 2 boxes
                angle_i = np.rad2deg(np.arctan2(tb_params[i][0], 1))
                angle_j = np.rad2deg(np.arctan2(tb_params[j][0], 1))
                diff_angle = abs(angle_i - angle_j)
                angle_cond = diff_angle < angle_ths
                if not angle_cond:
                    continue

                # 2. check height of 2 boxes.
                diff_h = 2 * abs(tb_size[i][1] - tb_size[j][1]) / (tb_size[i][1] + tb_size[j][1])
                height_cond = diff_h < height_ths
                if not height_cond:
                    continue

                # 3. check distance in y-axis of 2 boxes.
                mean_angle = (angle_i + angle_j) / 2
                slope = np.tan(np.deg2rad(mean_angle))
                center = [(tb_center[i][0] + tb_center[j][0]) / 2, (tb_center[i][1] + tb_center[j][1]) / 2]
                b = center[1] - slope * center[0]

                height_dis_i = distance_from_pts_to_line(tb_center[i][0], tb_center[i][1], slope, -1, b)
                height_dis_j = distance_from_pts_to_line(tb_center[j][0], tb_center[j][1], slope, -1, b)
                height_dis = height_dis_i + height_dis_j
                diff_distance_h = (2 * height_dis) / (tb_size[i][1] + tb_size[j][1])
                distance_h_cond = diff_distance_h < distance_h_ths
                if not distance_h_cond:
                    continue

                # 4. check distance in x-axis of 2 boxes.
                diff_w = 2 * (
                        euclidean_distance(tb_center[i], tb_center[j]) - ((tb_size[i][0] + tb_size[j][0]) / 2)) / (
                                 tb_size[i][1] + tb_size[j][1])
                distance_w_cond = diff_w < distance_w_ths
                if not distance_w_cond:
                    continue

                if debug: print(i, j, (self.list_text_boxes[i].value + '+' + self.list_text_boxes[j].value).ljust(80),
                                round(diff_angle, 2), round(diff_h, 2),
                                round(diff_distance_h, 2), round(diff_w, 2))
                tb_connected[i].append(j)

        for n in range(total_box):
            for node in tb_connected[n]:
                if n not in tb_connected[node]:
                    tb_connected[node].append(n)

        def dfs(group, connected, i, count):
            tb_visited[i] = True
            for node in connected[i]:
                if not tb_visited[node]:
                    group[node] = count
                    dfs(group, connected, node, count)

        count = 0
        for n in range(total_box):
            if len(tb_connected[n]) > 0 and not tb_visited[n]:
                count += 1
                tb_group[n] = count
                dfs(tb_group, tb_connected, n, count)

        list_line = []
        for n in range(total_box):
            if tb_assign[n]: continue
            if tb_group[n] == 0:
                list_line.append(self.list_text_boxes[n])
                tb_assign[n] = True
            else:
                li = [self.list_text_boxes[n]]
                tb_assign[n] = True
                for m in range(n + 1, total_box):
                    if tb_group[m] == tb_group[n] and not tb_assign[m]:
                        li.append(self.list_text_boxes[m])
                        tb_assign[m] = True

                li = sorted(li, key=lambda x: x.list_pts[0][0])  # sort by x

                total_pts = []
                for tb in li:
                    total_pts.extend(tb.list_pts)

                rect = cv2.minAreaRect(np.asarray(total_pts).astype(np.int32))
                pts = np.int0(cv2.boxPoints(rect))
                res_pts = order_points(pts)
                merge_tb = text_box(res_pts.reshape(-1).tolist())
                list_line.append(merge_tb)

        self.list_text_boxes = list_line


class text_box():
    def __init__(self, segment_pts, type=1, value=''):
        if isinstance(segment_pts, str):
            segment_pts = [int(f) for f in segment_pts.split(',')]
        elif isinstance(segment_pts, list):
            segment_pts = [round(f) for f in segment_pts]
        self.type = type
        self.value = value
        num_pts = int(len(segment_pts) / 2)
        if num_pts != 4:
            print('text_box is not qualitareal!')
        first_pts = [segment_pts[0], segment_pts[1]]
        self.list_pts = [first_pts]
        for i in range(1, num_pts):
            self.list_pts.append([segment_pts[2 * i], segment_pts[2 * i + 1]])

        # init confident for detector and recognizer
        self.conf_det = -1
        self.conf_rec = -1
        self.center = []
        self.params = []  # a,b in equation y = ax +b

    def check_max_wh_ratio(self):
        '''
        find longer edge + shorter edge and calculate long edge
        :return:
        '''
        max_ratio = 0
        assert len(self.list_pts) == 4
        first_edge = euclidean_distance(self.list_pts[0], self.list_pts[1])
        second_edge = euclidean_distance(self.list_pts[1], self.list_pts[2])
        if first_edge / second_edge > 1:
            long_edge = (self.list_pts[0][0] - self.list_pts[1][0], self.list_pts[0][1] - self.list_pts[1][1])
            short_edge = (self.list_pts[1][0] - self.list_pts[2][0], self.list_pts[1][1] - self.list_pts[2][1])
        else:
            long_edge = (self.list_pts[1][0] - self.list_pts[2][0], self.list_pts[1][1] - self.list_pts[2][1])
            short_edge = (self.list_pts[0][0] - self.list_pts[1][0], self.list_pts[0][1] - self.list_pts[1][1])
        max_ratio = max(first_edge / second_edge, second_edge / first_edge)
        return max_ratio, long_edge, short_edge

    def calculate_width_height_center(self):
        assert len(self.list_pts) == 4
        first_edge = euclidean_distance(self.list_pts[0], self.list_pts[1])
        second_edge = euclidean_distance(self.list_pts[1], self.list_pts[2])
        # assume that width always bigger than height
        self.width, self.height = max(first_edge, second_edge), min(first_edge, second_edge)
        self.center = [int((self.list_pts[0][0] + self.list_pts[1][0] + self.list_pts[2][0] + self.list_pts[3][0]) / 4),
                       int((self.list_pts[0][1] + self.list_pts[1][1] + self.list_pts[2][1] + self.list_pts[3][1]) / 4)]

    def is_horizontal_box(self):
        '''
        check if a box is horizontal box or not
        :return:
        '''
        angle_with_horizontal_line = self.get_horizontal_angle()
        if math.fabs(angle_with_horizontal_line) > 45 and math.fabs(angle_with_horizontal_line) < 135:
            return False
        else:
            return True

    def get_horizontal_angle(self):
        assert len(self.list_pts) == 4
        max_ratio, long_edge, short_edge = self.check_max_wh_ratio()
        if long_edge[0] == 0:
            if long_edge[1] < 0:
                angle_with_horizontal_line = -90
            else:
                angle_with_horizontal_line = 90
        else:
            angle_with_horizontal_line = math.atan2(long_edge[1], long_edge[0]) * 57.296
        return angle_with_horizontal_line

    def get_params_of_text_box(self):
        '''
        # get a,b in equation y = ax + b. this straight line go throuh centor of box

        :return:
        '''
        if self.center == []:
            self.calculate_width_height_center()

        if self.list_pts[0][0] != self.list_pts[1][0]:
            a = (self.list_pts[0][1] - self.list_pts[1][1]) / (self.list_pts[0][0] - self.list_pts[1][0])
            b = self.list_pts[0][1] - a * self.list_pts[0][0]
            self.params = [a, b]

    def to_icdar_line(self, value=False, type=False):
        line_str = ''
        if len(self.list_pts) == 4:
            for pts in self.list_pts:
                line_str += '{},{},'.format(pts[0], pts[1])
            line_str = line_str.rstrip(',')
            if value:
                line_str += ',' + self.value
            if type:
                line_str += ',' + str(self.type)

        else:
            print('to_icdar_line. text_box is not qualitareal')
        return line_str
