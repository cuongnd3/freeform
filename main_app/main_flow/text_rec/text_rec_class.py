import cv2, os
from main_app.utils.document import document
from main_app.main_flow.text_det.text_det_class import TextDet
from main_app.main_flow.doc_rot.doc_rot_class import DocRot
from main_app.config.general import PROJ_ROOT, config_by_type
import importlib


class TextRec():
    def __init__(self, model='paddle_v11'):
        self.model = model
        if model == 'paddle_v11':
            from main_app.main_flow.text_rec.paddle_v11 import text_recognition_paddle
            self.text_recognizer = text_recognition_paddle

    def inference(self, doc, merge_text_box=False, vis=False):
        if doc is None:
            print('TextRec. No input document!')
            return
        #overide config by config of each type
        if doc.type in config_by_type:
            config = importlib.import_module('freeform.main_app.config.form.' + doc.type)
            merge_text_box = config.merge_text_box

        if merge_text_box:
            doc.merge_text_box_in_page()
        if self.model == 'paddle_v11':
            self.text_recognizer.detect_document(doc, vis=vis)


if __name__ == '__main__':
    # file_path = '/home/duycuong/home_data/vvn/OCR_MAFC/BHYT/web/imgs/1734149_1729699_BHYT_0_1.png'
    file_path = '/home/duycuong/home_data/vvn/OCR_MAFC/BHYT/normal/1738769_1734234_BHYT_0.pdf'
    base_name = os.path.basename(file_path)

    vis_dir = os.path.join(PROJ_ROOT, 'uploads', base_name)
    if not os.path.exists(vis_dir):
        os.makedirs(vis_dir)

    # img = cv2.imread(file_path)
    new_doc = document(file_path, type='BHYT', max_pages=2)

    # init model
    text_detector = TextDet(model='paddle_v11')
    doc_rotator = DocRot(model='mobilenetv3')
    text_recognizer = TextRec(model='paddle_v11')

    text_detector.inference(new_doc)
    doc_rotator.inference(new_doc)
    text_recognizer.inference(new_doc)

    new_doc.visualize(vis_dir)
