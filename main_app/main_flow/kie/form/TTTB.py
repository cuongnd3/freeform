from main_app.utils.string_rule_based import strip_punctuation_from_str, find_nearest_sub_str_by_word, \
    norm_dist_between_2_str, reformat_string
from main_app.utils.document import text_box
from main_app.config.general import use_case
from main_app.main_flow.kie.rules import find_text_box_by_keywords, find_text_box_value_from_bottom, \
    find_value_in_text_box_or_nearby, find_text_box_in_same_row, find_text_box_in_same_column, find_text_box_contain_str

order_key = ['Họ tên', 'Sinh nhật', 'Số giấy tờ', 'Thuê bao']
map_key = {'Họ tên': 'hoVaTen',
           'Sinh nhật': 'namSinh',
           'Số giấy tờ': 'soGiayTo',
           'Thuê bao': 'soDienThoai'}

def remap_key_value_MAFC(key_value):
    res_info={}
    for key in map_key.keys():
        res_info[map_key[key]] = 'N/A'
    res_info['dcx']=0

    for key in key_value.keys():
        if key in map_key.keys():
            res_info[map_key[key]] = key_value[key]

    res_info['dcx'] = key_value['dcx'] if 'dcx' in key_value.keys()  else 0
    res_info['formType'] = '1414.1' if res_info['soDienThoai'] =='N/A' else '1414.2'
    return res_info


def insert_char(string, index, char=' '):
    return string[:index] + char + string[index:]


def extract_TTTB_by_rules(doc):
    # WARNING: extract information only on first page
    page = doc.list_pages[0]
    list_text_box = page.list_text_boxes


    sms_str = ''
    begin_idx = 0
    total_conf = 0
    len_conf = 0

    #find first line
    first_line_keywords = ['Ho ten', 'Tb tra truoc', 'TB ca nhan', 'TB tra sau']
    begin_idx, key, _ = find_text_box_by_keywords(page,
                                                       list_keywords=first_line_keywords,
                                                       single=False,
                                                       lower=False,
                                                       location='left',
                                                       similarity_thres=0.21,
                                                       max_number=1)
    if len(begin_idx)==0:
        return
    begin_idx = begin_idx[0]
    sms_str = list_text_box[begin_idx].value + ' '
    total_conf += list_text_box[begin_idx].conf_rec
    len_conf += 1

    #find body message
    list_body_msg_idx = find_text_box_in_same_column(page,
                                                     begin_idx,
                                                     position='bottom',
                                                     align='left',
                                                     from_idx = begin_idx,
                                                     max_number=20)

    for idx in list_body_msg_idx:
        sms_str += list_text_box[idx].value + ' '
        total_conf += list_text_box[idx].conf_rec
        len_conf += 1

    #reformat for better recogniton
    sms_str = reformat_string(sms_str)

    info_keys = [['Ho ten'], ['Ngay sinh'], ['So giay to', 'CMT', 'the can cuoc:'],
                 ['noi cap', 'ngay cap', 'noi cop', 'ngoy cop'], ['cac thue bao','dung thue bao','vu thue bao']]

    sms_str_words = sms_str.replace('  ', ' ').split(' ')

    key_value = {'Họ tên': 'N/A', 'Sinh nhật': 'N/A', 'Số giấy tờ': 'N/A', 'Thuê bao': 'N/A'}

    for key in info_keys[0]:
        check_key = find_nearest_sub_str_by_word(sms_str, key, thr=0.21, from_idx=0)
        name_start_pos = check_key[3]

    dob_start_pos = 0
    for key in info_keys[1]:
        check_key = find_nearest_sub_str_by_word(sms_str, key, thr=0.21, from_idx=name_start_pos)
        dob_start_pos = check_key[3]

    val = ' '.join(sms_str_words[name_start_pos + 2:dob_start_pos])
    val = strip_punctuation_from_str(val)
    key_value['Họ tên'] = val

    id_start_pos = 0
    id_string_words_len = 0
    for key in info_keys[2]:
        check_key = find_nearest_sub_str_by_word(sms_str, key, thr=0.251, from_idx=dob_start_pos, end_idx=dob_start_pos+10)
        if check_key[3] > 0:
            id_start_pos = check_key[3]
            id_string_words_len = len(key.split(' '))
            break

    val = ' '.join(sms_str_words[dob_start_pos + 2:id_start_pos])
    val = strip_punctuation_from_str(val)
    key_value['Sinh nhật'] = val

    noicap_ngaycap_start_pos = 0
    list_idx = []
    for key in info_keys[3]:
        check_key = find_nearest_sub_str_by_word(sms_str, key, thr=0.251, from_idx=id_start_pos)
        if check_key[3] > 0:
            list_idx.append(check_key[3])
    if len(list_idx) > 0:
        noicap_ngaycap_start_pos = min(list_idx)

    val = ' '.join(sms_str_words[id_start_pos + id_string_words_len:noicap_ngaycap_start_pos])
    val = strip_punctuation_from_str(val)
    key_value['Số giấy tờ'] = val

    sdt_start_pos = -1
    key_value['Thuê bao'] = 'N/A'
    for key in info_keys[4]:
        check_key = find_nearest_sub_str_by_word(sms_str, key, thr=0.21, from_idx=noicap_ngaycap_start_pos)
        if check_key[3] > 0:
            sdt_start_pos = check_key[3]
            break
    if sdt_start_pos > -1:
        val = sms_str_words[sdt_start_pos + 3]
        val = strip_punctuation_from_str(val)
        key_value['Thuê bao'] = val

    if len_conf > 0:
        total_conf = total_conf / len_conf
    key_value['dcx'] = total_conf

    doc.list_pages[0].key_value = key_value

    # merge all key_value from page
    for page in doc.list_pages:
        doc.key_value.update(page.key_value)

    # return data for table display
    doc.table_data = []
    if len(order_key) > 0:
        for order in order_key:
            if order in doc.key_value.keys():
                v = doc.key_value[order]
                doc.table_data.append([order, v])
    else:
        for key in doc.key_value.keys():
            doc.table_data.append([key, doc.key_value[key]])

    if use_case == 'MAFC':
        doc.key_value = remap_key_value_MAFC(doc.key_value)

    print(doc.list_pages[0].key_value)
