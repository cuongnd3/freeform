# from main_app.kie_models.PICK.pick_class import extract
from main_app.kie_models.sdmgr.sdmgr_class import extract
from main_app.utils.string_rule_based import get_value_from_str, check_if_a_str_in_list, string_contains_word, \
    eliminate_noise_from_str
import os

order_key = [('importer_name', "Importer's name"),
             ('exporter_name', "Exporter's name"),
             ('exporter_add', "Exporter's address"),
             ('contract_no', 'Contract No.'),
             ('payment_method', 'Payment method'),
             ('ben_name', "Beneficiary's name"),
             ('ben_add', "Beneficiary's address"),
             ('ben_bank_name', 'Bank of beneficiary'),
             ('ben_acc', "Beneficiary's account"),
             ('swift_code', 'SWIFT code')]


def extract_information(texts, metadata):
    """
    extract information in texts (should be block text) by keyword
    :param texts:
    :param metadata:
    :return:
    """
    # get keyword
    info = {}
    info_keywords = {}
    break_keywords = metadata['break_keywords']['keywords']
    for k in metadata.keys():
        if k == 'break_keywords':
            continue
        for kw in metadata[k]['keywords']:
            info_keywords[kw] = k

    # extract information by template
    start = 0
    end = len(texts)
    while start < end:
        # find place contain keywords
        idx = string_contains_word(texts[start], info_keywords.keys(), is_vp_bank=False)
        if idx == -1:
            start += 1
            continue
        text = texts[start]
        if not text.__contains__(':'):
            text += ':'
        if start == len(texts) - 1:
            start += 1
        # find all content of found keyword, stop when found other keyword
        for i in range(start + 1, len(texts)):
            start = i
            if string_contains_word(texts[i], info_keywords.keys(), is_vp_bank=False) >= 0 or \
                    string_contains_word(texts[i], break_keywords, is_vp_bank=False) >= 0:
                break
            text += ' ' + texts[i].strip()
        # extract keyword and content
        if idx >= 0:
            k = info_keywords[list(info_keywords.keys())[idx]]
            if text.__contains__(':') and k not in info:
                val = text[text.find(':') + 1:]
                if val.strip() != '':
                    if '(' in val and ')' not in val:
                        val = val.replace('(', '')
                    if ')' in val and '(' not in val:
                        val = val.replace(')', '')

                    # check if field content in defined list data
                    if len(metadata[k]['list_data']) > 0:
                        for dt in metadata[k]['list_data']:
                            if val.__contains__(' {} '.format(dt)):
                                info[k] = dt
                                break
                    else:
                        info[k] = val.strip()

    return info


def extract_sale_contract_by_rules(combined_list, metadata):
    block_keywords = metadata['block_keywords']['keywords']
    break_block_keywords = metadata['break_block_keywords']['keywords']
    texts = []
    for line in combined_list:
        line = sorted(line, key=lambda x: x[0])
        for bb in line:
            texts.append(bb[6])
            # print(bb[6])
    # pre-process to separate keyword
    break_keywords = metadata['break_keywords']['keywords']
    for kw in break_keywords:
        kw = kw.strip()
        if kw[-1] != ':':
            kw += ':'
        new_texts = []
        for i, text in enumerate(texts):
            import unidecode
            utext = unidecode.unidecode(text).upper()
            ukw = unidecode.unidecode(kw).upper()
            if utext.__contains__(ukw):
                idx = utext.index(ukw)
                if idx > 0:
                    new_texts.append(text[:idx])
                new_texts.append(text[idx:])
            else:
                new_texts.append(text)
        texts = new_texts
    # 1. get block content
    blocks = []
    other_block = []
    start = 0
    while start < len(texts):
        if string_contains_word(texts[start], block_keywords, is_vp_bank=False) == -1:
            other_block.append(texts[start])
            start += 1
            continue
        blocks.append([])
        blocks[-1].append(texts[start])
        if start == len(texts) - 1:
            start += 1
        for i in range(start + 1, len(texts)):
            start = i
            if string_contains_word(texts[i], block_keywords, is_vp_bank=False) >= 0 or string_contains_word(texts[i],
                                                                                                             break_block_keywords,
                                                                                                             is_vp_bank=False) >= 0:
                break
            blocks[-1].append(texts[i].strip())

    # 2. assign block content
    importer_block = []
    exporter_block = []
    beneficiary_block = []
    for block in blocks:
        if string_contains_word(' '.join(block), ['beneficiary'], is_vp_bank=False) >= 0:
            beneficiary_block = block
    beneficiary_info = extract_information(beneficiary_block,
                                           {'beneficiary_name': metadata['beneficiary_name'],
                                            'break_keywords': metadata['break_keywords']})
    importer_name_keywords = metadata['importer_name']['keywords'][:]
    importer_name_keywords.remove('and:')
    importer_name_keywords.remove('between:')
    exporter_name_keywords = metadata['exporter_name']['keywords'][:]
    exporter_name_keywords.remove('between:')
    exporter_name_keywords.remove('and:')
    for block in blocks:
        text = ' '.join(block)
        if not text.__contains__('beneficiary'):

            if string_contains_word(text, importer_name_keywords, is_vp_bank=False) >= 0:
                is_seller = False
            elif string_contains_word(text, exporter_name_keywords, is_vp_bank=False) >= 0:
                is_seller = True
            else:
                if 'beneficiary_name' in beneficiary_info:
                    beneficiary_name = beneficiary_info['beneficiary_name']
                    # check name of beneficiary same with
                    if string_contains_word(text, [beneficiary_name], is_vp_bank=False) >= 0:
                        is_seller = True
                    else:
                        is_seller = False
                else:
                    if len(importer_block) == 0:
                        is_seller = False
                    else:
                        is_seller = True
        else:
            continue
        if not is_seller:
            importer_block += block
        else:
            exporter_block += block
    print(beneficiary_block)
    print(exporter_block)
    print(importer_block)
    if len(exporter_block) > 0 and len(beneficiary_block) == 0:
        beneficiary_block = exporter_block
    # 3. Extract information for each block
    exporter_info = extract_information(exporter_block, {'exporter_name': metadata['exporter_name'],
                                                         'exporter_add': metadata['exporter_add'],
                                                         'break_keywords': metadata['break_keywords']})
    beneficiary_info = extract_information(beneficiary_block, {
        'beneficiary_bank_name': metadata['beneficiary_bank_name'],
        'beneficiary_add': metadata['beneficiary_add'],
        'beneficiary_name': metadata['beneficiary_name'],
        'beneficiary_acc': metadata['beneficiary_acc'],
        'swift_code': metadata['swift_code'],
        'break_keywords': metadata['break_keywords']})

    if 'exporter_name' not in exporter_info and 'beneficiary_name' in beneficiary_info:
        exporter_info['exporter_name'] = beneficiary_info['beneficiary_name']
    if 'exporter_name' in exporter_info and 'beneficiary_name' not in beneficiary_info:
        beneficiary_info['beneficiary_name'] = exporter_info['exporter_name']
    if 'exporter_add' not in exporter_info and 'beneficiary_add' in beneficiary_info:
        exporter_info['exporter_add'] = beneficiary_info['beneficiary_add']

    importer_info = extract_information(importer_block, {'importer_name': metadata['importer_name'],
                                                         'break_keywords': metadata['break_keywords']})
    # 4. extract other information
    other_info = extract_information(other_block, {'contract_no': metadata['contract_no'],
                                                   'swift_code': metadata['swift_code'],
                                                   'payment_method': metadata['payment_method'],
                                                   'break_keywords': metadata['break_keywords']})
    info = {}
    info.update(exporter_info)
    info.update(beneficiary_info)
    info.update(importer_info)
    info.update(other_info)

    return info


def extract_sale_contract_by_kie_model(save_dir, metadata, map_txt):
    '''

    :param save_dir:
    :return:
    '''
    ann_file = os.path.join(save_dir, 'test.txt')
    viz_dir = os.path.join(save_dir, 'sdmgr_output')
    final_list_keys = extract(ann_file=ann_file, viz_dir=viz_dir)

    # info = {'raw_data': {}, 'table_data': [], 'document_type': 'sale_contract'}
    # return info
    # final_list_keys = extract(save_dir, save_dir, save_dir)

    # rule base to fix key...
    total_list_key = {}

    payment_method_strict_keys = ['T/T', 'L/C', 'UPAS', 'D/P', 'D/A', 'TTR']

    for list_keys in final_list_keys:
        for key in list_keys.keys():
            for val in list_keys[key]:
                fix_key = key
                fix_val = map_txt[val]

                # Split key-value by colon (:) and remove noise in string
                print('extract_sale_contract_by_kie_model', fix_key, '||', fix_val)
                status, res_key, res_val = get_value_from_str(fix_val, keywords=metadata[key]['keywords'])
                if status:
                    fix_val = res_val
                fix_val = eliminate_noise_from_str(fix_val, noise_keywords=metadata['noise']['keywords'])

                # don't add string that too simple
                if fix_val == '':
                    continue

                if fix_key == 'payment_method':
                    # Get only keys from payment
                    for payment_key in metadata['payment_method']['list_data']:
                        if payment_key.upper() in fix_val.upper():
                            fix_val = payment_key
                else:
                    # fix wrong key-value for non-payment method field
                    for payment_key in payment_method_strict_keys:
                        if payment_key in fix_val.upper():
                            fix_key = 'payment_method'
                            fix_val = payment_key
                            break

                print('--------------------------------->', fix_key, '||', fix_val)

                # don't add string that exist
                if fix_key not in total_list_key.keys():
                    total_list_key[fix_key] = [fix_val]
                else:
                    if not check_if_a_str_in_list(total_list_key[fix_key], fix_val):
                        total_list_key[fix_key].append(fix_val)

                # TODO fix value for payment_method to get only keywords

    info = {'raw_data': {}, 'table_data': [], 'document_type': 'sale_contract'}

    for ent in order_key:
        if ent[0] in total_list_key.keys():
            if len(total_list_key[ent[0]]) > 1:
                for idx, val in enumerate(total_list_key[ent[0]]):
                    info['raw_data'][ent[1] + str(idx + 1)] = val
            else:
                for idx, val in enumerate(total_list_key[ent[0]]):
                    info['raw_data'][ent[1]] = val

    # return data for table display
    for order in order_key:
        for k in info['raw_data'].keys():
            if order[1] in k:
                v = info['raw_data'][k]
                # print(k, v)
                info['table_data'].append([k, v])
    return info


def extract_sale_contract(save_dir, combined_list, metadata, map_txt, rule_base=False):
    if rule_base:
        info = extract_sale_contract_by_rules(combined_list, metadata)
    else:
        info = extract_sale_contract_by_kie_model(save_dir, metadata, map_txt)
    # print('----------SALE CONTRACT-------------')
    return info, []
