from main_app.utils.string_rule_based import norm_dist_between_2_str
from main_app.utils.common import euclidean_distance
from main_app.utils.document import text_box


def find_val_poly(keys_poly, list_poly, expand_ratio=0.2, ignore_type=0):
    total_x, total_y = 0, 0
    min_h, max_h = 5000, 0
    value = ''
    for pts in keys_poly.list_pts:
        total_x += pts[0]
        total_y += pts[1]
        if pts[1] < min_h:
            min_h = pts[1]
        if pts[1] > max_h:
            max_h = pts[1]

    key_pol_max_x = max(keys_poly.list_pts[0][0], keys_poly.list_pts[1][0])
    expand_value = (max_h - min_h) * expand_ratio
    min_h = min_h - expand_value
    max_h = max_h + expand_value
    center_key_pts = (total_x / 4, total_y / 4)

    min_distance = 10000
    min_idx = None
    for idx, pol in enumerate(list_poly):
        if pol.type == ignore_type:
            continue
        total_x, total_y = 0, 0
        for pts in pol.list_pts:
            total_x += pts[0]
            total_y += pts[1]
        center_can_pts = (total_x / 4, total_y / 4)

        pol_min_x = min(pol.list_pts[0][0],pol.list_pts[1][0])
        if center_can_pts[1] > min_h and center_can_pts[1] < max_h and pol_min_x > key_pol_max_x:
            dis = euclidean_distance(center_key_pts, center_can_pts)
            if dis < min_distance:
                min_distance = dis
                min_idx = idx
    if min_idx is not None:
        list_poly[min_idx].type = 0
        value = list_poly[min_idx].value
    return value, min_idx


def find_name_val(tax_val_idx, list_poly, offset_x=10, max_box=4):
    '''
    find name of TEN NGUOI NOP THUE
    :param tax_val_idx:
    :param list_poly:
    :param offset_x:
    :param max_box:
    :return:
    '''
    name_val = ''
    box_height = 0
    begin_x = list_poly[tax_val_idx].list_pts[0][0] - offset_x
    last_begin_of_box = 0
    for idx in range(tax_val_idx + 1, tax_val_idx + 10):
        if list_poly[idx].type == 0:
            continue
        if box_height == 0:  # find first box
            if list_poly[idx].list_pts[0][0] > begin_x:
                name_val += list_poly[idx].value
                box_height = list_poly[idx].list_pts[2][1] - list_poly[idx].list_pts[1][1]
                last_begin_of_box = list_poly[idx].list_pts[0][1]
        else:
            if list_poly[idx].list_pts[0][0] > begin_x and list_poly[idx].list_pts[0][
                1] - last_begin_of_box < 1.5 * box_height:
                name_val += ' ' + list_poly[idx].value
                last_begin_of_box = list_poly[idx].list_pts[0][1]
    return name_val


def extract_giay_dkmst_by_rules(save_dir, combined_list, dist_thres=0.15):
    '''

    :param save_dir:
    :param combined_list:
    :param dist_thres:
    :return:
    '''
    # extract name

    # name_keywords = ['MÃ SỐ','MÃSỐ ','MÁ SỐ','MÀ SỐ']
    tax_keywords = ['MÃ SỐ THUẾ', 'MÃ SỐ NGƯỜI NỘP THUẾ']
    name_keywords = ['TÊN NGƯỜI NỘP THUẾ', 'TÊN ĐỐI TƯỢNG NỘP THUẾ', 'TÊN TỔ CHỨC, CÁ NHÂN NỘP THUẾ']
    list_poly = []

    for list_pol in combined_list:
        for polygon in list_pol:
            x1, y1, x2, y2 = str(polygon[0]), str(polygon[1]), str(polygon[2]), str(polygon[3])
            icdar_line = ','.join([x1, y1, x2, y1, x2, y2, x1, y2])
            pol = text_box(icdar_line, value=polygon[6])
            list_poly.append(pol)

    tax_poly = None
    idx_tax = -1
    min_dist_tax = 1
    name_poly = None
    idx_name = -1
    min_dist_name = 1
    for idx, pol in enumerate(list_poly):
        for key in tax_keywords:
            dist = norm_dist_between_2_str(key, pol.value)
            if dist < min_dist_tax:
                min_dist_tax = dist
                idx_tax = idx

        for key in name_keywords:
            dist = norm_dist_between_2_str(key, pol.value)
            if dist < min_dist_name:
                min_dist_name = dist
                idx_name = idx

    info = {'raw_data': {}, 'table_data': [], 'document_type': 'giay_dkmst'}
    tax_val_idx = None
    if min_dist_tax < dist_thres:
        print('MST', min_dist_tax, list_poly[idx_tax].value)
        list_poly[idx_tax].type = 0
        tax_poly = list_poly[idx_tax]
        tax_val, tax_val_idx = find_val_poly(tax_poly, list_poly, expand_ratio=1.0)
        tax_val = tax_val.replace(' ', '').replace('.', '')
        info['table_data'].append(('MÃ SỐ THUẾ', tax_val))
        info['raw_data']['tax_id'] = tax_val

    if tax_val_idx is not None:
        print('NAME', min_dist_name, list_poly[idx_name].value)
        list_poly[idx_name].type = 0
        name_val = find_name_val(tax_val_idx, list_poly)
        info['table_data'].append(('TÊN NGƯỜI NỘP', name_val))
        info['raw_data']['tax_payer'] = name_val

    print(info['table_data'])
    return info


def extract_giay_dkmst(save_dir, combined_list):
    info = extract_giay_dkmst_by_rules(save_dir, combined_list)
    print('----------GIAY DANG KY MST-------------')
    return info, []
