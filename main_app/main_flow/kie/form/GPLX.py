import requests, json, os, time
from main_app.utils.common import get_list_file_in_folder
from main_app.config.general import use_case

map_key = {'id': 'soGiayPhepLaiXe',
           'name': 'hoVaTen',
           'birthday': 'namSinh',
           'address': 'noiTru',
           'issue_date': 'ngayCap',
           'issue_by': 'noiCap',
           'expiry': 'ngayHetHan'}

def remap_key_value_MAFC(key_value):
    res_info = {}
    for key in map_key.keys():
        res_info[map_key[key]] = 'N/A'
    res_info['dcx']=0
    total_conf_field = 0
    if len(key_value.keys())<=1:
        return res_info
    if 'id' in key_value.keys():
        res_info['soGiayPhepLaiXe'] = key_value['id']
    dcx = 0
    if key_value['idconf'] != 'N/A':
        idconf_list = [float(f) for f in key_value['idconf'][1:-1].split(', ')]
        idconf = sum(idconf_list) / len(idconf_list)
        total_conf_field += 1
        dcx += idconf

    res_info['hoVaTen'] = key_value['name']
    if key_value['nameconf'] != 'N/A':
        nameconf_list = [float(f) for f in key_value['nameconf'][1:-1].split(', ')]
        nameconf = sum(nameconf_list) / len(nameconf_list)
        total_conf_field += 1
        dcx += nameconf

    res_info['namSinh'] = key_value['birthday'].replace('-', '/')
    if key_value['birthdayconf'] != 'N/A':
        birthday_conf = float(key_value['birthdayconf'][1:-1])
        total_conf_field += 1
        dcx += birthday_conf

    res_info['noiTru'] = key_value['address']
    if key_value['addressconf'] != 'N/A':
        addressconf = float(key_value['addressconf'][1:-1])
        total_conf_field += 1
        dcx += addressconf

    res_info['ngayCap'] = key_value['issue_date']
    if key_value['issue_date_conf'] != 'N/A':
        issue_date_conf = float(key_value['issue_date_conf'][1:-1])
        total_conf_field += 1
        dcx += issue_date_conf

    res_info['noiCap'] = key_value['issue_by']
    if key_value['issue_by_conf'] != 'N/A':
        issue_by_conf = float(key_value['issue_by_conf'][1:-1])
        total_conf_field += 1
        dcx += issue_by_conf

    res_info['ngayHetHan'] = key_value['expiry']

    res_info['dcx'] = dcx / total_conf_field

    return res_info


def ocr(api_url, api_key, image_path):
    files = {'image': open(image_path, 'rb')}
    try:
        resp = requests.post(url=api_url, files=files, headers={'key': api_key})
        res = json.loads(resp.text)
    except:
        res = {"result_code": 501}
    return res


def extract_GPLX(list_img_path):
    ocr_api_url = 'https://api.cloudekyc.com/v3.2/ocr/recognition'
    ocr_api_key = '8mq1AbRut79K19MILqj38hl8wW1Oisdw'

    key_value = {}
    for img_path in list_img_path:
        begin = time.time()
        res = ocr(ocr_api_url, ocr_api_key, img_path)
        end = time.time()
        print('extract_GPLX. times', round(1000 * (end - begin)), 'ms')
        if 'document' in res:
            if "DRIVER LICENSE" in res['document']:
                key_value = res
                break
        else: #wrong service
            key_value = res
            break

    table_data = []
    for k in key_value.keys():
        table_data.append([k, key_value[k]])

    if use_case == 'MAFC':
        key_value = remap_key_value_MAFC(key_value)
    return key_value, table_data


if __name__ == '__main__':
    # kk="[0.8, 0.79, 0.94, 0.87, 0.92, 0.91, 0.91, 0.87, 0.92, 0.91, 0.91, 0.92]"
    # list_kk=list(kk)
    #
    # img_dir = '/data_backup/cuongnd/Viettel_freeform/MAFC/GPLX/test'
    # list_imgs = get_list_file_in_folder(img_dir)
    # list_imgs = sorted(list_imgs)
    # list_img_path = [os.path.join(img_dir, f) for f in list_imgs]

    # extract_GPLX(list_img_path)

    kk = open('/home/duycuong/home_data/vvn/OCR_MAFC/BHYT/web/imgs/1723905_1719497_BHYT_0_0.png', 'rb')

    files = {'image': open('/home/duycuong/home_data/vvn/OCR_MAFC/BHYT/web/imgs/1723905_1719497_BHYT_0_0.png', 'rb')}
