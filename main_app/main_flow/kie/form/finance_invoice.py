from main_app.utils.string_rule_based import get_value_from_str, check_if_a_str_in_list

order_key = [('importer_name', "Importer's name"),
             ('exporter_name', "Exporter's name"),
             ('exporter_add', "Exporter's address"),
             ('contract_no', 'Contract No.'),
             ('payment_method', 'Payment method'),
             ('ben_name', "Beneficiary's name"),
             ('ben_add', "Beneficiary's address"),
             ('ben_bank_name', 'Bank of beneficiary'),
             ('ben_acc', "Beneficiary's account"),
             ('swift_code', 'SWIFT code')]

def extract_finance_invoice_by_kie_model(save_dir):


    info = {'raw_data': {}, 'table_data': [], 'document_type': 'finance_invoice'}


    return info

def extract_finance_invoice(save_dir, combined_list, metadata):
    info = extract_finance_invoice_by_kie_model(save_dir)
    print('----------FINANCE INVOICE-------------')
    return info, []