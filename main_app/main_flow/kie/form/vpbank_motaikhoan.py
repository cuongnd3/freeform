from PIL import Image
import cv2
import numpy as np
import glob
import re

from main_app.tools import test_paddle_ocr
import MTM
from MTM import matchTemplates, drawBoxesOnRGB
from main_app.main_flow.checkbox_detection import cb_detection
from main_app.config.general import DEBUG_MODE
from main_app.utils.string_rule_based import string_contains_word

print("MTM version: ", MTM.__version__)
list_template = []
for p in glob.glob('template/no/*.*'):
    list_template.append(('no', cv2.imread(p)))
for p in glob.glob('template/yes/*.*'):
    list_template.append(('yes', cv2.imread(p)))
format_path = 'metadata/vpbank_motaikhoan.tsv'


def read_format(format_path):
    import csv
    doc_format = {}
    with open(format_path, 'r', encoding='utf-8') as fi:
        reader = csv.reader(fi, delimiter='\t')
        last_key = ''
        for line in reader:
            no, key, ktype, break_keywords, values = line
            key = key.strip()
            ktype = ktype.strip()
            values = values.split(';')
            break_keywords = break_keywords.split(';')
            if ktype == 'block':
                doc_format[key] = {"type": ktype, "break_keywords": break_keywords, 'values': values, 'children': {}}
                last_key = key
            if ktype != 'block' and last_key != '':
                doc_format[last_key]['children'][key] = {"type": ktype, 'values': values, "break_keywords": break_keywords}
    return doc_format


doc_format = read_format(format_path)


def find_checkbox_template_matching(img):
    """
    Find checked checkbox
    :param img:
    :return:
    """
    h = 3000
    w = int(h / img.shape[0] * img.shape[1])
    scale = img.shape[0]/h
    img = cv2.resize(img, (w, h))
    list_detected_checkbox = []
    # find checkbox by template matching
    hits = matchTemplates(list_template, img, score_threshold=0.6, method=cv2.TM_CCOEFF_NORMED,
                          maxOverlap=0.25)

    out_image = img.copy()
    for index, row in hits.iterrows():
        x, y, w, h = row['BBox']
        if row['TemplateName'] in ['yes']:
            cv2.rectangle(out_image, (x, y), (x + w, y + h), (0, 255, 0))
        else:
            cv2.rectangle(out_image, (x, y), (x + w, y + h), (255, 255, 0))
    if DEBUG_MODE:
        cv2.namedWindow('mtm', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('mtm', 1280, 960)
        cv2.imshow('mtm', out_image)
        cv2.waitKey(1)
    for index, row in hits.iterrows():
        x1, y1, w, h = row['BBox']
        if row['TemplateName'] in ['yes', 'no']:
            list_detected_checkbox.append([x1, y1, w, h, row['TemplateName']])

    for i in range(len(list_detected_checkbox)):
        list_detected_checkbox[i][0] = int(scale * list_detected_checkbox[i][0])
        list_detected_checkbox[i][1] = int(scale * list_detected_checkbox[i][1])
        list_detected_checkbox[i][2] = int(scale * list_detected_checkbox[i][2])
        list_detected_checkbox[i][3] = int(scale * list_detected_checkbox[i][3])
    return list_detected_checkbox, out_image


def extract_checkbox(key, values, list_texts, list_detected_checkbox):
    vals = []
    if len(list_texts) == 0:
        return ','.join(vals)
    # find all yes check box in list text location
    x1 = min([bx1 for bx1, by1, bx2, by2, cy, h, txt, score, _ in list_texts])
    x2 = max([bx2 for bx1, by1, bx2, by2, cy, h, txt, score, _ in list_texts])
    y1 = min([by1 for bx1, by1, bx2, by2, cy, h, txt, score, _ in list_texts])
    y2 = max([by2 for bx1, by1, bx2, by2, cy, h, txt, score, _ in list_texts])
    list_yes_checkbox = []
    for x, y, w, h, checked in list_detected_checkbox:
        if x1 <= x + w/2 <= x2 and y1 <= y + h/2 <= y2:
            if checked == 'yes':
                list_yes_checkbox.append([x, y, w, h, checked])
    if len(list_yes_checkbox) == 0:
        return ','.join(vals)

    list_corrected_texts = []
    for bx1, by1, bx2, by2, cy, h, txt, score, _ in list_texts:
        if key == 'Thu nhập trung bình năm(triệu đồng)':
            is_number = True
        else:
            is_number = False
        idx = string_contains_word(txt, values, is_number=is_number)
        if idx >= 0:
            list_corrected_texts.append([bx1, by1, bx2, by2, cy, h, values[idx], score])
        else:
            list_corrected_texts.append([bx1, by1, bx2, by2, cy, h, txt, score])
    import math
    for x, y, w, h, checked in list_yes_checkbox:
        cx, cy = x + w/2, y + h/2
        min_dy = min([math.fabs(bcy-cy) if not by1 <= cy <= by2 else 0 for bx1, by1, bx2, by2, bcy, bh, txt, score in list_corrected_texts])
        b_val = ''
        min_dx = 1111111111111111
        for bx1, by1, bx2, by2, bcy, bh, txt, score in list_corrected_texts:
            dy = math.fabs(bcy-cy) if not by1 <= cy <= by2 else 0
            if dy > min_dy + h/2:
                continue
            dx = math.fabs((bx1 + bx2)/2-cx) if not bx1 <= cx <= bx2 else 0
            if min_dx > dx:
                min_dx = dx
                b_val = txt
        if b_val != '':
            vals.append(b_val)

    return ','.join(vals)


def extract_pt(key, txt):
    if txt == '':
        return txt
    if key == 'Điện thoại di động':
        nu = re.findall(r'\d+', txt)
        nu = sorted(nu, key=lambda k: len(k), reverse=True)
        txt = nu[0]
    if key == 'Email':
        es = txt.split()
        es = sorted(es, key=lambda k: len(k), reverse=True)
        for e in es:
            if e.__contains__('@'):
                txt = e
                break
    if key == 'Kính gửi_Chi nhánh/Văn Phòng giao dịch':
        es = txt.split()
        # es = sorted(es, key=lambda k: len(k), reverse=True)
        txt = es[0]
    return txt


def extract_info(big_part, list_words, list_detected_checkbox):
    res = {}
    # find line for each key in bigpart
    start = 0
    end = 0
    list_part = {}
    for key in doc_format[big_part]['children']:
        list_keys = [k.split('_')[0].strip().split("(")[0] for k in key.split(';')]
        break_keywords = doc_format[big_part]['children'][key]['break_keywords']
        for i in range(end, len(list_words)):
            txt = list_words[i][6].split(":")[0].split("(")[0]
            # str must contain key but not break words
            if string_contains_word(txt, list_keys, check_contains=False) >= 0:
                # if string_contains_word(txt, break_keywords, check_contains=False) >= 0:
                #     continue
                start = i
                # spacial case for name
                if key == "Họ và tên (Đồng thời là tên tài khoản thanh toán)":
                    roi_img = list_words[i][-1]
                    w = len(key)/(len(key) + (len(list_words[i][6]) - len(key))*1.2) * roi_img.shape[1]
                    name_img = roi_img[:, int(w):, :]
                    # cv2.imshow('name', name_img)
                    # cv2.waitKey()
                    name = test_paddle_ocr.text_recognition_paddle.predict_vn(name_img, return_score=False)
                    list_words[i][6] = key + ' : ' + name
                break
        if len(break_keywords) == 0 or break_keywords[0] == "":
            end = len(list_words)
        for i in range(start + 1, len(list_words)):
            txt = list_words[i][6].split(":")[0].split("(")[0]
            if len(break_keywords) == 0 or break_keywords[0] == "" :
                end = len(list_words)
                break
            if string_contains_word(txt, break_keywords, check_contains=False) >= 0:
                end = i
                break
        list_part[key] = list_words[start:end]
        start = end + 1
        continue

    # extract information
    for key in list_part:
        kt = doc_format[big_part]['children'][key]['type']
        val = ''
        if kt == 'pt':
            ocr_text = ' '.join([w[6] for w in list_part[key]])
            if ocr_text.__contains__(':'):
                val = ocr_text[ocr_text.rfind(':') + 1:].strip()
                val = extract_pt(key, val)
        if kt == 'cb':
            val = extract_checkbox(key, doc_format[big_part]['children'][key]['values'], list_part[key], list_detected_checkbox)
        res[key] = val

    return res


def get_big_part(list_detected_checkbox, combined_list):
    start = 0
    end = 0
    list_big_part = {}
    for big_part in doc_format.keys():
        found = False
        big_part_kw = big_part.split(';')
        for i in range(end, len(combined_list)):
            for w in combined_list[i]:
                txt = w[6]
                if string_contains_word(txt, big_part_kw) >= 0:
                    start = i
                    found = True
                    break
            if found:
                break
        found = False
        for i in range(start, len(combined_list)):
            if len(doc_format[big_part]['break_keywords']) == 0 or doc_format[big_part]['break_keywords'][0] == "" :
                end = len(combined_list)
                break
            for w in combined_list[i]:
                txt = w[6]
                if string_contains_word(txt, doc_format[big_part]['break_keywords']) >= 0:
                    end = i
                    found = True
                    break
            if found:
                break
        list_big_part[big_part] = []
        for line in combined_list[start:end]:
            list_big_part[big_part] += line
        start = end + 1
        continue

    for big_part in list_big_part.keys():
        print(big_part)
        for w in list_big_part[big_part]:
            print(w[:-1])
    return list_big_part


def sort_merge_pages(list_page_data):
    merge_list_detected_checkbox = []
    merge_combined_list = []
    list_page_order = []
    for k in doc_format.keys():
        list_page_order += [w.split('.')[-1] for w in k.split(';')]
    list_p_idx = []
    for list_detected_checkbox, combined_list_, _ in list_page_data:
        txts = ([' '.join([w[6] for w in line ]) for line in combined_list_])
        for txt in txts:
            p_idx = string_contains_word(txt, list_page_order)
            if p_idx >= 0:
                list_page_order[p_idx] = '--dsdklshfk45893fh84yff'
                break
        if p_idx < 0:
            p_idx = len(list_page_order) + 1
        list_p_idx.append(p_idx)
    print(list_p_idx)
    for i in range(len(list_page_data)):
        list_page_data[i].append(list_p_idx[i])
    list_page_data = sorted(list_page_data, key=lambda k: k[-1])
    h = 0
    for list_detected_checkbox, combined_list_, img_shape, _ in list_page_data:
        for detected_checkbox in list_detected_checkbox:
            detected_checkbox[1] += h
            merge_list_detected_checkbox.append(detected_checkbox)
        for line in combined_list_:
            for i in range(len(line)):
                line[i][1] += h
                line[i][3] += h
            merge_combined_list.append(line)
        h += img_shape[0]
    return merge_list_detected_checkbox, merge_combined_list


def read_idcard(img):
    import requests
    import json
    api_url, api_key = 'https://api.cloudekyc.com/v3.2/ocr/recognition', 'zUrNTCfAwK1qhO2h4dzCcIt6VNXOMiLT'
    tmp_path = 'tmp.jpg'
    cv2.imwrite(tmp_path, img)
    files = {'image': open(tmp_path, 'rb')}
    try:
        resp = requests.post(url=api_url, files=files, headers={'key': api_key})
        res = json.loads(resp.text)
    except:
        res = {}

    return res


def to_text(list_img, img_path=""):
    """
    Ocr each image
    find checkbox by template matching
    :param list_img:
    :param img_path:
    :return:
    """
    list_page_data = []
    idcard_info = {}
    for i, img in enumerate(list_img):
        if img is None:
            continue
        cv2.imwrite(img_path + '{}.png'.format(i), img)
        # list_detected_checkbox, out_image = find_checkbox_template_matching(img)
        list_detected_checkbox, out_image = cb_detection.detection_checkbox(img)
        det_res, txts, scores, res_img = test_paddle_ocr.ocr(img, list_detected_checkbox, drop_score=0.5, vis=DEBUG_MODE)

        if len(txts) == 0:
            continue
        if (string_contains_word('chungminhnhandan', txts) >= 0 or string_contains_word('giaychungminhnhandan', txts) >= 0 or string_contains_word('cancuoccongdan', txts)  >= 0)and string_contains_word('ConghoaxahoichunghiaVietNam', txts) >= 0:
            idcard_info = read_idcard(img[:img.shape[0]//2, :img.shape[1] * 2 // 3,:])

        combined_list_, img = test_paddle_ocr.group_text_box_to_line(img, det_res, txts, scores)
        # attach roi image to each word
        for k, line in enumerate(combined_list_):
            for j, w in enumerate(line):
                x1, y1, x2, y2 = w[:4]
                w.append(img[y1:y2, x1:x2, :])
                combined_list_[k][j] = w
        cv2.imwrite(img_path + '_{}res.png'.format(i), res_img)
        cv2.imwrite(img_path + '_{}checkbox.png'.format(i), out_image)
        print(img.shape)
        if DEBUG_MODE:
            cv2.imshow('img', img)
            cv2.waitKey(1)
        list_page_data.append([list_detected_checkbox, combined_list_, img.shape])
        for line in combined_list_:
            for x1, y1, x2, y2, cy, h, text, score, _ in line:
                print(text)
    return list_page_data, idcard_info


def return_data(res):
    # return data for table display
    # and dict for parsing
    extracted_info = {}
    table_data = []
    for big_part in res.keys():
        table_data.append(['', big_part.split(';')[0], ''])
        extracted_info[big_part.split(';')[0]] = {}
        idx = 1
        for k in doc_format[big_part]['children'].keys():
            v = res[big_part][k]
            table_data.append([idx, k.split(';')[0], v])
            extracted_info[big_part.split(';')[0]][k.split(';')[0]] = v
            idx += 1

    return table_data, extracted_info


def hard_code_by_rule(res, idcard_info):
    customer_key = ""
    sig_key = ''
    internet_bank_key = ''
    tktt_key = ''
    for k in res.keys():
        if k.__contains__('I. THÔNG TIN KHÁCH HÀNG'):
            customer_key = k
        if k.__contains__('VI. CHỮ KÝ CỦA KHÁCH HÀNG(ĐỒNG THỜI LÀ CHỮ KÝ MẪU) VÀ THÔNG TIN ĐĂNG KÝ'):
            sig_key = k
        if k.__contains__('IV. ĐĂNG KÝ SỬ DỤNG DICH VỤ NGÂN HÀNG ĐIÊN TỬ ("DV NHĐT")'):
            internet_bank_key = k
        if k.__contains__('II. ĐĂNG KÝ MỞ TÀI KHOẢN THANH TOÁN'):
            tktt_key = k

    # name (get from ID card if exits)
    if idcard_info.__contains__('name') and not idcard_info['name'].__contains__('N/A'):
        if customer_key in res:
            res[customer_key]['Họ và tên (Đồng thời là tên tài khoản thanh toán)'] = idcard_info['name']
    # SDT - email
    if customer_key in res and sig_key in res:
        res[customer_key]['Điện thoại di động, Email'] = \
            res[sig_key]['Điện thoại di động'] + ' ' + \
            res[sig_key]['Email']
    # VPbank sms
    if tktt_key in res and res[tktt_key]['Gói tài khoản'].__contains__('nhận lương'):
        if internet_bank_key in res and 'Hình thức nhận mã khóa bị mật dùng một lần (OTP)' in res[internet_bank_key]:
            res[internet_bank_key]['Hình thức nhận mã khóa bị mật dùng một lần (OTP)'] = 'VPBankSMS'
    return res


def read_content(list_img, img_path=''):
    # 1. ocr + checkbox detection
    import os
    import pickle

    tmp_file = img_path.replace('.tif', 'pick')
    # if not os.path.isfile(tmp_file):
    if True:
        list_page_data, idcard_info = to_text(list_img, img_path=img_path)
        with open(tmp_file, 'wb') as fi:
            pickle.dump([list_page_data, idcard_info], fi)
    else:
        with open(tmp_file, 'rb') as fi:
            list_page_data, idcard_info = pickle.load(fi)

    # 2. sort page --> merge all page to long page
    list_detected_checkbox, combined_list = sort_merge_pages(list_page_data)

    # 3. separate big part
    list_big_part = get_big_part(list_detected_checkbox, combined_list)

    res = {}
    # 4. extract information
    for big_part in list_big_part:
        res[big_part] = extract_info(big_part, list_big_part[big_part], list_detected_checkbox)

    # hard code by rule
    res = hard_code_by_rule(res, idcard_info)

    for big_part in res.keys():
        print('', big_part.split(';')[0])
        for k, v in res[big_part].items():
            print('{}: {}'.format(k, v))
    return res


def extract_img(img_path, save_dir=None):
    if img_path[-5:] == '.tiff':
        img = Image.open(img_path)
        images = []
        for i in range(img.n_frames):
            try:
                img.seek(i)
                if np.asarray(img).dtype == np.bool:
                    images.append(np.asarray(img).astype(np.uint8) * 255)
                    images[-1] = cv2.cvtColor(images[-1], cv2.COLOR_GRAY2BGR)
                elif np.asarray(img).dtype == np.uint8:
                    images.append(np.asarray(img))
                    images[-1] = cv2.cvtColor(images[-1], cv2.COLOR_RGB2BGR)
                else:
                    continue
            except EOFError:
                # Not enough frames in img
                break
    elif img_path[-4:] == '.pdf':
        from pdf_processing import extract_pdf_image
        images, save_dir, list_img_path = extract_pdf_image.get_list_img_from_pdf(img_path)
    else:
        images = [cv2.imread(img_path)]

    if save_dir is not None:
        n = len(glob.glob(save_dir + '/*.*'))
        import os
        for img in images:
            save_path = os.path.join(save_dir, "{:04d}.jpg".format(n))
            cv2.imwrite(save_path, img)
            n += 1
    return images


def read_content_from_document(img_path, return_table_data=False):
    import time
    s = time.time()
    images = extract_img(img_path)
    res = read_content(images, img_path)
    table_data, extracted_info = return_data(res)
    e = time.time()
    result = {"elapsed_time(s)": e - s,
              'extracted_info': extracted_info}
    if return_table_data:
        result['table_data'] = table_data
    return result


if __name__ == "__main__":
    # for img_path in glob.glob('/data/Dataset/Images/OCR/FreeForm/Viettel_FreeFrom/IssueLC/Sacombank/*.*'):
    #     extract_img(img_path, '/data/Dataset/Images/OCR/FreeForm/checkbox_detection')
    # exit()
    for img_path in glob.glob('/data/Dataset/Images/OCR/FreeForm/VPbank/Form/**/*.tif'):
        print(img_path)
        # img_path = '/data/Dataset/Images/OCR/FreeForm/VPbank/Form/RCOB-0000191768/CA_AOF.tif'
        img_path = '/data/Dataset/Images/OCR/FreeForm/VPbank/Form/RCOB-0000191730/CA_AOF.tif'
        read_content_from_document(img_path)
        break