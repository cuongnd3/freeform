from main_app.utils.string_rule_based import get_value_from_str, find_nearest_sub_str_by_word, norm_dist_between_2_str
from main_app.main_flow.kie.form.giay_ky_quy import extract_giay_ky_quy


def extract_giayLC_by_rules(save_dir, combined_list):
    texts = []

    for line in combined_list:
        line = sorted(line, key=lambda x: x[0])
        for bb in line:
            texts.append(bb[6])

    # check first page
    is_LC_first_page = False
    LC_first_page = 'GIẤY ĐỀ NGHỊ MỞ LC'
    is_ky_quy_first_page = False
    ky_quy_first_page = 'GIẤY ĐỀ NGHỊ KÝ QUỸ/THU PHÍ'
    for i in range(0, 7):
        # print(texts[i])
        LC_dist = norm_dist_between_2_str(LC_first_page.lower(), texts[i].lower())
        if LC_dist < 0.4:
            is_LC_first_page = True
            break

        ky_quy_dist = norm_dist_between_2_str(ky_quy_first_page.lower(), texts[i].lower())
        if ky_quy_dist < 0.4:
            is_ky_quy_first_page = True
            break

    LC_val, NO_val, DATE_val, DATE_val_refined, ACC_val = '', '', '', '', ''

    info = {'raw_data': {}, 'table_data': [], 'document_type': 'giayLC'}
    # find LC value in first page
    if is_LC_first_page:
        print('extract_giayLC_by_rules. Doc type: Giay LC page 1')
        LC_val_pre = 'irrevocable LC value'
        LC_val_post = 'theo nội'
        index_of_LC_val = -1
        for i in range(5, 10):
            # print(texts[i])
            split_txt = texts[i].replace('  ', ' ').replace('  ', ' ').split(' ')
            is_find_pre, nearest_text, min_dis, start_idx = find_nearest_sub_str_by_word(texts[i], LC_val_pre, thr=0.15)
            if is_find_pre:
                is_find_post, nearest_text, min_dis, end_idx = find_nearest_sub_str_by_word(texts[i], LC_val_post,
                                                                                            thr=0.15)
                if is_find_post:  # single box for key and value
                    LC_val = ' '.join(split_txt[start_idx + len(LC_val_pre.split(' ')): end_idx])
                else:  # value in different box
                    LC_val = texts[i + 1]
                # print('LC_val',LC_val)
                index_of_LC_val = i
                break

        extend_idx = 6
        if index_of_LC_val == -1:
            extend_idx = 15

        NO_keyword = 'hợp đồng ngoại thương'
        # NO_pre2 = 'số'
        DATE_keyword = ['ngày', 'ngày/date']
        for i in range(index_of_LC_val + 1, index_of_LC_val + extend_idx):
            # print(texts[i])
            # split_txt = texts[i].replace('  ',' ').replace('  ',' ').split(' ')
            is_find_NO, nearest_text, min_dis, NO_idx = find_nearest_sub_str_by_word(texts[i], NO_keyword, thr=0.15)
            if is_find_NO:
                new_find_text = ' '.join(texts[i:i + 5])
                new_split_txt = new_find_text.replace('  ', ' ').replace('  ', ' ').split(' ')
                for DATE in DATE_keyword:
                    is_find_DATE, nearest_text, min_dis, DATE_idx = find_nearest_sub_str_by_word(new_find_text, DATE,
                                                                                                 thr=0.3,
                                                                                                 from_idx=NO_idx + len(
                                                                                                     NO_keyword.split(
                                                                                                         ' ')))
                    if is_find_DATE:  # single box for key and value
                        if DATE == 'ngày/date':

                            is_find_num, nearest_text, min_dis, num_idx = find_nearest_sub_str_by_word(new_find_text,
                                                                                                       'Số/No:*',
                                                                                                       thr=0.3,
                                                                                                       from_idx=NO_idx + len(
                                                                                                           NO_keyword.split(
                                                                                                               ' ')))
                            if is_find_num:
                                NO_val = ' '.join(new_split_txt[num_idx + 1:NO_idx])

                            else:

                                NO_val = ' '.join(new_split_txt[NO_idx + len(NO_keyword.split(' ')) + 1:DATE_idx])
                                # TODO Clean NO_val

                                if DATE_idx < len(split_txt) - 1:
                                    DATE_val = split_txt[DATE_idx + 1]
                                    DATE_val_refined = DATE_val
                                    for year in range(2016, 2022):
                                        if str(year) in DATE_val:
                                            idx = DATE_val.find(str(year))
                                            DATE_val_refined = DATE_val[:idx + 4]
                                            break

                        else:
                            NO_val = ' '.join(new_split_txt[NO_idx + len(NO_keyword.split(' ')) + 1:DATE_idx])
                            # TODO Clean NO_val

                            if DATE_idx < len(new_split_txt) - 1:
                                DATE_val = new_split_txt[DATE_idx + 1]
                                DATE_val_refined = DATE_val
                                for year in range(2016, 2022):
                                    if str(year) in DATE_val:
                                        idx = DATE_val.find(str(year))
                                        DATE_val_refined = DATE_val[:idx + 4]
                                        break
                            else:
                                kk = 1
                                # TODO find DATE_val
                else:
                    # TODO find in next boxes
                    kk = 1
        info['table_data'].append(('LC value', LC_val))
        info['table_data'].append(('Contract No', NO_val))
        info['table_data'].append(('Date', DATE_val_refined))
    elif is_ky_quy_first_page:
        print('extract_giayLC_by_rules. Doc type: Giay ky quy')
        info, _ = extract_giay_ky_quy(save_dir, combined_list)

    else:  # page 2 or page 3 of LC
        print('extract_giayLC_by_rules. Doc type: Giay LC page 2 or 3')
        third_page_keyword = 'người đại diện hợp pháp/'
        is_third_page = False
        for i in range(5, len(texts) - 1):
            # print(texts[i])
            is_third_page, nearest_text, min_dis, start_idx = find_nearest_sub_str_by_word(texts[i], third_page_keyword,
                                                                                           thr=0.15)
            if is_third_page:
                break
        if is_third_page:
            ACC_pre = 'saving account no:'
            for i in range(2, len(texts) - 1):
                split_txt = texts[i].replace('  ', ' ').replace('  ', ' ').split(' ')
                is_find_pre, nearest_text, min_dis, start_idx = find_nearest_sub_str_by_word(texts[i], ACC_pre,
                                                                                             thr=0.15)
                if is_find_pre:
                    ACC_val = split_txt[start_idx + len(ACC_pre.split(' ')):start_idx + len(ACC_pre.split(' ')) + 1]
        info['table_data'].append(('Block account no', ACC_val))

    for pair in info['table_data']:
        print(pair[0], pair[1])

    return info


def extract_giayLC(save_dir, combined_list):
    info = extract_giayLC_by_rules(save_dir, combined_list)
    print('----------giayLC-------------')
    return info, []
