from main_app.utils.string_rule_based import get_value_from_str


def extract_information(combined_list, document_type):
    """
    Extract information by template method
    1. Find keyword
    2. find content
    3. stop by other keyword
    4. extract information from content
    :param combined_list: [line, line,...]
    line = [bbox, bbox, ...]
    bbox = [x1, x2, y1, y2, cy, h, text, score]
    :param document_type: document type --> get list keyword
    :return: keywrod defined by document type
    """
    texts = []
    kept_ids = []
    texts_column_id = []
    start_x = min([min([bb[0] for bb in line]) for line in combined_list])
    end_x = max([max([bb[1] for bb in line]) for line in combined_list])
    for line in combined_list:
        line = sorted(line, key=lambda x: x[0])
        for i, bb in enumerate(line):
            texts.append(bb[6])
            x1 = bb[0]
            if x1 < start_x + (end_x - start_x) * 0.02:
                texts_column_id.append(0)
            else:
                texts_column_id.append(1)

    # get keyword
    info = {}
    info_keywords = {}
    break_keywords = metadata[document_type]['break_keywords']['keywords']
    for k in metadata[document_type].keys():
        if k == 'break_keywords':
            continue
        for kw in metadata[document_type][k]['keywords']:
            info_keywords[kw] = k

    # extract information by template
    start = 0
    end = len(texts)
    while start < end:
        # find place contain keywords
        idx = string_contains_word(texts[start], info_keywords.keys(), is_vp_bank=False)
        if idx == -1:
            start += 1
            continue
        kept_ids.append(start)
        text = texts[start]
        if not text.__contains__(':'):
            text += ':'
        # find all content of found keyword, stop when found other keyword
        for i in range(start + 1, len(texts)):
            start = i
            if string_contains_word(texts[i], info_keywords.keys(), is_vp_bank=False) >= 0 or \
                    string_contains_word(texts[i], break_keywords, is_vp_bank=False) >= 0:
                break
            kept_ids.append(i)
            text += ' ' + texts[i].strip()
        # extract keyword and content
        if idx >= 0:
            k = info_keywords[list(info_keywords.keys())[idx]]
            if text.__contains__(':') and k not in info:
                val = text[text.find(':') + 1:]
                if val.strip() != '':
                    info[k] = val.strip()

    return info, kept_ids


def extract_general_by_rules(save_dir, combined_list):
    texts = []
    info = {'raw_data': {}, 'table_data': [], 'document_type': 'sale_contract'}
    for line in combined_list:
        line = sorted(line, key=lambda x: x[0])
        for bb in line:
            texts.append(bb[6])

    for i, text in enumerate(texts):
        status, res_key, res_val = get_value_from_str(text)
        if status:
            info['table_data'].append([res_key, res_val])

    print('Result key-value:')
    print(info['table_data'])
    return info


def extract_general(save_dir, combined_list):
    info = extract_general_by_rules(save_dir, combined_list)
    return info, []
