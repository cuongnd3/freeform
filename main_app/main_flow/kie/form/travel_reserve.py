from main_app.utils.string_rule_based import strip_punctuation_from_str, find_nearest_sub_str_by_word, \
    norm_dist_between_2_str, refined_string
from main_app.utils.document import text_box
from main_app.main_flow.kie.rules import find_text_box_by_keywords, find_text_box_value_from_bottom, \
    find_value_in_text_box_or_nearby, find_text_box_in_same_row, find_text_box_in_same_column, find_text_box_contain_str

order_key = []
map_key = {}


def insert_char(string, index, char=' '):
    return string[:index] + char + string[index:]


def extract_travel_reserve_by_rules(doc):
    # WARNING: extract information only on first page
    page = doc.list_pages[0]  # only extract first page
    list_text_box = page.list_text_boxes

    key_value_header = {'Họ tên': 'N/A', 'Mã đặt chỗ':'N/A'}

    prepare_keywords = ['PREPARED FOR', 'ĐÃ CHUẨN BỊ CHO']

    # find passenger name and reservation code

    prepare_tb_idx, key, _ = find_text_box_by_keywords(page,
                                                       list_keywords=prepare_keywords,
                                                       single=True,
                                                       lower=True,
                                                       location='left',
                                                       max_number=1)
    en = True if key[0] == 'PREPARED FOR' else False

    reservation_keywords = ['RESERVATION CODE'] if en else ['MÃ ĐẶT CHỖ']
    reservation_tb_idx, _, sub_str = find_text_box_by_keywords(page,
                                                               list_keywords=reservation_keywords,
                                                               single=False,
                                                               lower=True,
                                                               location='left',
                                                               from_idx=prepare_tb_idx[0],
                                                               max_number=1)

    list_passenger_name_idx = find_text_box_value_from_bottom(doc.list_pages[0],
                                                              prepare_tb_idx[0],
                                                              align='left',
                                                              end_idx=reservation_tb_idx[0])

    # assign passenger name
    key_value_header['Họ tên'] = [] if len(list_passenger_name_idx) > 0 else 'N/A'
    for idx in list_passenger_name_idx:
        key_value_header['Họ tên'].append(list_text_box[idx].value.upper())
    key_value_header['Họ tên'] = '\n'.join(key_value_header['Họ tên'])

    # assign reservation code
    key_value_header['Mã đặt chỗ'] = find_value_in_text_box_or_nearby(page,
                                                               text_box_key_idx=reservation_tb_idx[0],
                                                               text_box_key=sub_str[0],
                                                               value_type='serial')
    key_value_header['Mã đặt chỗ'] = key_value_header['Mã đặt chỗ'].upper()

    # assign date time
    departure_keywords = ['DEPARTURE:'] if en else ['KHỞI HÀNH:', 'KHỞIHÀNH:']

    list_departure_idx = find_text_box_contain_str(page,
                                                   list_find_str=departure_keywords,
                                                   thr=0.21,
                                                   max_number=2,
                                                   position='left',
                                                   lower=True)
    en_month = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']
    vn_month = ['THÁNG 1', 'THÁNG 2', 'THÁNG 3', 'THÁNG 4', 'THÁNG 5', 'THÁNG 6', 'THÁNG 7', 'THÁNG 8', 'THÁNG 9',
                'THÁNG 10', 'THÁNG 11', 'THÁNG 12']
    list_month = en_month if en else vn_month

    list_end_idx = []
    for i in range(1, len(list_departure_idx)):
        list_end_idx.append(list_departure_idx[i])
    list_end_idx.append(-1)

    list_from_idx = [reservation_tb_idx[0]]
    for i in range(1, len(list_departure_idx)):
        list_from_idx.append(list_departure_idx[i])

    list_key_value_body=[]
    for idx, departure_idx in enumerate(list_departure_idx):

        key_value_body = {'Mã chuyến bay': 'N/A', 'Điểm đi': 'N/A', 'Điểm đến': 'N/A', 'Thời gian đi': 'N/A',
                          'Thời gian đến': 'N/A'}
        departure_str = list_text_box[list_departure_idx[idx]].value.split(':')
        date_time_info = departure_str[1:]  # <-----------------

        list_date_time = []
        for month in list_month:
            for date_time_str in date_time_info:
                check_key = find_nearest_sub_str_by_word(date_time_str, month, thr=0.01)
                if check_key[0]:
                    value = ' '.join(date_time_str.split(' ')[:check_key[3] + len(month.split(' '))])
                    list_date_time.append(value.upper())
                    # value = date_time_str[:check_key[3]+len()]

        # assign location
        vna_keyword = ['VIETNAM AIRLINES']
        vna_tb_idx, _, _ = find_text_box_by_keywords(page,
                                                     list_keywords=vna_keyword,
                                                     single=True,
                                                     lower=True,
                                                     similarity_thres=0.1,
                                                     from_idx=list_from_idx[idx],
                                                     end_idx=list_end_idx[idx],
                                                     max_number=1)

        list_flight_idx = find_text_box_in_same_column(page,
                                                          vna_tb_idx[0],
                                                          position='bottom',
                                                          align='left',
                                                          max_number=1)

        key_value_body['Mã chuyến bay'] = list_text_box[list_flight_idx[0]].value


        list_location_idx = find_text_box_in_same_row(page,
                                                      vna_tb_idx[0],
                                                      position='right',
                                                      max_number=2,
                                                      distance_h_ths=0.2)

        key_value_body['Điểm đi'] = refined_string(list_text_box[list_location_idx[0]].value)
        key_value_body['Điểm đến'] = refined_string(list_text_box[list_location_idx[1]].value)

        # assign depart, arrive
        departing_keywords = ['Departing at', 'Deparing at'] if en else ['Giờ khởi hành']
        departing_tb_idx, _, _ = find_text_box_by_keywords(page,
                                                           list_keywords=departing_keywords,
                                                           single=True,
                                                           lower=True,
                                                           similarity_thres=0.1,
                                                           from_idx=vna_tb_idx[0],
                                                           end_idx=list_end_idx[idx],
                                                           max_number=1)

        list_departing_idx = find_text_box_in_same_column(page,
                                                          departing_tb_idx[0],
                                                          position='bottom',
                                                          align='left',
                                                          max_number=1)

        arriving_keywords = ['Arriving at', 'Ariving at'] if en else ['Giờ đến']
        arriving_tb_idx, _, _ = find_text_box_by_keywords(page,
                                                          list_keywords=arriving_keywords,
                                                          single=True,
                                                          lower=True,
                                                          similarity_thres=0.1,
                                                          from_idx=vna_tb_idx[0],
                                                          end_idx=list_end_idx[idx],
                                                          max_number=1)

        list_arriving_idx = find_text_box_in_same_column(page,
                                                         arriving_tb_idx[0],
                                                         position='bottom',
                                                         align='left',
                                                         max_number=1)

        key_value_body['Thời gian đi'] = list_text_box[list_departing_idx[0]].value + ' ' + list_date_time[0]
        if len(list_date_time) == 1:
            key_value_body['Thời gian đến'] = list_text_box[list_arriving_idx[0]].value + ' ' + list_date_time[0]
        else:
            key_value_body['Thời gian đến'] = list_text_box[list_arriving_idx[0]].value + ' ' + list_date_time[1]
        list_key_value_body.append(key_value_body)

    merge_key_value_body={}
    prefix = 'Chuyến '
    for i in range(len(list_key_value_body)):
        for key in list_key_value_body[i].keys():
            merge_key_value_body[' '.join([prefix,str(i+1),key])] = list_key_value_body[i][key]

    key_value_header.update(merge_key_value_body)

    doc.list_pages[0].key_value = key_value_header

    # merge all key_value from page
    for page in doc.list_pages:
        doc.key_value.update(page.key_value)


    # return data for table display
    doc.table_data = []
    if len(order_key)>0:
        for order in order_key:
            if order in doc.key_value.keys():
                v = doc.key_value[order]
                doc.table_data.append([order, v])
    else:
        for key in doc.key_value.keys():
            doc.table_data.append([key, doc.key_value[key]])


    print(doc.list_pages[0].key_value)