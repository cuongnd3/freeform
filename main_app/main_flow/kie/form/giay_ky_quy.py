from main_app.utils.string_rule_based import get_value_from_str, \
    find_nearest_sub_str_by_word, find_nearest_sub_str, norm_dist_between_2_str, \
    strip_punctuation_from_str, check_number_in_str
from main_app.utils.common import euclidean_distance


def extract_giay_ky_quy_by_rules(save_dir, combined_list):
    texts = []
    coors = []
    info = {'raw_data': {}, 'table_data': [], 'document_type': 'giay_ky_quy'}
    for line in combined_list:
        line = sorted(line, key=lambda x: x[0])
        for bb in line:
            texts.append(bb[6])
            coors.append([bb[0], bb[1], bb[2], bb[3]])  # x1,y1,x2,y2

    ACLK_val = ''
    ACLK_keyword = 'ACLK:'
    ACLK_idx = 0
    ACLK_txt_idx = -1
    for i in range(10, len(texts) - 1):
        # print(texts[i])
        split_txt = texts[i].replace('  ', ' ').replace('  ', ' ').split(' ')
        is_find_pre, nearest_text, min_dis, start_idx = find_nearest_sub_str_by_word(texts[i], ACLK_keyword, thr=0.21)

        if is_find_pre:
            if start_idx + 1 < len(split_txt):
                ACLK_val = split_txt[start_idx + 1]
            else:
                ACLK_val = find_value_beside(texts, coors, i)
            ACLK_txt_idx = i
            break
    info['table_data'].append(('Số ACLK', ACLK_val))

    LC_val = ''
    LC_keyword = 'LC trị giá'
    HDNT_val = ''
    HDNT_keyword = 'theo HĐNT số:'
    date_keyword = 'ngày'
    LC_txt_idx = -1

    for i in range(ACLK_txt_idx + 1, len(texts) - 1):
        # print(texts[i])
        split_txt = texts[i].replace('  ', ' ').replace('  ', ' ').split(' ')
        is_find_LC, nearest_text, min_dis, LC_idx = find_nearest_sub_str_by_word(texts[i], LC_keyword, thr=0.21)
        if is_find_LC:
            is_find_HDNT, nearest_text, min_dis, HDNT_idx = find_nearest_sub_str_by_word(texts[i], HDNT_keyword,
                                                                                         thr=0.21, from_idx=LC_idx)
            LC_txt_idx = i
            if is_find_HDNT:
                LC_val = ' '.join(split_txt[LC_idx + len(LC_keyword.split(' ')):HDNT_idx])
                is_find_date, nearest_text, min_dis, date_idx = find_nearest_sub_str_by_word(texts[i], date_keyword,
                                                                                             thr=0.21,
                                                                                             from_idx=HDNT_idx)
                if is_find_date:
                    HDNT_val = ' '.join(split_txt[HDNT_idx + len(HDNT_keyword.split(' ')):date_idx])
                else:
                    HDNT_val = ' '.join(split_txt[HDNT_idx + len(HDNT_keyword.split(' ')):len(texts[i]) - 1])
            else:
                if LC_idx + len(LC_keyword.split(' ')) < len(split_txt):
                    LC_val = ' '.join(split_txt[LC_idx + len(LC_keyword.split(' ')):])
                else:
                    LC_val = find_value_beside(texts, coors, i)

            LC_val = strip_punctuation_from_str(LC_val)
            break

    info['table_data'].append(('LC trị giá', LC_val))
    info['table_data'].append(('HĐNT số', HDNT_val))

    ACC_val = ''
    ACC_keyword = 'tài khoản thu phí'
    ACC_no_keyword = 'số:'
    ACC_txt_idx = -1

    for i in range(LC_txt_idx + 1, len(texts) - 1):
        is_find_ACC, nearest_text, min_dis, ACC_idx = find_nearest_sub_str_by_word(texts[i], ACC_keyword, thr=0.21)
        if is_find_ACC:
            ACC_txt_idx = i
            is_find_ACC_no, nearest_text, min_dis, ACC_no_idx = find_nearest_sub_str(texts[i], ACC_no_keyword, thr=0.21,
                                                                                     unicode=False)
            if is_find_ACC_no and ACC_no_idx + len(ACC_no_keyword) + 1 < len(texts[i]):
                val = texts[i][ACC_no_idx + len(ACC_no_keyword):]
                if check_number_in_str(val):
                    ACC_val = val
            if ACC_val == '':
                ACC_val = find_value_beside(texts, coors, i)
            ACC_val = strip_punctuation_from_str(ACC_val.replace('số', ''))
            break

    info['table_data'].append(('Số tài khoản thu phí', ACC_val))

    exchange_val = ''
    exchange_keyword = 'tỷ giá'
    # exchange_no_keyword = 'số:'
    exchange_txt_idx = -1

    for i in range(ACC_txt_idx + 1, len(texts) - 1):
        is_find_exchange, nearest_text, min_dis, exchange_idx = find_nearest_sub_str_by_word(texts[i], exchange_keyword,
                                                                                             thr=0.21)
        if is_find_exchange:
            if check_number_in_str(texts[i]):
                exchange_val = texts[i]
            else:
                exchange_val = find_value_beside(texts, coors, i)
            exchange_txt_idx = i
            break
    exchange_val_refined = exchange_val
    for idx, ch in enumerate(exchange_val):
        if ch.isdigit():
            exchange_val_refined = exchange_val[idx:idx + 5]
            break

    info['table_data'].append(('Tỷ giá VNĐ/USD', exchange_val_refined))

    dich_vu_val = ''
    dich_vu_keyword = 'DỊCH VỤ'
    dich_vu_txt_idx = -1
    dien_phi_val = ''
    dien_phi_keyword = 'ĐIỆN PHÍ'
    dien_phi_txt_idx = -1

    for i in range(exchange_txt_idx + 1, len(texts) - 1):
        dist = norm_dist_between_2_str(dich_vu_keyword, texts[i])
        if dist < 0.2:
            dich_vu_val = find_value_below(texts, coors, i)
            dich_vu_txt_idx = i
            break

    for i in range(dich_vu_txt_idx + 1, len(texts) - 1):
        dist = norm_dist_between_2_str(dien_phi_keyword, texts[i])
        if dist < 0.2:
            dien_phi_val = find_value_below(texts, coors, i)
            dien_phi_txt_idx = i
            break

    info['table_data'].append(('Số tiền dịch vụ', dich_vu_val))
    info['table_data'].append(('Số tiền điện phí', dien_phi_val))

    last_block_keywords = ['Mã KH', 'MÃ CV.KH', 'Mã HMPH LC', 'Facility ID', 'Pre-limit activation']
    start_idx = dien_phi_txt_idx
    for idx, keyword in enumerate(last_block_keywords):
        res, res_idx = find_value_last_block(keyword, texts, coors, start_idx)
        res = strip_punctuation_from_str(res)
        info['table_data'].append((keyword, res))
        if res_idx != -1:
            start_idx = res_idx
        # print(keyword, res)

    for pair in info['table_data']:
        print(pair[0], pair[1])
    return info


def find_value_last_block(keyword, texts, coors, start_idx):
    for i in range(start_idx + 1, len(texts) - 1):
        is_find, nearest_text, min_dis, find_idx = find_nearest_sub_str_by_word(texts[i], keyword, thr=0.21)
        split_txt = texts[i].replace('  ', ' ').replace('  ', ' ').split(' ')
        if is_find:
            if find_idx + len(keyword.split(' ')) < len(split_txt):
                val = ' '.join(split_txt[find_idx + len(keyword.split(' ')):])
            else:
                val = find_value_beside(texts, coors, i)
            return val, i
    return '', -1


def find_value_below(texts, coors, start_idx=0):
    key_coor = coors[start_idx]
    center_key = [(key_coor[0] + key_coor[2]) / 2, (key_coor[1] + key_coor[3]) / 2]
    limit_align = key_coor[3] - key_coor[1]
    limit_dist = 4 * (key_coor[3] - key_coor[1])
    for i in range(start_idx + 1, len(texts) - 1):
        center_box = [(coors[i][0] + coors[i][2]) / 2, (coors[i][1] + coors[i][3]) / 2]
        x_diff = abs(center_box[0] - center_key[0])
        dist = euclidean_distance(center_key, center_box)
        if x_diff < limit_align and dist < limit_dist:
            return texts[i]
    return ''


def find_value_beside(texts, coors, start_idx=0):
    key_coor = coors[start_idx]
    center_key = [(key_coor[0] + key_coor[2]) / 2, (key_coor[1] + key_coor[3]) / 2]
    limit_align = key_coor[3] - key_coor[1]
    limit_dist = 8 * (key_coor[3] - key_coor[1])
    for i in range(start_idx + 1, len(texts) - 1):
        center_box = [(coors[i][0] + coors[i][2]) / 2, (coors[i][1] + coors[i][3]) / 2]
        y_diff = abs(center_box[1] - center_key[1])
        dist = coors[i][0] - key_coor[2]
        if y_diff < limit_align and dist < limit_dist:
            return texts[i]
    return ''




def extract_giay_ky_quy(save_dir, combined_list):
    info = extract_giay_ky_quy_by_rules(save_dir, combined_list)
    print('----------giay_ky_quy-------------')
    return info, []
