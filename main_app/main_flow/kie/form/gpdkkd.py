import unidecode
import os
import cv2
import glob
import re
from PIL import Image
from main_app.utils.pdf_processing import extract_pdf_image
from main_app.tools import test_paddle_ocr
from main_app.utils.string_rule_based import string_contains_word, find_nearest_sub_str
from main_app.config.general import font_path

this_dir = os.path.dirname(__file__)

break_keywords = ['Tên địa điểm kinh doanh', 'Ten van phong', 'Tên công ty', 'Tên chi nhánh', 'Tên doanh nghiệp',
                  'Địa chỉ', 'Mã số thuế', 'Ten cu']
break_keywords += ['Nganh nghe', 'Nganh, nghe', 'kinh doanh']
break_keywords += ['Ma so', 'Dien thoai', 'Tel', 'Fax', 'Website', 'Email', 'Ten dia diem', 'Von', 'Bang chu']
break_keywords += ['Nguoi dai dien', 'Thong tin', 'Ten to chuc', 'Ho va ten', 'Gioi tinh',
                   'Sinh ngay', 'Ngay Sinh', 'Quoc tich', 'Loai giay to', 'So giay', 'Ngay cap', 'Noi cap', 'Noi ap',
                   'Noi dang ky', 'Noi dang ki', 'Cho o', 'Chuc danh', 'Dan toc', 'Dan oc',
                   'Truong phong', 'Dang ky', 'Cap ngay']

order_key = [('company_name', 'Tên công ty'),
             ('company_name_eng', 'Company name'),
             ('company_name_short', 'Viết tắt'),
             ('company_id', 'Mã số doanh nghiệp'),
             ('company_tax_id', 'Mã số thuế'),
             ('company_type', 'Loại hình'),
             ('company_capital', 'Vốn điều lệ'),
             ('company_tel', 'Số điện thoại'),
             ('company_email', 'Email'),
             ('company_fax', 'Fax'),
             ('company_location', 'Địa chỉ'),
             ('boss_name', 'Họ và tên'),
             ('boss_position', 'Chức vụ'),
             ('boss_id_card', 'Số CMT/CCCD'),
             ('boss_issue_date', 'Ngày cấp'),
             ('boss_issue_place', 'Nơi cấp'),
             ('boss_birthday', 'Ngày sinh'),
             ('boss_sex', 'Giới tính'),
             ('boss_ethnic', 'Dân tộc'),
             ('boss_nationality', 'Quốc tịch'),
             ('boss_permanent_add', 'Hộ khẩu'),
             ('boss_current_add', 'Hiện tại')
             ]


def extract_company_name(combined_list):
    """
    Extract company name
    :param combined_list: [line, line,...]
    line = [bbox, bbox, ...]
    bbox = [x1, x2, y1, y2, cy, h, text, score]
    :return:
    {'company_name_eng': 'tiếng nước ngoài',
    'company_name_short': 'viết tắt',
    'company_name': '',
    'company_type': ''}
    }
    """
    texts = []
    kept_ids = []
    for line in combined_list:
        line = sorted(line, key=lambda x: x[0])
        for bb in line:
            texts.append(bb[6])
            print(bb[6])

    keywords = ['Tên địa điểm kinh doanh', 'Ten van phong', 'Tên công (y', 'Tên công y', 'Tên công ty', 'Tên chi nhánh',
                'Tên doanh nghiệp']

    sub_keywords = {'company_name_eng': 'tiếng nước ngoài',
                    'company_name_short': 'viết tắt',
                    'company_name': ''}
    company = {}
    start = 0
    while start < len(texts):
        if string_contains_word(texts[start].split(':')[0], keywords) == -1:
            start += 1
            continue
        kept_ids.append(start)
        text = texts[start]
        if not text.__contains__(':'):
            text += ':'
        for i in range(start + 1, len(texts)):
            start = i
            if string_contains_word(texts[i].split(':')[0], keywords) >= 0 or string_contains_word(
                    texts[i].split(':')[0], break_keywords) >= 0:
                break
            kept_ids.append(i)
            text += ' ' + texts[i].strip()
        for k, sub_keyword in sub_keywords.items():
            if unidecode.unidecode(text).replace(' ', '').upper().__contains__(
                    unidecode.unidecode(sub_keyword).replace(' ', '').upper()):
                if text.__contains__(':') and k not in company:
                    val = text[text.find(':') + 1:]
                    if val.strip() != '':
                        company[k] = val.upper()
                break

    company_type_keywords = {'trach nhiem huu han': 'TNHH',
                             'TNHH': 'TNHH',
                             'Co phan': 'CP',
                             'CP': 'CP',
                             'C P': 'CP',
                             'JSC': 'CP',
                             }

    company_name = ' '.join([v for k, v in company.items()])
    ct_idx = string_contains_word(company_name, list(company_type_keywords.keys()))
    if ct_idx >= 0:
        company['company_type'] = company_type_keywords[list(company_type_keywords.keys())[ct_idx]]
    else:
        company['company_type'] = ''
    return company, kept_ids


def extract_company_info(combined_list):
    """
    Extract company info
    :param combined_list: [line, line,...]
    line = [bbox, bbox, ...]
    bbox = [x1, x2, y1, y2, cy, h, text, score]
    :return:
    {'company_location': '',
    'company_tel': '',
    'company_fax': '',
    'company_email': '',
    'company_website': '',
    'company_id': '',
    'company_capital': ''
    }
    """
    texts = []
    kept_ids = []
    for line in combined_list:
        line = sorted(line, key=lambda x: x[0])
        for i, bb in enumerate(line):
            texts.append(bb[6])
    company_info = {}
    company_info_keywords = {'Dia chi': 'company_location',
                             'Fax': 'company_fax',
                             'Dien thoai': 'company_tel',
                             'Email': 'company_email',
                             'Website': 'company_website',
                             'Ma so': 'company_id',
                             'Von dieu le': 'company_capital'
                             }
    # find company info
    start = 0
    while start < len(texts):
        # find place contain keywords
        idx = string_contains_word(texts[start].split(':')[0], company_info_keywords.keys())
        if idx == -1:
            start += 1
            continue
        kept_ids.append(start)
        text = texts[start]
        if not text.__contains__(':'):
            text += ':'
        # find all content of found keyword, stop when found other keyword
        for i in range(start + 1, len(texts)):
            start = i
            if string_contains_word(texts[i].split(':')[0], company_info_keywords.keys()) >= 0 or \
                    string_contains_word(texts[i].split(':')[0], break_keywords) >= 0:
                break
            kept_ids.append(i)
            text += ' ' + texts[i].strip()
        # extract keyword and content

        if idx >= 0:
            k = company_info_keywords[list(company_info_keywords.keys())[idx]]
            if text.__contains__(':') and k not in company_info:
                val = text[text.find(':') + 1:]
                if k == 'company_capital':
                    if val.__contains__('('):
                        val = val[:val.find('(')]
                if val.strip() != '':
                    company_info[k] = val.strip()
                if k == 'company_id':
                    exits, nearest_text, min_dis, start_idx = find_nearest_sub_str(text, 'Ma so doanh nghiep')
                    if exits:
                        company_info['company_tax_id'] = val.strip()
    if company_info.__contains__('company_capital'):
        company_info['company_capital'] = company_info['company_capital'].replace(',', '.')
    return company_info, kept_ids


def extract_company_boss_info(combined_list):
    """
    Extract company boss
    :param combined_list: [line, line,...]
    line = [bbox, bbox, ...]
    bbox = [x1, x2, y1, y2, cy, h, text, score]
    :return:
    {'boss_name': '',
    'boss_sex': ''
    'boss_id_card': '',
    'boss_issue_date': '',
    'boss_issue_place': '',
    'boss_birthday': '',
    'boss_nationality': '',
    'boss_permanent_add': '',
    'boss_current_add': '',
    'boss_position': ''
    }
    """
    texts = []
    kept_ids = []
    texts_column_id = []
    start_x = min([min([bb[0] for bb in line]) for line in combined_list])
    end_x = max([max([bb[1] for bb in line]) for line in combined_list])
    for line in combined_list:
        line = sorted(line, key=lambda x: x[0])
        for i, bb in enumerate(line):
            texts.append(bb[6])
            x1 = bb[0]
            if x1 < start_x + (end_x - start_x) * 0.02:
                texts_column_id.append(0)
            else:
                texts_column_id.append(1)

    boss_info = {}
    boss_info_keywords = {
        'Ho va ten': 'boss_name',
        'Loai giay to': 'boss_id_type',
        'Gioi tinh': 'boss_sex',
        'So giay': 'boss_id_card',
        'Ngay cap': 'boss_issue_date',
        'Noi cap': 'boss_issue_place',
        'Noi ap': 'boss_issue_place',
        'Chuc danh': 'boss_position',
        'Sinh Ngay': 'boss_birthday',
        'Ngay sinh': 'boss_birthday',
        'Quoc tich': 'boss_nationality',
        'Dan toc': 'boss_ethnic',
        'thuong tru': 'boss_permanent_add',
        'ho khau': 'boss_permanent_add',
        'Cho o hien tai': 'boss_current_add'
    }
    # find boss info location
    strictly_boss_keyword = ['Ho va ten', 'Gioi tinh', 'Ngay sinh', 'Quoc tich']
    start = 0
    end = len(texts)
    list_check_texts = []
    for i, text in enumerate(texts):
        if (string_contains_word(text.split(':')[0], break_keywords) >= 0 and text[0].isnumeric()) or i == len(
                texts) - 1:
            check_text = ' '.join(list_check_texts)
            if string_contains_word(check_text, strictly_boss_keyword) >= 0:
                end = i
                start = end - len(list_check_texts)
                break
            list_check_texts = [text]
        else:
            if len(list_check_texts) > 0:
                list_check_texts.append(text)

    while start < end:
        # find place contain keywords
        idx = string_contains_word(texts[start].split(':')[0], boss_info_keywords.keys())
        if idx == -1:
            start += 1
            continue
        else:
            k = boss_info_keywords[list(boss_info_keywords.keys())[idx]]
            if k == 'boss_permanent_add':
                # check bounding box for boss_permanent_add --> only at first column
                if texts_column_id[start] != 0:
                    start += 1
                    continue
        kept_ids.append(start)
        text = texts[start]
        if not text.__contains__(':'):
            text += ':'
        # find all content of found keyword, stop when found other keyword
        for i in range(start + 1, len(texts)):
            start = i
            if string_contains_word(texts[i].split(':')[0], boss_info_keywords.keys()) >= 0 or \
                    string_contains_word(texts[i].split(':')[0], break_keywords) >= 0:
                break
            kept_ids.append(i)
            text += ' ' + texts[i].strip()
        # extract keyword and content
        if idx >= 0:
            k = boss_info_keywords[list(boss_info_keywords.keys())[idx]]
            if text.__contains__(':') and k not in boss_info:
                val = text[text.find(':') + 1:]
                if val.strip() != '':
                    boss_info[k] = val.strip()

    if boss_info.__contains__('boss_issue_place'):
        e = boss_info['boss_issue_place']
        if e[0] == 'C4':
            boss_info['boss_issue_place'] = ' '.join(['CA'] + e[1:])

    for no in [12, 9]:
        if not boss_info.__contains__('boss_id_card'):
            for i, txt in enumerate(texts):
                s = "\d" + "{" + str(no) + "}"
                regex = re.compile(s)
                m = re.search(regex, txt)
                if m is not None:
                    id_no = m.group(0)
                    txt1 = ''
                    txt2 = ''
                    if i > 0:
                        txt1 = texts[i - 1]
                    if i < len(texts) -1:
                        txt2 = texts[i + 1]
                    exits1, _, _, _ = find_nearest_sub_str(txt1, 'So giay')
                    exits2, _, _, _ = find_nearest_sub_str(txt2, 'So giay')
                    if exits1 or exits2:
                        boss_info['boss_id_card'] = id_no
                        break

    return boss_info, kept_ids


def extract_gpdkkd(combined_list):
    dkkd_info = {}
    extracted_kept_ids = []

    info, kept_ids = extract_company_name(combined_list)
    dkkd_info.update(info)
    extracted_kept_ids += kept_ids

    info, kept_ids = extract_company_info(combined_list)
    dkkd_info.update(info)
    extracted_kept_ids += kept_ids

    info, kept_ids = extract_company_boss_info(combined_list)
    dkkd_info.update(info)
    extracted_kept_ids += kept_ids

    info = {'raw_data': dkkd_info, 'table_data': [], 'document_type': 'gpdkkd'}

    # return data for table display
    for order in order_key:
        for k in dkkd_info.keys():
            if order[0] == k:
                v = dkkd_info[k]
                print(order[1], v)
                info['table_data'].append([order[1], v])

    return info, extracted_kept_ids


def extract_gpdkkd_from_pdf(pdf_path):
    list_img, save_dir, list_img_path = extract_pdf_image.get_list_img_from_pdf(pdf_path)
    combined_lists = []
    for img_path in list_img_path:
        img = cv2.imread(img_path)
        det_res, txts, scores, res_img = test_paddle_ocr.ocr(img, vis=False)
        combined_list, img = test_paddle_ocr.group_text_box_to_line(img, det_res, txts, scores)
        combined_lists += combined_list
        cv2.imwrite(img_path + '_res.png', res_img)
    info, _, _, _ = extract_gpdkkd(combined_lists)
    for k, v in info.items():
        print('{}: {}'.format(k, v))
    return info


def extract_gpdkkd_from_img(img_path, debug=True):
    img = cv2.imread(img_path)
    det_res, txts, scores, res_img = test_paddle_ocr.ocr(img, vis=debug)
    combined_list, img = test_paddle_ocr.group_text_box_to_line(img, det_res, txts, scores)
    cv2.imwrite(img_path + '_res.png', res_img)
    info, boxes, txts, scores = extract_gpdkkd(combined_list)

    # draw extracted result
    if debug:
        image = Image.fromarray(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
        draw_img = test_paddle_ocr.draw_ocr_box_txt(
            image,
            boxes,
            txts,
            scores,
            drop_score=0,
            font_path=font_path)
        res_img = draw_img[:, :, ::-1]
        extracted_path = img_path + '_extracted.png'
        cv2.imwrite(extracted_path, res_img)

    for k, v in info.items():
        print('{}: {}'.format(k, v))
    if debug:
        cv2.namedWindow('res', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('res', 720, 1280)
        cv2.imshow('res', img)
        cv2.waitKey(1)
    return info


def return_list_key():
    """
    0	boss_birthday	12/11/1979
1	boss_current_add	Xóm 4, Thôn Hải Bối, Xã Hải Bối, Huyện Đông Anh, Thành phố Hà Nội, Việt Nam
2	boss_ethnic	Kinh
3	boss_id_card	013651540
4	boss_id_type	Chứng minh nhân dân
5	boss_issue_date	25/07/2013
6	boss_issue_place	CA TP Hà Nội
7	boss_name	NGÔ MINH CHIÊN
8	boss_nationality	Việt Nam
9	boss_permanent_add	Xóm 4, Thôn Hải Bối, Xã Hải Bối, Huyện Đông Anh, Thành phô Hà Nội, Việt Nam
10	boss_sex	Nam
11	company_capital	2.000.000.000 đồng
12	company_email	phukiennuoc@tuvenanh.com Weosite:phukiennuoc.com.vn
13	company_fax	02439518637
14	company_id	0108133252
15	company_location	Xóm 4, Thôn Hải Bối, Xã Hải Bối, Huyện Đông Anh, Thành phố Hà Nội, Việt Nam
16	company_name	CÔNG TY TNHH THƯƠNG MẠI TUYÊN ANH
17	company_name_eng	TUYEN ANH TRADING COMPANY LIMITED
18	company_name_short	TUYEN ANH CO.LTD
19	company_tel	0982146946
20	company_type	TNHH

    :return:
    """
    list_key = [('company_name', 'Tên công ty'),
                ('company_name_eng', 'Company name'),
                ('company_name_short', 'Viết tắt'),
                ('company_id', 'Mã số doanh nghiệp'),
                ('company_type', 'Loại hình'),
                ('company_capital', 'Vốn điều lệ'),
                ('company_tel', 'Số điện thoại'),
                ('company_email', 'Email'),
                ('company_fax', 'Fax'),
                ('company_location', 'Địa chỉ'),
                ('boss_name', 'Hộ và tên'),
                ('boss_position', 'Chức vụ'),
                ('boss_id_card', 'Số CMT/CCCD'),
                ('boss_issue_date', 'Ngày cấp'),
                ('boss_issue_place', 'Nơi cấp'),
                ('boss_birthday', 'Ngày sinh'),
                ('boss_sex', 'Giới tính'),
                ('boss_ethnic', 'Dân tộc'),
                ('boss_nationality', 'Quốc tịch'),
                ('boss_permanent_add', 'Hộ khẩu'),
                ('boss_current_add', 'Hiện tại')
                ]
    return []


if __name__ == '__main__':
    # extract_gpdkkd_from_pdf('/data/tiepnh/Data/Viettel/Ngan hang/OCR/Business Registration.pdf')
    # extract_gpdkkd_from_img('/data/tiepnh/Data/Viettel/Ngan hang/DKKD/0d518b9d329bc2c59b8a.jpg')
    extract_gpdkkd_from_img(
        '/data/tiepnh/Data/Viettel/Ngan_hang/DKKD-20210120T074327Z-001/DKKD/DKKD_Tra/0108271862-00001.jpg')
    exit()
    for img_path in sorted(glob.glob('/data/tiepnh/Data/Viettel/Ngan hang/ĐKKD-20210120T074327Z-001/ĐKKD/**/*.jpg')):
        print(img_path)
        if os.path.isfile(img_path + '_extracted.png'):
            continue
        extract_gpdkkd_from_img(img_path)
