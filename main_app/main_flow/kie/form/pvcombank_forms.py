# -*- coding: utf-8 -*-
import json
import requests
import time
import key_information_extraction as kie
from main_app.utils.string_rule_based import norm_dist_between_2_str
from main_app.utils.pdf_processing import extract_pdf_image


list_id_info = [("name", "Họ và tên"),
                ("birthday", "Ngày sinh"),
                ("national", "Quốc tịch"),
                ("id", "Số ID"),
                ("sex", "Giới tính"),
                ("address", "Địa chỉ"),
                ("issue_date", "Ngày cấp"),
                ("issue_by", "Cấp bởi"),
                ("expiry", "Ngày hết hạn"),

                # ("district", "Quận/Huyện"),
                # ("document", "Loại giấy tờ"),
                # ("ethnicity", "Dân tộc"),
                # ("id_check", "Kiểm tra"),
                # ("id_logic", "Kiểm tra logic"),
                # ("id_logic_message", "Thông báo kiểm tra logic"),
                # ("id_type", "Mặt trước/sau"),
                # ("precinct", "Phường/Xã"),
                # ("province", "Tỉnh/Thành Phố"),
                # ("religion", "Tôn giáo"),
                # ("street", "Đường phố"),
                # ("street_name", "Tên đường")
                ]

list_visa_info = [("country", "Quốc gia đến"),
                  ("name", "Họ và tên"),
                  ("birthday", "Ngày sinh"),
                  ("national", "Quốc tịch"),
                  ("id", "Số passport"),
                  ("sex", "Giới tính"),
                  ("expiry", "Ngày hết hạn")]


def get_table_data(res, list_info):
    table_data = []
    for k1, k2 in list_info:
        if res.__contains__(k1):
            table_data.append((k2, res[k1]))
    return table_data


def ocr(api_url, api_key, image_path):
    files = {'image': open(image_path, 'rb')}
    try:
        resp = requests.post(url=api_url, files=files, headers={'key': api_key})
        res = json.loads(resp.text)
    except:
        res = {"result_code": 500}
    return res


def cross_check_result(res):
    """
    Kiểm tra các chéo các thông tin sau
    :param res:
    :return:
    """
    check_mst = 'OK'
    check_visa = 'OK'
    check_id_boss = 'OK'
    if "Giấy phép đăng ký kinh doanh" in res and 'raw_data' in res["Giấy phép đăng ký kinh doanh"]:
        # 1. Kiểm tra giấy phép kinh doanh và giấy chứng nhận mã số thuế
        if 'company_tax_id' not in res["Giấy phép đăng ký kinh doanh"]['raw_data']:
            if "Giấy chứng nhận mã số thuế" not in res or 'raw_data' not in res["Giấy chứng nhận mã số thuế"] or 'tax_payer' not in res["Giấy chứng nhận mã số thuế"]['raw_data']:
                check_mst = 'Không có thông tin về mã số thuế'
            else:
                tax_payer = res["Giấy chứng nhận mã số thuế"]['raw_data']['tax_payer']
                if 'company_name' in res["Giấy phép đăng ký kinh doanh"]['raw_data']:
                    company_name = res["Giấy phép đăng ký kinh doanh"]['raw_data']['company_name']
                else:
                    company_name = ''
                if norm_dist_between_2_str(tax_payer, company_name) >= 0.2:
                    check_mst = 'Tên công ty trong giấy đăng ký kinh doanh và giấy chứng nhận mã số thuế không khớp nhau'
        else:
            company_tax_id = res["Giấy phép đăng ký kinh doanh"]['raw_data']['company_tax_id']
            if "Giấy chứng nhận mã số thuế" in res and 'raw_data' in res["Giấy chứng nhận mã số thuế"] and 'tax_id' in res["Giấy chứng nhận mã số thuế"]['raw_data']:
                tax_id = res["Giấy chứng nhận mã số thuế"]['raw_data']['tax_id']
                if company_tax_id != tax_id:
                    check_mst = 'Mã số thuế trong giấy đăng ký kinh doanh và giấy chứng nhận mã số thuế không khớp nhau'

        # 2. Kiểm tra giấy phép kinh doanh và giấy tờ tùy thân
        if 'boss_id_card' in res["Giấy phép đăng ký kinh doanh"]['raw_data']:
            boss_id_card = res["Giấy phép đăng ký kinh doanh"]['raw_data']['boss_id_card']
            have_boss_id_card = False
            check_card = [("boss_name", "name", "Tên người đại diện trong giấy ĐKKD không khớp giấy tờ tùy thân"),
                          ("boss_sex", "sex", "Giới tính người đại diện trong giấy ĐKKD không khớp giấy tờ tùy thân"),
                          ("boss_birthday", "birthday", "Ngày sinh người đại diện trong giấy ĐKKD không khớp giấy tờ tùy thân"),
                          # ("boss_nationality", "national", "Quốc tịch người đại diện trong giấy ĐKKD không khớp giấy tờ tùy thân"),
                          ("boss_issue_date", "issue_date", "Ngày cấp giấy tờ tùy thân không khớp với ngày cấp trong giấy ĐKKD"),
                          ("boss_issue_place", "issue_by", "Nơi cấp giấy tờ tùy thân không khớp với ngày cấp trong giấy ĐKKD"),
                          ]
            for k in res.keys():
                if k.__contains__("Giấy tờ tùy thân") and 'raw_data' in res[k] and 'id' in res[k]['raw_data']:
                    id_card = res[k]['raw_data']['id']
                    if id_card != boss_id_card:
                        continue
                    have_boss_id_card = True
                    for k1, k2, m in check_card:
                        if k2 in res[k]['raw_data'] and k1 in res["Giấy phép đăng ký kinh doanh"]['raw_data']:
                            if res[k]['raw_data'][k2] == 'N/A':
                                continue
                            if norm_dist_between_2_str(res[k]['raw_data'][k2].replace('/', '').replace('-', ''),
                                                       res["Giấy phép đăng ký kinh doanh"]['raw_data'][k1].replace('/', '').replace('-', '')) >= 0.1:
                                check_id_boss = m
                                break
                    break
            if not have_boss_id_card:
                check_id_boss = 'Không có giấy tờ tùy thân của người đại diện trong giấy phép đăng ký kinh doanh'
        else:
            check_id_boss = 'Không có thông tin người đại diện trong giấy phép đăng ký kinh doanh'
    else:
        check_mst = 'Không có thông tin giấy phép đăng ký kinh doanh'
        check_id_boss = 'Không có thông tin giấy phép đăng ký kinh doanh'

    res['Kiểm tra chéo thông tin'] = {'raw_data': {"check_mst": check_mst,
                                                   "check_visa": check_visa,
                                                   "check_id_boss": check_id_boss},
                                      'table_data': [['Kiểm tra giấy chứng nhận mã số thuế', check_mst],
                                                     ['Kiểm tra VISA', check_visa],
                                                     ['Kiểm tra thông tin chủ sở hữu', check_id_boss]]
                                      }
    return res


def read_all_documents(list_input_data):
    ocr_api_url = 'https://api.cloudekyc.com/v3.2/ocr/recognition'
    ocr_api_key = 'e7k8YzXU8BfexOsUah9PiKltFH6u61Pw'
    # call API
    start = time.time()
    res = {"Giấy phép đăng ký kinh doanh": {},
           "Giấy chứng nhận mã số thuế": {},
           "Visa": {},
           "Giấy tờ tùy thân_00": {},
           "Kiểm tra chéo thông tin": {}}
    if list_input_data['document_gpdkkd'] != '':
        res_gpdkkd = kie.kie_from_file(list_input_data['document_gpdkkd'],
                                                              document_type='business_registration',
                                                              max_pages=3)
        res["Giấy phép đăng ký kinh doanh"] = res_gpdkkd

    start_mst = time.time()
    if list_input_data['document_mst'] != '':
        res_mst = kie.kie_from_file(list_input_data['document_mst'],
                                                           document_type='giay_dkmst')
        res["Giấy chứng nhận mã số thuế"] = res_mst

    start_visa = time.time()
    if list_input_data['document_visa'] != '':
        res_visa = ocr(ocr_api_url , ocr_api_key, list_input_data['document_visa'])
        res["Visa"] = {'raw_data': res_visa,
                       'table_data': get_table_data(res_visa, list_visa_info) }

    start_id = time.time()
    if len(list_input_data['document_ID']) > 0:
        list_img_path = []
        idx = 0
        for img_path in list_input_data['document_ID']:
            if img_path[-4:].lower() == '.pdf':
                images, save_dir, list_img_path_ = extract_pdf_image.get_list_img_from_pdf(img_path)
                list_img_path += list_img_path_[:2]
            else:
                list_img_path += [img_path]

        if 1 <= len(list_img_path) <= 2:
            gttt = {}
            for img_path in list_img_path:
                r = ocr(ocr_api_url , ocr_api_key, img_path)
                if 'result_code' in r and r['result_code'] == 200:
                    if r['id_type'] == '0':
                        for k, v in r.items():
                            if k not in gttt or v != 'N/A':
                                gttt[k] = v
                        if r['document'] in ['CCCD', 'NEW ID', 'OLD ID', 'CHIP ID']:
                            gttt['national'] = "Việt Nam"
                    else:
                        gttt['issue_by'] = r['issue_by']
                        gttt['issue_date'] = r['issue_date']
                        gttt['characteristics'] = r['characteristics']

            res["Giấy tờ tùy thân_{0:02d}".format(idx)] = {'raw_data': gttt,
                                                           'table_data': get_table_data(gttt, list_id_info)}
            idx += 1

    end_id = time.time()
    print(res)

    # cross-check results
    res = cross_check_result(res)

    # return info
    end = time.time()
    res['elapsed_time'] = {"Giấy phép đăng ký kinh doanh(s)": start_mst-start,
                           "Giấy chứng nhận mã số thuế(s)": start_visa-start_mst,
                           "Visa(s)": start_id-start_visa,
                           "Giấy tờ tùy thân(s)": end_id-start_id,
                           "Tổng thời gian(s)": end-start}
    return res