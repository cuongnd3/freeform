from main_app.utils.string_rule_based import get_value_from_str, \
    norm_dist_between_2_str, find_nearest_sub_str_by_word, strip_punctuation_from_str
from main_app.config.general import use_case

order_key = ['Mã Số','Họ và tên', 'Ngày sinh','Địa chỉ']

map_key = {'Mã Số': 'maSo',
           'Họ và tên': 'hoVaTen',
           'Ngày sinh': 'namSinh',
           'Địa chỉ': 'diaChi'}

def remap_key_value_MAFC(key_value):
    res_info = {}
    for key in map_key.keys():
        res_info[map_key[key]] = 'N/A'
    res_info['dcx']=0

    for key in key_value.keys():
        if key in map_key.keys():
            res_info[map_key[key]] = key_value[key]

    res_info['dcx'] = key_value['dcx'] if 'dcx' in key_value.keys() else 0
    return res_info


def check_BHYT_type(list_text_box, keyword='Thẻ hợp lệ!'):
    for idx, text_box in enumerate(list_text_box):
        sub_text = text_box.value[:len(keyword)]
        dist = norm_dist_between_2_str(sub_text, keyword)
        # print(dist, sub_text)
        if dist < 0.21:
            return True, idx
    return False, -1


def classify_BHYT(doc):
    '''
    classify into 3 types: BHYT, BHYT_web hoac other, other gom BHYT_back va cac loai khac
    :param doc:
    :return:
    '''

    BHYT_keywords = {'BHYT_front': ['Mã số', 'Họ và tên'], 'BHYT_web': ['Thẻ hợp lệ!']}
    is_find, page_idx, type, idx = False, -1, 'other', -1
    for page_jdx, page in enumerate(doc.list_pages):
        if is_find:
            break
        for key in BHYT_keywords['BHYT_web']:
            is_find, idx = check_BHYT_type(page.list_text_boxes, keyword=key)
            if is_find:
                page.type = 'BHYT_web'
                type = 'BHYT_web'
                page_idx = page_jdx
                break
        if is_find:
            break
        for key in BHYT_keywords['BHYT_front']:
            is_find, idx = check_BHYT_type(page.list_text_boxes, keyword=key)
            if is_find:
                page.type = 'BHYT_front'
                type = 'BHYT_front'
                page_idx = page_jdx
                break
    return is_find, page_idx, type, idx


def extract_BHYT_by_rules(doc):
    print('----------------------------------extract_BHYT_by_rules---------------------------------')
    key_is_find = {'Mã Số': False, 'Họ và tên': False, 'Ngày sinh': False, 'Địa chỉ': False}
    info = {'raw_data': {}, 'table_data': []}

    key_value = {}

    is_find, page_idx, type, keyword_box_idx = classify_BHYT(doc)
    print('extract_BHYT_by_rules. BHYT type', type)

    texts = []
    confidents = []
    doc_conf = 0
    for text_box in doc.list_pages[page_idx].list_text_boxes:
        left = text_box.list_pts[0][0]
        top = text_box.list_pts[0][1]
        right = text_box.list_pts[1][0]
        bottom = text_box.list_pts[2][1]
        texts.append((text_box.value, [left, top, right, bottom]))
        confidents.append(text_box.conf_rec)

    if type == 'BHYT_web':
        # find name, dOb, address
        info_str = ''
        x_align = texts[keyword_box_idx][1][0]
        x_diff = 20
        total_box_to_combine = 5
        find_box = 0
        block_conf = 0
        for idx in range(keyword_box_idx, len(texts) - 1):
            # print(texts[idx])
            if abs(texts[idx][1][0] - x_align) < x_diff:
                info_str += texts[idx][0] + ' '
                find_box += 1
                block_conf += confidents[idx]
                if find_box == total_box_to_combine:
                    break
        print(info_str)
        info_keys = ['Họ tên:', 'Ngày sinh:', 'Giới tính:', '(ĐC:', 'Nơi KCBBĐ:']
        info_vals = []
        val_start_pos = -1
        val_end_pos = -1
        info_str_words = info_str.replace('  ', ' ').split(' ')
        for key in info_keys:
            check_key = find_nearest_sub_str_by_word(info_str, key, thr=0.21, from_idx=max(val_end_pos, 0))
            val_end_pos = check_key[3]
            if val_start_pos > -1 and val_end_pos > val_start_pos:
                val = ' '.join(info_str_words[val_start_pos:val_end_pos])
                val = strip_punctuation_from_str(val)
                info_vals.append(val)
            val_start_pos = val_end_pos + len(key.split(' '))

            # print(check_key)
        if len(info_vals) > 0:
            key_value['Họ và tên'] = info_vals[0]
        if len(info_vals) > 1:
            key_value['Ngày sinh'] = info_vals[1]
        if len(info_vals) > 3:
            key_value['Địa chỉ'] = info_vals[3]

        # find ma so
        MA_SO_keywords = ['Mã thẻ', 'Mãthẻ', 'Mã thẻ +', 'Mã thẻ -']
        MS_idx = -1
        MS_is_found = False
        for idx, text in enumerate(texts):
            for key in MA_SO_keywords:
                dist = norm_dist_between_2_str(text[0], key)
                # print(dist, text[0], key)
                if dist < 0.21:
                    MS_idx = idx
                    MS_is_found = True
                    break
            if MS_is_found:
                break
        MS_box_center_y = (texts[MS_idx][1][1] + texts[MS_idx][1][3]) / 2
        MS_box_w = abs(texts[MS_idx][1][0] - texts[MS_idx][1][2])
        MS_box_h = abs(texts[MS_idx][1][1] - texts[MS_idx][1][3])
        MS_val = 'N/A'
        MS_val_conf = 0
        for idx in range(MS_idx + 1, len(texts) - 1):
            box_center_y = (texts[idx][1][1] + texts[idx][1][3]) / 2
            y_diff = abs(box_center_y - MS_box_center_y)
            x_diff = abs(texts[idx][1][0] - texts[MS_idx][1][2])
            if y_diff < MS_box_h and x_diff < 3 * MS_box_w:
                # print(texts[idx])
                MS_val = texts[idx][0]
                MS_val_conf = confidents[idx]
                break
        key_value['Mã Số'] = MS_val
        doc_conf = (block_conf + MS_val_conf) / (find_box + 1)
    elif type == 'BHYT_front':
        total_conf = 0
        for i, text in enumerate(texts):
            match = False
            for key in key_is_find.keys():
                if not key_is_find[key] and not match:
                    status, res_key, res_val = get_value_from_str(text[0],
                                                                  keywords=[key],
                                                                  check_key=True)
                    if status:
                        match = True
                        key_is_find[key] = True
                        conf = confidents[i]

                        if key == 'Mã Số':
                            if res_val == '':
                                from main_app.utils.string_rule_based import check_number_in_str
                                if check_number_in_str(texts[i - 1][0], num_len=3):
                                    res_val = texts[i - 1][0]
                                    conf = confidents[i - 1]
                                elif check_number_in_str(texts[i + 1][0], num_len=3):
                                    res_val = texts[i + 1][0]
                                    conf = confidents[i + 1]

                            res_val = res_val.replace(' ', '').replace(' ', '').replace('.', '').replace('-', '')

                        key_value[key] = res_val
                        doc_conf += conf
                        total_conf += 1
        if total_conf > 0:
            doc_conf = doc_conf / total_conf
    else:
        print('Not support')

    # Reformat ma so

    if 'Mã Số' in key_value.keys() and len(key_value['Mã Số']) == 15:
        ms = key_value['Mã Số']
        code, num1, num2, num3, num4, num5 = ms[0:2], ms[2:3], ms[3:5], ms[5:8], ms[8:11], ms[11:]
        key_value['Mã Số'] = ' '.join([code, num1, num2, num3, num4, num5])

    key_value['dcx'] = doc_conf

    doc.list_pages[page_idx].key_value = key_value

    # merge all key_value from page
    for page in doc.list_pages:
        doc.key_value.update(page.key_value)

    # return data for table display
    doc.table_data = []
    if len(order_key) > 0:
        for order in order_key:
            if order in doc.key_value.keys():
                v = doc.key_value[order]
                doc.table_data.append([order, v])
    else:
        for key in doc.key_value.keys():
            doc.table_data.append([key, doc.key_value[key]])

    if use_case == 'MAFC':
        doc.key_value = remap_key_value_MAFC(doc.key_value)

    print(doc.list_pages[0].key_value)