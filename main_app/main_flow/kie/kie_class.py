import cv2
from main_app.utils.document import document


class KIE():
    def __init__(self, model='rule_base'):
        '''

        :param model: sdmgr or rule_base
        '''
        self.model = model
        if model =='rule_base':
            import main_app.main_flow.kie.kie_rule_base as kie
            self.kie = kie
    def inference(self, doc, vis = False):
        if doc is None:
            print('KIE. No input document!')
            return
        if self.model =='rule_base':
            self.kie.extract_document(doc, vis=vis)

if __name__=='__main__':
    from main_app.main_flow.text_det.text_det_class import TextDet
    file_path = '/home/duycuong/home_data/vvn/OCR_MAFC/BHYT/web/imgs/1734149_1729699_BHYT_0_1.png'
    file_path = '/home/duycuong/home_data/vvn/OCR_MAFC/BHYT/normal/1723321_1718968_BHYT_0.pdf'

    #img = cv2.imread(file_path)
    new_doc = document(file_path, type='BHYT', max_pages=2)

    text_detector = TextDet(model='paddle_v11')
    text_detector.inference(new_doc)

    # new_doc.visualize()

    doc_rotator = DocRot(model='mobilenetv3')
    doc_rotator.inference(new_doc, vis=False)

    new_doc.visualize()