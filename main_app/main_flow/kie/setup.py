import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pick",
    version="0.1",
    description="key infor extraction",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
    install_requires=[
        'tabulate==0.8.7',
        'overrides==3.0.0',
        'pandas==1.0.5',
        'allennlp==1.0.0',
        'torchtext==0.6.0',
        'tqdm==4.47.0'
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
