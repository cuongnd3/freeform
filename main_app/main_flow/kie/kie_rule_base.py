from main_app.main_flow.kie.form.BHYT import extract_BHYT_by_rules
from main_app.main_flow.kie.form.TTTB import extract_TTTB_by_rules
from main_app.main_flow.kie.form.travel_reserve import extract_travel_reserve_by_rules
from main_app.main_flow.kie.form.general import extract_information

def extract_document(doc, vis=False):
    if doc.type == 'BHYT':
        extract_BHYT_by_rules(doc)
    elif doc.type == 'TTTB':
        extract_TTTB_by_rules(doc)
    elif doc.type == 'travel_reserve':
        extract_travel_reserve_by_rules(doc)
    else:
        info, kept_ids = extract_information(combined_list, doc.type)
        
    