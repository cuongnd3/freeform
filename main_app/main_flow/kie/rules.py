from main_app.utils.string_rule_based import norm_dist_between_2_str, strip_punctuation_from_str, refined_string, find_nearest_sub_str
from main_app.utils.image_processing import resize_max, distance_from_pts_to_line
from main_app.utils.common import euclidean_distance
import numpy as np


def count_NA_field(key_value):
    count=0
    for key in key_value.keys():
        if isinstance(key,str):
            if key_value[key]=='N/A':
                count+=1
    return count


# rules to check position between 2 text box
def text_box_in_same_line(tb_i, tb_j, distance_h_ths=0.3):
    if tb_i.params == []:
        tb_i.get_params_of_text_box()
    if tb_j.params == []:
        tb_j.get_params_of_text_box()

    angle_i = np.rad2deg(np.arctan2(tb_i.params[0], 1))
    angle_j = np.rad2deg(np.arctan2(tb_j.params[0], 1))

    mean_angle = (angle_i + angle_j) / 2
    slope = np.tan(np.deg2rad(mean_angle))
    center = [(tb_i.center[0] + tb_j.center[0]) / 2, (tb_i.center[1] + tb_j.center[1]) / 2]
    b = center[1] - slope * center[0]

    height_dis_i = distance_from_pts_to_line(tb_i.center[0], tb_i.center[1], slope, -1, b)
    height_dis_j = distance_from_pts_to_line(tb_j.center[0], tb_j.center[1], slope, -1, b)
    height_dis = height_dis_i + height_dis_j
    diff_distance_h = (2 * height_dis) / (tb_i.height + tb_j.height)
    # print(tb_i.value,tb_j.value, diff_distance_h)
    distance_h_cond = diff_distance_h < distance_h_ths
    return distance_h_cond


def text_box_nearby_in_horizontal_line(tb_i, tb_j, distance_w_ths=1.5):
    if tb_i.center == []:
        tb_i.calculate_width_height_center()
    if tb_j.center == []:
        tb_j.calculate_width_height_center()
    diff_w = 2 * (euclidean_distance(tb_i.center, tb_j.center) - ((tb_i.width + tb_j.width) / 2)) / (
            tb_i.height + tb_j.width)
    distance_w_cond = diff_w < distance_w_ths
    return distance_w_cond


def text_box_in_the_right(tb_anchor, tb_to_check):
    if tb_anchor.center == []:
        tb_anchor.calculate_width_height_center()
    if tb_to_check.center == []:
        tb_to_check.calculate_width_height_center()
    return tb_to_check.center[0] > tb_anchor.center[0]

def text_box_in_the_bottom(tb_anchor, tb_to_check):
    if tb_anchor.center == []:
        tb_anchor.calculate_width_height_center()
    if tb_to_check.center == []:
        tb_to_check.calculate_width_height_center()
    return tb_to_check.center[1] > tb_anchor.center[1]

def text_box_in_the_top(tb_anchor, tb_to_check):
    if tb_anchor.center == []:
        tb_anchor.calculate_width_height_center()
    if tb_to_check.center == []:
        tb_to_check.calculate_width_height_center()
    return tb_to_check.center[1] < tb_anchor.center[1]

def find_text_box_by_keywords(page, list_keywords, single=False, lower=False, location='left', similarity_thres=0.21,
                              from_idx=0, end_idx =-1, max_number=1):
    '''
    find key text box, the page.list_text_boxes should be sorted
    :param page:
    :param list_keywords:
    :param single:  text box contain only keyword or can contain other information
    :param lower:
    :param location: 'left' 'right' or 'random' in text box
    :param similarity_thres:
    :param from_idx:
    :param end_idx:
    :param max_number: find 1 or many text box, -1 mean find all
    :return: index of text box and keyword in the text box
    '''
    list_idx = []
    list_keyword = []
    list_tb_sub_str = []
    count = 0

    if end_idx ==-1: end_idx = len(page.list_text_boxes) -1
    for idx, tb in enumerate(page.list_text_boxes):
        if idx < from_idx  or idx > end_idx:
            continue
        if location == 'left':
            for keyword in list_keywords:
                tb_sub_str = tb.value[:len(keyword)]
                if norm_dist_between_2_str(keyword, tb_sub_str, lower=lower) < similarity_thres:
                    list_idx.append(idx)
                    list_keyword.append(keyword)
                    list_tb_sub_str.append(tb_sub_str)
                    count += 1
                    if count >= max_number:
                        return list_idx, list_keyword, list_tb_sub_str

    return list_idx, list_keyword, list_tb_sub_str


def find_text_box_value_from_bottom(page, text_box_key_idx, align='left', end_idx=-1):
    '''
    find value in the bottom of the text box key
    :param page:
    :param text_box_key_idx:
    :param align: 'left' 'right' 'center' or 'random'
    :param end_idx: to stop finding
    :return: list of text box value
    '''
    tb_key = page.list_text_boxes[text_box_key_idx]
    if end_idx == -1:
        end_idx = len(page.list_text_boxes) - 1
    list_text_box_value_idx = []
    for idx, tb in enumerate(page.list_text_boxes):
        if idx <= text_box_key_idx or idx >= end_idx:
            continue
        if align == 'left':
            left_align = tb_key.list_pts[0][0]
            left_align_diff = abs(tb.list_pts[0][0] - left_align)
            if left_align_diff < 40:
                list_text_box_value_idx.append(idx)

    return list_text_box_value_idx


def find_value_in_text_box_or_nearby(page, text_box_key_idx, text_box_key, value_type='serial'):
    '''
    find value in the same box as text_box_key or locate nearby (in horizontal position)
    :param page: 
    :param text_box_key_idx: 
    :param text_box_key: 
    :return: 
    '''
    tb_key = page.list_text_boxes[text_box_key_idx]
    tb_key_value = tb_key.value[len(text_box_key):]

    # value in text box
    if tb_key_value != '':
        value = refined_string(tb_key_value, type=value_type)
    else:  # value nearby
        for tb in page.list_text_boxes:
            same_line = text_box_in_same_line(tb, tb_key)
            nearby = text_box_nearby_in_horizontal_line(tb, tb_key)
            in_the_right = text_box_in_the_right(tb_key, tb)
            if same_line and nearby and in_the_right:
                value = tb.value
                value = refined_string(value, type=value_type)
                break
    return value


def find_text_box_in_same_row(page, text_box_anchor_idx, position='right', max_number=-1, distance_h_ths=0.3):
    '''

    :param page:
    :param text_box_anchor_idx:
    :param position:
    :param max_number:
    :return:
    '''
    tb_anchor = page.list_text_boxes[text_box_anchor_idx]
    list_tb_idx = []
    list_tb = []
    for idx, tb in enumerate(page.list_text_boxes):
        if tb.list_pts[0][1] > tb_anchor.list_pts[3][1]:
            continue
        same_row = text_box_in_same_line(tb, tb_anchor, distance_h_ths=distance_h_ths)
        if position == 'right':
            in_the_right = text_box_in_the_right(tb_anchor, tb)
            if same_row and in_the_right:
                list_tb_idx.append(idx)
                list_tb.append(page.list_text_boxes[idx])

    list_tb_idx = sorted(list_tb_idx, key=lambda x: page.list_text_boxes[x].list_pts[0][0])
    if max_number != -1:
        list_tb_idx = list_tb_idx[:max_number]
    return list_tb_idx


def find_text_box_in_same_column(page, text_box_anchor_idx, align='left', position='bottom', max_number=-1, from_idx =0,
                                 distance_h_ths=0.3):
    '''

    :param page:
    :param text_box_anchor_idx:
    :param align: 'left' 'right' or 'random' (but in same column)
    :param position:
    :param max_number:
    :param from_idx:
    :param distance_h_ths:
    :return:
    '''

    tb_anchor = page.list_text_boxes[text_box_anchor_idx]
    list_tb_idx = []
    list_tb = []
    last_left_x = tb_anchor.list_pts[0][0]
    last_right_x = tb_anchor.list_pts[1][0]
    for idx, tb in enumerate(page.list_text_boxes):
        if idx <from_idx or tb.list_pts[0][0] > tb_anchor.list_pts[1][0] or tb.list_pts[1][0] < tb_anchor.list_pts[0][0]:
            continue
        if tb.center ==[]:
            tb.calculate_width_height_center()
        if align == 'left':
            same_column = abs(tb.list_pts[0][0] - last_left_x) < tb_anchor.height/2
        elif align =='right':
            same_column = abs(tb.list_pts[1][0] - last_right_x) < tb_anchor.height / 2
        else: #random
            same_column = True
        if position == 'bottom':
            condition = text_box_in_the_bottom(tb_anchor, tb)
        else: #top
            condition = text_box_in_the_top(tb_anchor, tb)
        if same_column and condition:
            last_left_x = tb.list_pts[0][0]
            last_right_x = tb.list_pts[1][0]
            list_tb_idx.append(idx)
            list_tb.append(page.list_text_boxes[idx])

    if position =='bottom':
        list_tb_idx = sorted(list_tb_idx, key=lambda x: page.list_text_boxes[x].list_pts[0][1])
    else:
        list_tb_idx = sorted(list_tb_idx, key=lambda x: page.list_text_boxes[x].list_pts[0][1], reverse=True)
    if max_number != -1:
        list_tb_idx = list_tb_idx[:max_number]
    return list_tb_idx


def find_text_box_contain_str(page, list_find_str, thr=0.21, max_number = 1, position = 'left', lower=True, unicode = True):
    '''

    :param page:
    :param list_find_str:
    :param thr:
    :param max_number:
    :param position: 'left' 'right' or 'random'
    :param lower:
    :param unicode:
    :return:
    '''
    list_idx = []
    count =0
    for find_str in list_find_str:
        for idx, tb in enumerate(page.list_text_boxes):
            check_key= find_nearest_sub_str(tb.value, find_str, thr=0.21)
            if check_key[0]:
                if position =='left' and check_key[3]<3:
                    list_idx.append(idx)
                    count+=1
                if position =='right' and check_key[3]+len(check_key[1])>len(tb.value)-3: #todo
                    list_idx.append(idx)
                    count+=1
                if position =='random':
                    list_idx.append(idx)
                    count += 1
                if count >=max_number:
                    return list_idx
    return list_idx



