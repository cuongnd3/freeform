from sklearn.metrics import f1_score
from main_app.utils.common import get_list_file_in_folder, split_idx_coor_val_type_from_pick_anno
import os
from main_app.utils.common import get_list_img_from_PICK_csv


def eval_marco_F1(y_pred, y_gt, labels=None):
    '''

    :param y_pred:
    :param y_gt:
    :param labels:
    :return:
    '''
    return f1_score(y_gt, y_pred, labels=labels, average='macro', zero_division=0)


def eval_marco_F1_PICK(pred_dir, bboxes_and_trans_dir, entity_list, list_file_csv=None, other_cls_name='other'):
    '''

    :param pred_dir:
    :param bboxes_and_trans_dir:
    :param entity_list:
    :param list_file_csv:
    :param other_cls_name:
    :return:
    '''
    list_pred_files = get_list_file_in_folder(pred_dir, ext=['.txt'])
    list_pred_files = sorted(list_pred_files)
    if other_cls_name not in entity_list:
        entity_list.append(other_cls_name)
    print('eval_marco_F1_PICK. Entity list:', entity_list)

    list_files_for_eval = None
    if list_file_csv is not None:
        print('eval_marco_F1_PICK. list file:', list_file_csv)
        list_files_for_eval = get_list_img_from_PICK_csv(list_file_csv)
        list_files_for_eval = [os.path.splitext(f)[0] for f in list_files_for_eval]

    y_pred = []
    y_gt = []
    for idx, pred_file in enumerate(list_pred_files):
        if list_files_for_eval is not None:
            name_wo_ext = os.path.splitext(pred_file)[0]
            if name_wo_ext not in list_files_for_eval:
                # print(idx, pred_file,'ignore!!!')
                continue
        print(idx, pred_file)
        with open(os.path.join(bboxes_and_trans_dir, pred_file.replace('.txt', '.tsv')), mode='r',
                  encoding='utf-8') as f:
            list_anno = f.readlines()

        pred = []
        gt = []
        for anno in list_anno:
            idx, coors, val, type = split_idx_coor_val_type_from_pick_anno(anno)
            if type not in entity_list:
                print("'{}'not in Entity list!".format(type))
                type = other_cls_name
            gt.append(type)
            pred.append(other_cls_name)

        with open(os.path.join(pred_dir, pred_file), mode='r',
                  encoding='utf-8') as f:
            list_pred_anno = f.readlines()
        for pred_anno in list_pred_anno:
            idx, coors, type, val = pred_anno.split('\t')
            pred[int(idx) - 1] = type

        y_pred.extend(pred)
        y_gt.extend(gt)

    entity_list.remove(other_cls_name)
    print('F1 scores of each class.................................................')
    count=0
    total_F1=0
    for entity in entity_list:
        score = eval_marco_F1(y_pred=y_pred,
                              y_gt=y_gt,
                              labels=[entity],
                              )
        print(entity.ljust(20), score)
        if score>-10:
            count+=1
            total_F1 +=score
    print('average F1 in',count,'class:', total_F1/count)
    return score


if __name__ == '__main__':
    pred_dir = '/home/duycuong/home_data/finance_invoices_PICK/output_pick'
    bboxes_and_trans_dir = '/home/duycuong/home_data/finance_invoices_PICK/boxes_and_transcripts'
    list_file_csv='/home/duycuong/home_data/finance_invoices_PICK/val_list.csv'
    # Entities_list = ['VAT_amount', 'VAT_amount_val', 'VAT_rate', 'VAT_rate_val', 'account_no', 'address', 'address_val',
    #                  'amount_in_words', 'amount_in_words_val', 'bank', 'buyer', 'company_name', 'company_name_val',
    #                  'date',
    #                  'exchange_rate', 'exchange_rate_val', 'form', 'form_val', 'grand_total', 'grand_total_val', 'no',
    #                  'no_val', 'seller', 'serial', 'serial_val', 'tax_code', 'tax_code_val', 'total', 'total_val',
    #                  'website', 'other']
    #
    # Entities_list = ['contract_no','exporter_name','exporter_add','importer_name','payment_method',
    #                  'ben_bank_name','ben_add','ben_name','ben_acc','swift_code','other']
    from main_app.kie_models.PICK.config import Entities_list

    eval_marco_F1_PICK(pred_dir=pred_dir,
                       bboxes_and_trans_dir=bboxes_and_trans_dir,
                       list_file_csv=list_file_csv,
                       entity_list=Entities_list)
