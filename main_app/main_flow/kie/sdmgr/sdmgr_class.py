import argparse
import os, time
import os.path as osp

import mmcv
import torch
from mmcv import Config
from mmcv.image import tensor2imgs
from mmcv.parallel import MMDataParallel
from mmcv.runner import load_checkpoint
from mmocr.datasets import build_dataloader, build_dataset
from mmocr.models import build_detector

from sklearn.metrics import f1_score
from main_app.kie_models.sdmgr.config import config_path, \
    ckpt_path, visualize, class_list_path, dict_path

show = False


def eval_marco_F1(y_pred, y_gt, labels=None):
    '''

    :param y_pred:
    :param y_gt:
    :param labels:
    :return:
    '''
    return f1_score(y_gt, y_pred, labels=labels, average='macro', zero_division=0)


class KIE_SDMGR():
    def __init__(self):
        self.cfg = Config.fromfile(config_path)
        # import modules from string list.
        if self.cfg.get('custom_imports', None):
            from mmcv.utils import import_modules_from_strings
            import_modules_from_strings(**self.cfg['custom_imports'])
        # set cudnn_benchmark
        if self.cfg.get('cudnn_benchmark', False):
            torch.backends.cudnn.benchmark = True
        self.cfg.model.pretrained = None

        # build the model and load checkpoint
        self.cfg.model.train_cfg = None
        self.model = build_detector(self.cfg.model, test_cfg=self.cfg.get('test_cfg'))
        print('KIE_SDMGR. load ckpt', ckpt_path)
        load_checkpoint(self.model, ckpt_path, map_location='cpu')
        self.model = MMDataParallel(self.model, device_ids=[0])
        self.model.eval()

        with open(class_list_path, mode='r', encoding='utf-8') as f:
            class_list_txt = f.readlines()
        self.class_list = [classes.split(' ')[1].replace('\n', '') for classes in class_list_txt]

    def __call__(self, ann_file, show=False, out_dir=None, eval=False):
        distributed = False
        # sample
        self.cfg.data.test.ann_file = ann_file
        self.cfg.data.test.img_prefix = os.path.dirname(ann_file)
        self.cfg.data.test.dict_file = dict_path
        # build the dataloader
        dataset = build_dataset(self.cfg.data.test)
        data_loader = build_dataloader(
            dataset,
            samples_per_gpu=1,
            workers_per_gpu=self.cfg.data.workers_per_gpu,
            dist=distributed,
            shuffle=False)

        results = []
        # dataset = data_loader.dataset
        prog_bar = mmcv.ProgressBar(len(dataset))
        y_pred = []
        y_gt = []
        final_list_key = []
        for idx, data in enumerate(data_loader):
            with torch.no_grad():
                result = self.model(return_loss=False, rescale=True, **data)

            batch_size = len(result)
            if show or out_dir:
                img_tensor = data['img'].data[0]
                img_metas = data['img_metas'].data[0]
                imgs = tensor2imgs(img_tensor, **img_metas[0]['img_norm_cfg'])
                assert len(imgs) == len(img_metas)
                gt_bboxes = [data['gt_bboxes'].data[0][0].numpy().tolist()]

                for i, (img, img_meta) in enumerate(zip(imgs, img_metas)):
                    list_key = {}
                    print(img_meta['ori_filename'])
                    h, w, _ = img_meta['img_shape']
                    img_show = img[:h, :w, :]

                    box_ann_infos = dataset.data_infos[idx]['annotations']
                    node_gt = [box_ann_info['label'] for box_ann_info in box_ann_infos]
                    node_text = [box_ann_info['text'] for box_ann_info in box_ann_infos]

                    if out_dir:
                        out_file = osp.join(out_dir, img_meta['ori_filename'])
                    else:
                        out_file = None

                    vis_img, node_pred = self.model.module.show_result(
                        img_show,
                        result[i],
                        gt_bboxes[i],
                        show=show,
                        out_file=out_file)
                    if len(node_pred) != len(node_gt):
                        print('Here')
                    y_pred.extend(node_pred)
                    y_gt.extend(node_gt)
                    total_pred = len(node_pred)-1
                    for idx, pred in enumerate(node_pred):
                        reverse_idx =total_pred-idx
                        if node_pred[reverse_idx] != len(self.class_list) - 1:
                            entity = self.class_list[node_pred[reverse_idx]]
                            print(entity, node_text[reverse_idx])
                            if entity not in list_key.keys():
                                list_key[entity] = [node_text[reverse_idx]]
                            else:
                                list_key[entity].append(node_text[reverse_idx])
                    final_list_key.append(list_key)

            for _ in range(batch_size):
                prog_bar.update()

        if eval:
            labels = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
            print('\nF1 scores of each class.................................................')
            count = 0
            total_F1 = 0
            for label in labels:
                score = eval_marco_F1(y_pred=y_pred,
                                      y_gt=y_gt,
                                      labels=[label],
                                      )
                print(str(label).ljust(20), score)
                if score > -10:
                    count += 1
                    total_F1 += score
            print('average F1 in', count, 'class:', total_F1 / count)

        '''
              Example of final_list_key
              final_list_key= [{'exporter_name': ['XIN MINGHUAPTELT'], 
                   'exporter_add': ['55 Tuas Crescent 107-0', 'Singapore 63874'],
                   'importer_name': ['Kami engineering Joint stock compan'], 
                   'payment_method': [': TTin advanc'],
                   'ben_bank_name': ['United Overseas Bank Limited (Jalan Sultan Branch Singapore'],
                   'ben_name': ['ACCOUNT NAME: Xin Ming Hua Pte Lt']}]
        '''

        return final_list_key


kie_model = KIE_SDMGR()

def extract(ann_file, viz_dir):
    begin = time.time()
    res_json = kie_model(ann_file=ann_file, out_dir=viz_dir)
    end = time.time()
    print('\ninference times', end - begin, 'seconds')
    return res_json

if __name__ == '__main__':
    extract()
