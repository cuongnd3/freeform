import os

CONFIG_ROOT = os.path.dirname(__file__)


def full_path(sub_path, file=True):
    path = os.path.join(CONFIG_ROOT, sub_path)
    if not file and not os.path.exists(path):
        try:
            os.makedirs(path)
        except:
            print('full_path. Error makedirs', path)
    return path


visualize = False
eval = True
config_path = full_path('weights/kie/sale_contracts/sdmgr_unet16_60e_sale_contracts.py')
ckpt_path = full_path('weights/kie/sale_contracts/epoch_35_8516.pth')
class_list_path = full_path('weights/kie/sale_contracts/class_list.txt')
dict_path = full_path('weights/kie/sale_contracts/dict.txt')
test_file = full_path('temp/test.txt')
viz_dir = full_path('viz/sale_contracts', file=False)
