# ==============================================================================
# DoNT dont3293@gmail.com
# ==============================================================================
import time
import cv2
import sys, os
import numpy as np
from main_app.config.general import USING_GPU
YOLO_DEBUG = False


class DetModel:
    def __init__(self, model_config, model_path):
        self.confThreshold = 0.9  # Confidence threshold
        self.nmsThreshold = 0.2  # Non-maximum suppression threshold
        self.inpWidth = 608  # Width of network's input image
        self.inpHeight = 608  # Height of network's input image

        self.classes = ['yes', 'no']
        if not os.path.isfile(model_config) or not os.path.isfile(model_path):
            print("dsggd")
        self.net = cv2.dnn.readNetFromDarknet(model_config, model_path)
        if USING_GPU:
            try:
                self.net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
                self.net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)
            except:
                self.net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
                self.net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)
        else:
            self.net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
            self.net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)

        print(''.format('Done load Checkbox DET model'))

        self.ouputsNames = self.getOutputsNames(self.net)

    def getOutputsNames(self, net):
        layersNames = net.getLayerNames()
        return [layersNames[i[0] - 1] for i in net.getUnconnectedOutLayers()]

    def detectText(self, image):
        blob = cv2.dnn.blobFromImage(image, 1 / 255, (self.inpWidth, self.inpHeight), [0, 0, 0], 1, crop=False)
        self.net.setInput(blob)

        outs = self.net.forward(self.ouputsNames)

        h, w = image.shape[:2]

        classIds = []
        confidences = []
        boxes = []
        for out in outs:
            for detection in out:
                scores = detection[5:]
                classId = np.argmax(scores)
                confidence = scores[classId]
                # if classId in [10, 17, 18, 19]:
                #     print(classId, confidence)
                if confidence > self.confThreshold:
                    center_x = int(detection[0] * w)
                    center_y = int(detection[1] * h)
                    width = min(w, int(detection[2] * w))
                    height = min(h, int(detection[3] * h))
                    left = max(0, int(center_x - width / 2))
                    top = max(0, int(center_y - height / 2))
                    classIds.append(classId)
                    confidences.append(float(confidence))
                    boxes.append([left, top, width, height])

        indices = cv2.dnn.NMSBoxes(boxes, confidences, self.confThreshold, self.nmsThreshold)
        list_boxes = []
        for i in indices:
            i = i[0]
            box = boxes[i]
            if box[2] * box[3]:
                list_boxes.append((box, self.classes[classIds[i]], confidences[i]))

        return list_boxes

    def predict(self, image, need_rotate=True):
        list_boxes = self.detectText(image)
        draw = image.copy()
        list_boxes = [(rect, label, conf)for rect, label, conf in list_boxes if 0.66 <= rect[2]/rect[3] <= 1.5]
        for rect, label, conf in list_boxes:
            if label == self.classes[0]:
                color = (0, 255, 0)
            else:
                color = (255, 0, 0)
            cv2.rectangle(draw, (rect[0], rect[1]), (rect[0] + rect[2], rect[1] + rect[3]), color, 4)
            cv2.putText(draw, label, (rect[0], rect[1]), cv2.FONT_HERSHEY_SIMPLEX, 1, color)
            cv2.putText(draw, "{:.2f}".format(conf), (rect[0] + rect[2], rect[1]), cv2.FONT_HERSHEY_SIMPLEX, 1, color, 3)
        if YOLO_DEBUG:
            cv2.namedWindow('detection', cv2.WINDOW_NORMAL)
            cv2.resizeWindow('detection', 1280, 720)
            cv2.imshow('detection', draw)
            cv2.waitKey(1)
            # cv2.imwrite('result.jpg', draw)
        return draw, list_boxes


root = os.path.dirname(os.path.realpath(__file__))

modelConfiguration = os.path.join(root, "model", "cb_yolov4.cfg")
modelWeights = os.path.join(root, "model", "cb_yolov4_27000.weights")
reg_model = DetModel(modelConfiguration, modelWeights)


def detection_checkbox(image):
    s = time.time()
    fx = 960/image.shape[1]
    image = cv2.resize(image, (0, 0), fx=fx, fy=fx)
    image, list_boxes = reg_model.predict(image)
    list_detected_checkbox = []
    for rect, label, conf in list_boxes:
        x1, y1, w, h = rect
        list_detected_checkbox.append([int(x1/fx), int(y1/fx), int(w/fx), int(h/fx), label])
    e = time.time()
    print("checkbox detection time: ", (e-s)*1000)
    return list_detected_checkbox, image


if __name__ == "__main__":
    import cv2
    while True:
        img = cv2.imread(input('image: '))
        reg_model.predict(img)
        print('detection model')
        cv2.waitKey(0)
