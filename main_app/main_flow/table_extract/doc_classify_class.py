import cv2
from main_app.utils.common import document
from main_app.main_flow.text_det.text_det_class import TextDet


class DocRot():
    def __init__(self, model='mobilenetv3'):
        self.model = model
        if model =='mobilenetv3':
            from main_app.main_flow.doc_rot.mobilenet import run
            self.doc_rotator = run
    def inference(self, doc, vis = False):
        if self.model =='mobilenetv3':
            self.doc_rotator.rotate_document(doc, vis=vis)

if __name__=='__main__':
    file_path = '/home/duycuong/home_data/vvn/OCR_MAFC/BHYT/web/imgs/1734149_1729699_BHYT_0_1.png'
    file_path = '/home/duycuong/home_data/vvn/OCR_MAFC/BHYT/normal/1723321_1718968_BHYT_0.pdf'

    #img = cv2.imread(file_path)
    new_doc = document(file_path, type='BHYT', max_pages=2)

    text_detector = TextDet(model='paddle_v11')
    text_detector.inference(new_doc)

    # new_doc.visualize()

    doc_rotator = DocRot(model='mobilenetv3')
    doc_rotator.inference(new_doc, vis=False)

    new_doc.visualize()