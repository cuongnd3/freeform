import os
import mxnet as mx
import numpy as np
import time
import cv2
import torch
from torch.autograd import Variable
from mxnet.gluon.data.vision import transforms
from craft import craft_utils
from craft.craft_model import CRAFT
from config import config
from collections import OrderedDict
if config.USING_GPU:
    import torch.backends.cudnn as cudnn


def copyStateDict(state_dict):
    if list(state_dict.keys())[0].startswith("module"):
        start_idx = 1
    else:
        start_idx = 0
    new_state_dict = OrderedDict()
    for k, v in state_dict.items():
        name = ".".join(k.split(".")[start_idx:])
        new_state_dict[name] = v
    return new_state_dict


def str2bool(v):
    return v.lower() in ("yes", "y", "true", "t", "1")


DEBUG_CRAFT = 0


def resize_aspect_ratio(img, square_size=1280, interpolation=cv2.INTER_LINEAR, mag_ratio=1.5):
    height, width, channel = img.shape

    # magnify image size
    target_size = mag_ratio * max(height, width)

    # set original image size
    if target_size > square_size:
        target_size = square_size
    
    ratio = float(target_size) / max(height, width)    

    target_h, target_w = int(height * ratio), int(width * ratio)
    proc = cv2.resize(img, (target_w, target_h), interpolation = interpolation)


    # make canvas and paste image
    target_h32, target_w32 = target_h, target_w
    if target_h % 32 != 0:
        target_h32 = target_h + (32 - target_h % 32)
    if target_w % 32 != 0:
        target_w32 = target_w + (32 - target_w % 32)
    resized = np.ones((target_h32, target_w32, channel), dtype=np.float32)
    resized[0:target_h, 0:target_w, :] = proc
    target_h, target_w = target_h32, target_w32

    size_heatmap = (int(target_w/2), int(target_h/2))

    return resized, ratio, size_heatmap


def normalizeMeanVariance(in_img, mean=(0.485, 0.456, 0.406), variance=(0.229, 0.224, 0.225)):
    # should be RGB order
    img = in_img.copy().astype(np.float32)

    img -= np.array([mean[0] * 255.0, mean[1] * 255.0, mean[2] * 255.0], dtype=np.float32)
    img /= np.array([variance[0] * 255.0, variance[1] * 255.0, variance[2] * 255.0], dtype=np.float32)
    return img


def cvt2HeatmapImg(img):
    img = (np.clip(img, 0, 1) * 255).astype(np.uint8)
    img = cv2.applyColorMap(img, cv2.COLORMAP_JET)
    return img


class CRAFTMXNET:
    def __init__(self,prefix,epoch=0,ctx=mx.cpu()):
        sym, arg_params, aux_params = mx.model.load_checkpoint(prefix,epoch)
        self.model = mx.mod.Module(symbol=sym, context=ctx, data_names=['data'], label_names=None)
        self.model.bind(for_training=False, data_shapes=[('data',(1,3,2400,2400))])
        self.model.set_params(arg_params, aux_params, allow_missing=False)
        self.transform_img = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ])

    def predict(self, image, canvas_size=2560, mag_ratio=1, text_threshold=0.4,
                link_threshold=0.5, low_text=0.4, poly=False):
        if DEBUG_CRAFT:
            t = time.time()
        img = cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
        img_resized, target_ratio, size_heatmap = resize_aspect_ratio(img, canvas_size,
                                                                      interpolation=cv2.INTER_LINEAR,
                                                                      mag_ratio=mag_ratio)
        ratio_h = ratio_w = 1 / target_ratio
        x = mx.nd.array(img_resized)
        x = self.transform_img(x)
        y = self.model.predict(x.expand_dims(axis=0))
        score_text = y[0,:,:,0].asnumpy()
        score_link = y[0,:,:,1].asnumpy()

        boxes, polys = craft_utils.getDetBoxes(score_text, score_link, text_threshold=text_threshold,
                                               link_threshold=link_threshold, low_text=low_text, poly=poly)
        boxes = craft_utils.adjustResultCoordinates(boxes, ratio_w, ratio_h)
        polys = craft_utils.adjustResultCoordinates(polys, ratio_w, ratio_h)
        for k in range(len(polys)):
            if polys[k] is None: polys[k] = boxes[k]


        if DEBUG_CRAFT:
            list_boxes = [cv2.boundingRect(box) for box in boxes]
            print("elapsed time craft : {} s".format(time.time() - t))
            render_img = score_text.copy()
            render_img = np.hstack((render_img, score_link))
            ret_score_text = cvt2HeatmapImg(render_img)
            cv2.namedWindow('mask',cv2.WINDOW_NORMAL)
            cv2.imshow('mask',ret_score_text)

            draw = image.copy()
            for (x,y,w,h) in list_boxes:
                cv2.rectangle(draw,(x,y),(x+w,y+h),[255,0,0],2)
            cv2.namedWindow('draw',cv2.WINDOW_NORMAL)
            cv2.imshow('draw',draw)
            cv2.waitKey(1)
            cv2.imwrite('draw.jpg',draw)

            poly_img = image.copy()
            for i, box in enumerate(polys):
                p = np.array(box).astype(np.int32).reshape((-1))
                p = p.reshape(-1, 2)
                cv2.polylines(poly_img, [p.reshape((-1, 1, 2))], True, color=(0, 0, 255), thickness=2)

            cv2.namedWindow('poly_img', cv2.WINDOW_NORMAL)
            cv2.imshow('poly_img', poly_img)
            cv2.waitKey(0)
        return boxes, polys


class CRAFTPYTORCH:
    def __init__(self, trained_model):
        # load net
        self.net = CRAFT()  # initialize

        print('Loading weights from checkpoint (' + trained_model + ')')
        if config.USING_GPU:
            self.net.load_state_dict(copyStateDict(torch.load(trained_model)))
        else:
            self.net.load_state_dict(copyStateDict(torch.load(trained_model, map_location='cpu')))

        if config.USING_GPU:
            self.net = self.net.cuda()
            self.net = torch.nn.DataParallel(self.net)
            cudnn.benchmark = False
        self.net.eval()

    def test_net(self, image, canvas_size, mag_ratio, text_threshold, link_threshold, low_text, cuda, poly):
        t0 = time.time()

        # resize
        img_resized, target_ratio, size_heatmap = resize_aspect_ratio(image, canvas_size,
                                                                              interpolation=cv2.INTER_LINEAR,
                                                                              mag_ratio=mag_ratio)
        ratio_h = ratio_w = 1 / target_ratio

        # preprocessing
        x = normalizeMeanVariance(img_resized)
        x = torch.from_numpy(x).permute(2, 0, 1)  # [h, w, c] to [c, h, w]
        x = Variable(x.unsqueeze(0))  # [c, h, w] to [b, c, h, w]
        if cuda:
            x = x.cuda()

        # forward pass
        with torch.no_grad():
            y, feature = self.net(x)

        # make score and link map
        score_text = y[0, :, :, 0].cpu().data.numpy()
        score_link = y[0, :, :, 1].cpu().data.numpy()

        t0 = time.time() - t0
        t1 = time.time()

        # Post-processing
        boxes, polys = craft_utils.getDetBoxes(score_text, score_link, text_threshold, link_threshold, low_text, poly)

        # coordinate adjustment
        boxes = craft_utils.adjustResultCoordinates(boxes, ratio_w, ratio_h)
        polys = craft_utils.adjustResultCoordinates(polys, ratio_w, ratio_h)
        for k in range(len(polys)):
            if polys[k] is None: polys[k] = boxes[k]

        t1 = time.time() - t1

        # render results (optional)
        render_img = score_text.copy()
        render_img = np.hstack((render_img, score_link))
        ret_score_text = cvt2HeatmapImg(render_img)

        if DEBUG_CRAFT:
            print("\ninfer/postproc time : {:.3f}/{:.3f}".format(t0, t1))
            list_boxes = [cv2.boundingRect(box) for box in boxes]
            render_img = score_text.copy()
            render_img = np.hstack((render_img, score_link))
            ret_score_text = cvt2HeatmapImg(render_img)
            cv2.namedWindow('mask',cv2.WINDOW_NORMAL)
            cv2.imshow('mask',ret_score_text)

            draw = image.copy()
            for (x,y,w,h) in list_boxes:
                cv2.rectangle(draw,(x,y),(x+w,y+h),[255,0,0],2)
            cv2.namedWindow('draw',cv2.WINDOW_NORMAL)
            cv2.imshow('draw',draw)
            cv2.waitKey(1)
            cv2.imwrite('draw.jpg',draw)

            poly_img = image.copy()
            for i, box in enumerate(polys):
                p = np.array(box).astype(np.int32).reshape((-1))
                p = p.reshape(-1, 2)
                cv2.polylines(poly_img, [p.reshape((-1, 1, 2))], True, color=(0, 0, 255), thickness=2)

            cv2.namedWindow('poly_img', cv2.WINDOW_NORMAL)
            cv2.imshow('poly_img', poly_img)
            cv2.waitKey(0)

        return boxes, polys, ret_score_text

    def predict(self, image, canvas_size=2560, mag_ratio=1, text_threshold=0.4,
                link_threshold=0.5, low_text=0.4, poly=False):
        bboxes, polys, scores = self.test_net(image, canvas_size, mag_ratio, text_threshold, link_threshold, low_text,
                                 config.USING_GPU, poly)

        return bboxes, polys


root = os.path.dirname(os.path.realpath(__file__))
prefix = os.path.join(root,"model/craft_v1")
craft_mxnet = CRAFTMXNET(prefix=prefix,epoch=1)

trained_model_path = os.path.join(root,"model/craft_mlt_25k.pth")
craft_torch = CRAFTPYTORCH(trained_model_path)


def detect_text(image, canvas_size=2560, mag_ratio=1, text_threshold=0.4,
                link_threshold=0.5, low_text=0.4, poly=False, craft_model='mxnet'):
    result = []
    if craft_model == 'mxnet':
        bboxes, polys = craft_mxnet.predict(image, canvas_size=canvas_size, mag_ratio=mag_ratio,
                                            text_threshold=text_threshold,
                                            link_threshold=link_threshold, low_text=low_text, poly=poly)
    else:
        bboxes, polys = craft_torch.predict(image, canvas_size=canvas_size, mag_ratio=mag_ratio,
                                            text_threshold=text_threshold,
                                            link_threshold=link_threshold, low_text=low_text, poly=poly)

    for i, box in enumerate(polys):
        poly = np.array(box).astype(np.int32).reshape((-1))
        result.append(poly)
    return result


def group_text_box(img, polys, slope_ths = 0.1, ycenter_ths = 0.7, height_ths = 0.7, width_ths = 1.0, add_margin = 0.05):
    """
    :param polys:
    :param slope_ths:
    :param ycenter_ths: ycenter nho -> khi merge hon
    :param height_ths: height_ths nho --> kho merge hon - so sanh chieu cao cua box
    chech lech giua 2 box < height_ths * height(box1) --> merge
    :param width_ths: distance of 2 box (in X axis) < width_ths * chracter with --> merge
    :param add_margin:
    :return:
    """
    # poly top-left, top-right, low-right, low-left
    horizontal_list, free_list,combined_list, merged_list = [],[],[],[]

    for poly in polys:
        slope_up = (poly[3]-poly[1])/np.maximum(10, (poly[2]-poly[0]))
        slope_down = (poly[5]-poly[7])/np.maximum(10, (poly[4]-poly[6]))
        if max(abs(slope_up), abs(slope_down)) < slope_ths:
            x_max = max([poly[0],poly[2],poly[4],poly[6]])
            x_min = min([poly[0],poly[2],poly[4],poly[6]])
            y_max = max([poly[1],poly[3],poly[5],poly[7]])
            y_min = min([poly[1],poly[3],poly[5],poly[7]])
            horizontal_list.append([x_min, x_max, y_min, y_max, 0.5*(y_min+y_max), y_max-y_min])
        else:
            height = np.linalg.norm( [poly[6]-poly[0],poly[7]-poly[1]])
            margin = int(1.44*add_margin*height)

            theta13 = abs(np.arctan( (poly[1]-poly[5])/np.maximum(10, (poly[0]-poly[4]))))
            theta24 = abs(np.arctan( (poly[3]-poly[7])/np.maximum(10, (poly[2]-poly[6]))))
            # do I need to clip minimum, maximum value here?
            x1 = poly[0] - np.cos(theta13)*margin
            y1 = poly[1] - np.sin(theta13)*margin
            x2 = poly[2] + np.cos(theta24)*margin
            y2 = poly[3] - np.sin(theta24)*margin
            x3 = poly[4] + np.cos(theta13)*margin
            y3 = poly[5] + np.sin(theta13)*margin
            x4 = poly[6] - np.cos(theta24)*margin
            y4 = poly[7] + np.sin(theta24)*margin

            free_list.append([[x1,y1],[x2,y2],[x3,y3],[x4,y4]])
    horizontal_list = sorted(horizontal_list, key=lambda item: item[4])
    if len(horizontal_list) > 0:
        character_width = min([poly[1]-poly[0] for poly in horizontal_list if poly[1]-poly[0] != 0])
    else:
        character_width = 0
    # combine box by "x" location and height

    # combine box in same line
    new_box = []
    for poly in horizontal_list:
        if len(new_box) == 0:
            cbimg = img.copy()
            b_height = [poly[5]]
            b_ycenter = [poly[4]]
            new_box.append(poly)
        else:
            # comparable height and comparable y_center level up to ths*height
            if (abs(np.mean(b_height) - poly[5]) < height_ths*np.mean(b_height)) and (abs(np.mean(b_ycenter) - poly[4]) < ycenter_ths*np.mean(b_height)):
                b_height.append(poly[5])
                b_ycenter.append(poly[4])
                new_box.append(poly)
                # cv2.rectangle(cbimg, (poly[0], poly[2]), (poly[1], poly[3]), (0, 255, 0), 3)
                # cv2.namedWindow('cbimg', cv2.WINDOW_NORMAL)
                # cv2.resizeWindow('cbimg', 1280, 720)
                # cv2.imshow('cbimg', cbimg)
                # cv2.waitKey(1)
            else:
                b_height = [poly[5]]
                b_ycenter = [poly[4]]
                combined_list.append(new_box)
                new_box = [poly]
                cbimg = img.copy()
    combined_list.append(new_box)

    # merge list use sort again
    for boxes in combined_list:
        if len(boxes) == 1: # one box per line
            box = boxes[0]
            margin = int(add_margin*box[5])
            merged_list.append([box[0]-margin,box[1]+margin,box[2]-margin,box[3]+margin])
        else: # multiple boxes per line
            boxes = sorted(boxes, key=lambda item: item[0])

            merged_box, new_box = [],[]
            for box in boxes:
                if len(new_box) == 0:
                    x_max = box[1]
                    new_box.append(box)
                else:
                    if abs(box[0]-x_max) < width_ths * character_width: # merge boxes
                        x_max = box[1]
                        new_box.append(box)
                    else:
                        x_max = box[1]
                        merged_box.append(new_box)
                        new_box = [box]
            if len(new_box) >0: merged_box.append(new_box)

            for mbox in merged_box:
                if len(mbox) != 1: # adjacent box in same line
                    # do I need to add margin here?
                    x_min = min(mbox, key=lambda x: x[0])[0]
                    x_max = max(mbox, key=lambda x: x[1])[1]
                    y_min = min(mbox, key=lambda x: x[2])[2]
                    y_max = max(mbox, key=lambda x: x[3])[3]

                    margin = int(add_margin*(y_max - y_min))

                    merged_list.append([x_min-margin, x_max+margin, y_min-margin, y_max+margin])
                else: # non adjacent box in same line
                    box = mbox[0]

                    margin = int(add_margin*(box[3] - box[2]))
                    merged_list.append([box[0]-margin,box[1]+margin,box[2]-margin,box[3]+margin])
    # may need to check if box is really in image
    return merged_list, free_list


if __name__ == "__main__":
    image = cv2.imread('/data_backup/tiep/MVN_Desktop/Proposal/VVN/OCR/BaoCaoTaiChinh/Data/BCTC_BIDV_2020/p6-44.png')
    import time
    for i in range(100):
        s = time.time()
        craft_mxnet.predict(image)
        e = time.time()
        print(e-s)
