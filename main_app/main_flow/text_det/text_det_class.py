import cv2
from main_app.utils.document import document


class TextDet():
    def __init__(self, model='paddle_v11'):
        self.model = model
        if model =='paddle_v11':
            from main_app.main_flow.text_det.paddle_v11 import text_detection_paddle
            self.text_detector = text_detection_paddle
    def inference(self, doc, vis = False):
        if doc is None:
            print('TextDet. No input document!')
            return
        if self.model =='paddle_v11':
            self.text_detector.detect_document(doc, vis=vis)

if __name__=='__main__':
    file_path = '/home/duycuong/home_data/vvn/OCR_MAFC/SMS_1414/imgs/clean/2010085_NGUYỄN ĐĂNG KHOA_0.png'
    img = cv2.imread(file_path)
    new_doc = document([img], type='TTTB')

    text_detector = TextDet()
    text_detector.inference(new_doc)
