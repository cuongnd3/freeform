import cv2
import os, sys
import numpy as np

TF_DEBUG = 0
confThreshold = 0.9  # Confidence threshold
maskThreshold = 0.2  # Mask threshold

root = os.path.dirname(os.path.realpath(__file__))
net = cv2.dnn.readNetFromTensorflow(os.path.join(root, 'model', 'mask_opt_graph.pb'),
                                    os.path.join(root, 'model', 'graph.pbtxt'))
net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)
print('{:=^80}'.format('Done load maskrcnn model'))


def getMask(image):
    blob = cv2.dnn.blobFromImage(image, size=(256, 256), swapRB=True, crop=False)
    net.setInput(blob)

    # Run the forward pass to get output from the output layers
    boxes, masks = net.forward(['detection_out_final', 'detection_masks'])

    numClasses = masks.shape[1]
    numDetections = boxes.shape[2]

    h, w = image.shape[:2]
    list_mask = []

    if TF_DEBUG:
        draw = image.copy()

    for i in range(numDetections):
        box = boxes[0, 0, i]
        mask = masks[i]
        score = box[2]
        if score > confThreshold:
            classId = int(box[1])

            # Extract the bounding box
            left = int(w * box[3])
            top = int(h * box[4])
            right = int(w * box[5])
            bottom = int(h * box[6])

            left = max(0, min(left, w - 1))
            top = max(0, min(top, h - 1))
            right = max(0, min(right, w - 1))
            bottom = max(0, min(bottom, h - 1))

            # Extract the mask for the object
            classMask = mask[classId]

            # Resize the mask, threshold, color and apply it on the image
            classMask = cv2.resize(classMask, (right - left + 1, bottom - top + 1))
            mask = (classMask > maskThreshold)

            if TF_DEBUG:
                roi = draw[top:bottom + 1, left:right + 1][mask]
                color = [255, 0, 0]
                draw[top:bottom + 1, left:right + 1][mask] = (
                        [0.3 * color[0], 0.3 * color[1], 0.3 * color[2]] + 0.7 * roi).astype(np.uint8)

            mask = mask.astype(np.uint8)
            if cv2.__version__.split('.')[0] == '4':
                contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            else:
                _, contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            list_mask.append((contours[0], image[top:bottom + 1, left:right + 1]))
    if TF_DEBUG:
        cv2.namedWindow('mask', cv2.WINDOW_NORMAL)
        cv2.imshow('mask', draw)
    if len(list_mask):
        list_mask = sorted(list_mask, key=lambda mask: mask[1].shape[0] * mask[1].shape[1], reverse=True)
        return list_mask[0]
    return (None, None)


from main_app.maskrcnn.preprocessing import *

def detect_max_mask(image, debug=False, full_step=False):
    if image is None:
        return None
    if debug:
        cv2.namedWindow('imageraw', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('imageraw', 960, 540)
        cv2.imshow('imageraw', image)
    contour, image = getMask(image)

    if contour is None or image.shape[0] * image.shape[1] == 0:
        return None

    hull = cv2.convexHull(contour, True)

    rect = cv2.minAreaRect(hull)
    rect = (rect[0], (rect[1][0] * 1.1, rect[1][1] * 1.1), rect[2])
    box = cv2.boxPoints(rect)
    box = np.int0(box)
    roi = four_point_transform(image, box, fix_size=False)
    if debug:
        cv2.namedWindow('roibox', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('roibox', 960, 540)
        cv2.imshow('roibox', roi)
        # cv2.waitKey(0)
    """
    tiepnh: tại sao phải có các bước tiếp theo ??
    Nếu dừng tại đây: input --> get countour --> tìm rotated boundingbox -> extract vùng ảnh đó ra          
    """
    if not full_step:
        return roi

    h, w, _ = image.shape

    mask = np.zeros((h, w), dtype=np.uint8)

    cv2.drawContours(mask, [hull], -1, (255), 2)
    if debug:
        cv2.drawContours(image, [hull], -1, (255), 2)
        cv2.namedWindow('image', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('image', 960, 540)
        cv2.imshow('image', image)
        cv2.namedWindow('maskX', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('maskX', 960, 540)
        cv2.imshow('maskX', mask)
    roi_mask = four_point_transform(mask, box, fix_size=False)
    if debug:
        cv2.namedWindow('roi_mask', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('roi_mask', 960, 540)
        cv2.imshow('roi_mask', roi_mask)
        cv2.waitKey()
    line1, line2 = findHorizontalLine(roi_mask)
    line3, line4 = findVerticalLine(roi_mask)

    pts = np.zeros((4, 2), dtype="float32")
    pts[0] = findIntersec2Line(line1, line3)
    pts[1] = findIntersec2Line(line1, line4)
    pts[2] = findIntersec2Line(line2, line4)
    pts[3] = findIntersec2Line(line2, line3)

    if debug:
        cv2.line(roi, (int(pts[0][0]), int(pts[0][1])), (int(pts[1][0]), int(pts[1][1])), (0, 255, 0))
        cv2.line(roi, (int(pts[1][0]), int(pts[1][1])), (int(pts[2][0]), int(pts[2][1])), (0, 255, 0))
        cv2.line(roi, (int(pts[2][0]), int(pts[2][1])), (int(pts[3][0]), int(pts[3][1])), (0, 255, 0))
        cv2.line(roi, (int(pts[3][0]), int(pts[3][1])), (int(pts[0][0]), int(pts[0][1])), (0, 255, 0))
        cv2.namedWindow('line', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('line', 960, 540)
        cv2.imshow('line', roi)
        cv2.waitKey()

    roi = four_point_transform(roi, pts, fix_size=False)
    if debug:
        cv2.namedWindow('roi', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('roi', 960, 540)
        cv2.imshow('roi', roi)
        cv2.waitKey(0)
    if TF_DEBUG:
        cv2.namedWindow('roi', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('roi', 960, 540)
        cv2.namedWindow('roi', cv2.WINDOW_NORMAL)
        cv2.imshow('roi', roi)
    if debug:
        cv2.waitKey(0)

    return roi


from main_app.utils.common import get_list_file_in_folder


def rotate_dir_by_mask(img_dir, output_dir):
    list_files = get_list_file_in_folder(img_dir)
    list_files = sorted(list_files)
    for idx, file in enumerate(list_files):
        if idx <=100:
            continue
        print(idx, file)
        img_path = os.path.join(img_dir, file)
        image = cv2.imread(img_path)
        roi = detect_max_mask(image, debug=False)
        cv2.imwrite(os.path.join(output_dir, file), roi)
        kk=1


if __name__ == "__main__":
    # image = cv2.imread(sys.argv[1])

    # image = cv2.imread("/data_backup/cuongnd/Viettel_freeform/MAFC/BHYT/imgs/raw/1687843_1683494_BHYT_0_0.png")
    # detect_max_mask(image, debug=True)

    img_dir='/data_backup/cuongnd/Viettel_freeform/MAFC/GPLX/imgs/raw'
    output_dir='/data_backup/cuongnd/Viettel_freeform/MAFC/GPLX/imgs/rotate'
    rotate_dir_by_mask(img_dir, output_dir)