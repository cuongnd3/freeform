import cv2, math, os
import numpy as np
import torchvision.transforms as transforms
from PIL import Image
import torch, time
from scipy.cluster.vq import kmeans, vq
from main_app.utils.document import text_box
from main_app.config.general import box_rectify_ckpt_path
from main_app.main_flow.doc_rot.mobilenet.mobilenetv3 import mobilenetv3
from main_app.main_flow.doc_rot.mobilenet.model import BoxRectify
from main_app.utils.common import euclidean_distance
from main_app.utils.image_processing import transform_point, rotate_and_crop, order_points
from main_app.main_flow.doc_rot.mobilenet.utils.loader import get_img_paths, NewPad


def init_box_rectify_model(weight_path):
    classList = ['0', '180']
    device = torch.device('cpu')
    model = mobilenetv3(n_class=2, dropout=.2, input_size=64)
    normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
    transform_test = transforms.Compose([
        NewPad(t_size=(64, 192), fill=(255, 255, 255)),
        transforms.Resize((64, 192), interpolation=Image.NEAREST),
        # transforms.CenterCrop((64, 192)),
        # transforms.RandomCrop((arg.input_size[0], arg.input_size[1])),
        transforms.ToTensor(),  # 3*H*W, [0, 1]
        normalize])
    cq = BoxRectify(model=model, weightPath=weight_path, classList=classList, transforms=transform_test, imW=192,
                    imH=64, device=device)
    return cq


box_rectify_model = init_box_rectify_model(weight_path=box_rectify_ckpt_path)


def calculate_page_orient(page, list_idx, debug=False):
    orient_state = {'0': 0, '180': 0}
    box_data = []
    begin = time.time()
    for idx in list_idx:
        roi_img = rotate_and_crop(page.img,
                              page.list_text_boxes[idx].list_pts,
                              debug=False,
                              extend=True,
                              extend_x_ratio=0.01,
                              extend_y_ratio=0.01,
                              min_extend_x=1,
                              min_extend_y=2)

        _, angle = box_rectify_model.inference(roi_img, debug=False)
        # orient_state[angle[0]] += 1
        # orient_state[angle[0]] += angle[1]
        if angle[0]=='0':
            orient_state['0'] += angle[1]
            orient_state['180'] += (1-angle[1])
        else:
            orient_state['180'] += angle[1]
            orient_state['0'] += (1-angle[1])

        if debug:
            print('orient:', angle)
            # cv2.imshow('roi img after', roi_img)
            # cv2.waitKey(0)
    if debug:
        print(orient_state)
    if orient_state['0'] >= orient_state['180']:
        page_orient = 0
    else:
        page_orient = 180
    return page_orient


def rotate_page_angle(page, angle, debug=False):
    ori_h = page.img.shape[0]
    ori_w = page.img.shape[1]
    Mat_rotation = cv2.getRotationMatrix2D((ori_w / 2, ori_h / 2), 360 - angle, 1)

    ori_pts = [[0, 0], [ori_w - 1, 0], [ori_w - 1, ori_h - 1], [0, ori_h - 1]]
    trans_pts = transform_point(ori_pts, Mat_rotation)

    x_min = min([pts[0] for pts in trans_pts])
    x_max = max([pts[0] for pts in trans_pts])
    y_min = min([pts[1] for pts in trans_pts])
    y_max = max([pts[1] for pts in trans_pts])

    Mat_translation = np.asarray([[1, 0, -x_min], [0, 1, -y_min]])

    final_trans_pts = transform_point(trans_pts, Mat_translation)

    src_pts = np.asarray(ori_pts[0:3], dtype=np.float32)
    dst_pts = np.asarray(final_trans_pts[0:3], dtype=np.float32)

    final_Mat = cv2.getAffineTransform(src_pts, dst_pts)

    new_w = x_max - x_min
    new_h = y_max - y_min

    page.img = cv2.warpAffine(page.img, final_Mat, (new_w, new_h), borderValue=(255, 255, 255))

    if debug:
        ratio = 0.2
        res_img = cv2.resize(res, (int(ratio * page.img.shape[1]), int(ratio * page.img.shape[0])))
        cv2.imshow('rotate result', res_img)
        cv2.waitKey(0)

    for tb in page.list_text_boxes:
        trans_pt = transform_point(tb.list_pts, final_Mat)
        tb.list_pts = order_points(np.asarray(trans_pt)).tolist()
        tb.calculate_width_height_center()


def filter_outliers_angle(list_angle, thresh=45):
    all_box_angle = np.array(list_angle)
    all_box_angle = np.absolute(all_box_angle)
    filter_angle_idx = []
    if all_box_angle.max() - all_box_angle.min() > thresh:
        codebook, _ = kmeans(all_box_angle, 2)  # three clusters
        cluster_indices, _ = vq(all_box_angle, codebook)
        if cluster_indices.mean() < 0.5:
            major_idx = 0
        else:
            major_idx = 1
        for idx, indice in enumerate(cluster_indices):
            if indice == major_idx:
                filter_angle_idx.append(idx)
    return filter_angle_idx


def get_mean_horizontal_angle(page, list_idx, debug=False):
    all_box_angle = []
    for idx in list_idx:
        angle_with_horizontal_line = page.list_text_boxes[idx].get_horizontal_angle()
        if angle_with_horizontal_line >= 0:
            angle_with_horizontal_line = 180 - angle_with_horizontal_line + 90
        else:
            angle_with_horizontal_line = math.fabs(angle_with_horizontal_line) - 90
        all_box_angle.append(angle_with_horizontal_line)
        if debug:
            print(angle_with_horizontal_line)

    filter_angle_idx = filter_outliers_angle(all_box_angle)
    final_box_angle = []
    filter_list_idx = []
    if len(filter_angle_idx) > 0:
        for idx in filter_angle_idx:
            final_box_angle.append(all_box_angle[idx])
            filter_list_idx.append(list_idx[idx])
    else:
        final_box_angle = all_box_angle
        filter_list_idx = list_idx

    # all_box_angle
    mean_angle = np.array(final_box_angle).mean()
    mean_angle = mean_angle - 90

    return mean_angle, filter_list_idx


from main_app.utils.visualize import viz_page
from main_app.config.general import font_path


def rotate_page(page, vis=False):
    begin = time.time()
    # filter box that has max_wh_ratio < 2 in order to calculate angle of document
    filter_idx = []
    for idx, tb in enumerate(page.list_text_boxes):
        max_ratio, long_edge, short_edge = tb.check_max_wh_ratio()
        if max_ratio > 2:
            filter_idx.append(idx)
    if len(filter_idx)==0:
        print('DocRot. rotate_page. No box to rotate!')
        return

    end_filter1 = time.time()
    # print('end_filter1', end_filter1 - begin)
    # calculate angle with horizontal
    page_angle, filter_idx = get_mean_horizontal_angle(page, list_idx=filter_idx)

    end_angle = time.time()
    # print('end_angle', end_angle - end_filter1)
    # rotate ALL img and text box in page --> rotate to horizontal
    rotate_page_angle(page, page_angle)

    end_rotate1 = time.time()
    # print('end_rotate1', end_rotate1 - end_angle)
    # distuiguish between 0 and 180 degree --> rotate page
    page_orient = calculate_page_orient(page, filter_idx, debug=False)

    end_orient = time.time()
    # print('end_orient', end_orient - end_rotate1)

    if page_orient != 0:
        rotate_page_angle(page, 180)

    end_rotate2 = time.time()
    # print('end_rotate2', end_rotate2 - end_orient)

    # filter again to ignore vertical box
    final_list_text_box = []
    for tb in page.list_text_boxes:
        if tb.is_horizontal_box():
            final_list_text_box.append(tb)
    end_filter2 = time.time()
    # print('end_filter2', end_filter2 - end_rotate2)
    page.list_text_boxes = final_list_text_box


def rotate_document(doc, drop_thres=[.5, 2], vis=False):
    for idx, page in enumerate(doc.list_pages):
        begin = time.time()
        rotate_page(page, vis)
        end = time.time()
        print('DocRot. time for page', idx, int(1000 * (end - begin)), 'ms')


if __name__ == '__main__':
    # pts = np.array([[1,5],[5,5],[5,9],[1,9]])
    pts = np.array([[5, 5], [5, 9], [1, 9], [1, 5]])
    res_pts = order_points(pts)

    from main_app.utils.document import document
    from main_app.main_flow.text_det.text_det_class import TextDet
    from main_app.main_flow.doc_rot.doc_rot_class import DocRot
    from main_app.config.general import metadata_dir, font_path, upload_dir

    file_path = '/home/duycuong/home_data/vvn/OCR_MAFC/BHYT/normal/pdf/1710088_1705698_BHYT_0.pdf'
    file_name = os.path.basename(file_path).split('.')[0]
    new_doc = document(file_path, type='BHYT', max_pages=2)

    text_detector = TextDet()
    text_detector.inference(new_doc)
    doc_rotator = DocRot()
    doc_rotator.inference(new_doc)

    new_doc.visualize(vis_dir=upload_dir,
                      vis_name=file_name,
                      text_box=True,
                      key_value=True,
                      font_path=font_path,
                      drop_score=-2,
                      max_len=4000)

    #
    # doc_rot = Doc
