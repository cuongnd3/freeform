import torch, cv2
import numpy as np
from PIL import Image
from torch.autograd import Variable
from main_app.main_flow.doc_rot.mobilenet.utils.core import ClsPostProcess
from main_app.main_flow.doc_rot.mobilenet.utils.utils import rotate_image_angle
import torch.nn as nn

class BoxRectify:
    def __init__(self, model=None, transforms=None, weightPath=None, classList=None, backBoneWPath=None, imW=400,
                 imH=252, device=None):
        self.classList = classList
        ## build model
        if device == None:
            self.device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
        else:
            self.device = device
        self.imW = imW
        self.imH = imH
        self.net_ = model.to(self.device)
        self.net_.load_state_dict(torch.load(weightPath,
                                             map_location=lambda storage, loc: storage))
        self.net_.eval()
        self.transform = transforms
        # net_ = net_.cuda()

    def inference(self, image, debug=False):
        if isinstance(image, str):
            image = Image.open(image)
        if isinstance(image, np.ndarray):
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            if debug:
                cv2.imshow('box_rectify. Before', image)
                print('Before', image.shape)
                # cv2.waitKey(0)
            image = Image.fromarray(image)

        x = self.transform(image)
        x = x.view(1, 3, self.imH, self.imW)
        x.unsqueeze(0)
        xx = Variable(x).to(self.device)

        out = self.net_(xx)
        preds = nn.Softmax(1)(out)

        post_pr = ClsPostProcess(self.classList)
        post_result = post_pr(preds.clone().detach().numpy())[0]
        image = rotate_image_angle(image, int(post_result[0]))
        # image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
        if debug:
            cv2.imshow('box_rectify. After', image)
            print('After', image.shape, post_result)
            cv2.waitKey(0)
        return image, post_result
