import cv2, importlib
from main_app.utils.document import document
from main_app.main_flow.text_det.text_det_class import TextDet
from main_app.config.general import config_by_type


class DocRot():
    def __init__(self, model='mobilenetv3'):
        self.model = model
        if model =='mobilenetv3':
            # from main_app.main_flow.doc_rot.mobilenet import run2 as run
            from main_app.main_flow.doc_rot.mobilenet import run_doc_rot
            self.doc_rotator = run_doc_rot
    def inference(self, doc, fix_text_box_angle =False, vis = False):
        if doc is None:
            print('DocRot. No input document!')
            return
        if self.model =='mobilenetv3':
            self.doc_rotator.rotate_document(doc, vis=vis)

        # overide config by config of each type
        if doc.type in config_by_type:
            config = importlib.import_module('main_app.config.form.' + doc.type)
            fix_text_box_angle = config.fix_text_box_angle

        if fix_text_box_angle:
            doc.fix_text_box_angle_in_page()

if __name__=='__main__':
    file_path = '/home/duycuong/home_data/vvn/OCR_MAFC/BHYT/web/imgs/1734149_1729699_BHYT_0_1.png'
    file_path = '/home/duycuong/home_data/vvn/OCR_MAFC/BHYT/normal/1710088_1705698_BHYT_0.pdf'

    #img = cv2.imread(file_path)
    new_doc = document(file_path, type='BHYT', max_pages=2)

    text_detector = TextDet(model='paddle_v11')
    text_detector.inference(new_doc)

    # new_doc.visualize()

    doc_rotator = DocRot(model='mobilenetv3')
    doc_rotator.inference(new_doc, vis=False)

    new_doc.visualize()