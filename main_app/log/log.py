import logging
from logging.handlers import RotatingFileHandler
import os

root = os.path.dirname(os.path.realpath(__file__))

log_formatter = logging.Formatter('%(asctime)s %(levelname)s %(funcName)s(%(lineno)d) %(message)s')

logFile = os.path.join(root,'app.log')

my_handler = RotatingFileHandler(logFile, mode='a', maxBytes=50*1024*1024, 
                                 backupCount=2, encoding=None, delay=0)
my_handler.setFormatter(log_formatter)
my_handler.setLevel(logging.INFO)
app_log = logging.getLogger('app')
app_log.setLevel(logging.INFO)
app_log.addHandler(my_handler)

debugFile = os.path.join(root,'debug.log')
debug_handler = RotatingFileHandler(debugFile, mode='a', maxBytes=50*1024*1024, 
                                 backupCount=2, encoding=None, delay=0)
debug_handler.setFormatter(log_formatter)
debug_handler.setLevel(logging.INFO)
debug_log = logging.getLogger('debug')
debug_log.setLevel(logging.INFO)
debug_log.addHandler(debug_handler)