import os, csv, glob, cv2, time, traceback
from PIL import Image
from main_app.config.general import metadata_dir, font_path, upload_dir, merge_text_box, fix_text_box_angle
from main_app.utils.string_rule_based import string_contains_word
from main_app.utils.common import get_list_file_in_folder, write_icdar_result, write_sdmgr_result
from main_app.utils.document import document
from main_app.main_flow.kie.form.GPLX import extract_GPLX
import shutil


def load_metadata(metadata_dir):
    metadata = {}
    if metadata_dir[-1] != '/':
        metadata_dir += '/'
    for csv_path in glob.glob(metadata_dir + '*.csv'):
        csv_name = os.path.basename(csv_path).replace('.csv', '')
        metadata[csv_name] = {}
        with open(csv_path, 'r', encoding='utf-8') as fi:
            reader = csv.reader(fi, delimiter='\t')
            # header = next(reader)
            for line in reader:
                keywords = line[2].split(',')
                list_data = line[3].split(',')
                if len(list_data) == 1 and list_data[0] == '':
                    list_data = []
                metadata[csv_name][line[1]] = {'keywords': keywords,
                                               'list_data': list_data}
    return metadata


def document_classification(combined_list):
    """
    Find the document type by number break keyword in document
    :param combined_list: document
    :return: document type
    """
    texts = []
    for line in combined_list:
        line = sorted(line, key=lambda x: x[0])
        for bb in line:
            texts.append(bb[6])
            # print(bb[6])

    best_match_score = 0
    best_match_doc_type = ''
    for doc_type in metadata:
        break_keywords = metadata[doc_type]['break_keywords']['keywords']
        score = sum([1 for text in texts if string_contains_word(text, break_keywords, is_vp_bank=False) >= 0])
        if best_match_score < score:
            best_match_score = score
            best_match_doc_type = doc_type
    return best_match_doc_type


def return_data(info, doc_type=''):
    # return data for table display
    table_data = []
    list_key = [(k, k) for k in sorted(list(info.keys()))]
    if doc_type == 'business_registration':
        list_key = return_list_key()
    for k, k_d in list_key:
        if not k in info:
            continue
        v = info[k]
        print(k, v)
        table_data.append([k_d, v])
    return table_data


from main_app.config.general import supported_doc_type, api_doc_type, \
    text_detection, doc_rotation, text_recognition, kie_model

# init model based on config --> memory saving
if text_detection is not None:
    print('main. Init text_detector')
    from main_app.main_flow.text_det.text_det_class import TextDet

    text_detector = TextDet(model=text_detection)

if doc_rotation is not None:
    print('main. Init doc_rotator')
    from main_app.main_flow.doc_rot.doc_rot_class import DocRot

    doc_rotator = DocRot(model=doc_rotation)

if text_recognition is not None:
    print('main. Init text_recognizer')
    from main_app.main_flow.text_rec.text_rec_class import TextRec

    text_recognizer = TextRec(model=text_recognition)

if kie_model is not None:
    print('main. Init kie')
    from main_app.main_flow.kie.kie_class import KIE

    kie = KIE(model=kie_model)

metadata = load_metadata(metadata_dir)


def kie_from_file(file_path, doc_type='', debug=False, save_dir='', max_pages=999, vis=False):
    """
    1. extract img from file (tiff/pdf) with maximum $max_pages pages for each file
    2. extract information from list img
    :param file_path: input document file(image/pdf or tiff)
    :param doc_type: type of input document (if empty --> find the type of document by rule)
    :param debug: True - enable debug mode
    :param max_pages: maximum pages extract from document file
    :return: status and dictionary contain extracted information
    """
    begin = time.time()
    list_img = []
    list_img_path = []
    error_type = None
    if doc_type in supported_doc_type:
        print('kie_from_file. document type', doc_type)
    else:
        print('kie_from_file.', doc_type, 'not support')

    new_doc = document(file_path, type=doc_type, max_pages=max_pages)
    if new_doc is None or new_doc.list_pages == []:
        new_doc = None
        return 'input_error', {}

    file_name = os.path.basename(file_path).split('.')[0]
    save_img_dir = os.path.join(upload_dir, file_name)

    try:
        # call by api, not by normal flow
        if new_doc.type in api_doc_type:
            list_img_path = new_doc.save_page_images(save_img_dir, max_len=1200)
            new_doc.key_value, new_doc.table_data = extract_GPLX(list_img_path)

        # normal flow
        else:
            # list_img_path= new_doc.save_page_images(save_img_dir, max_len=1200)
            if text_detection is not None:
                text_detector.inference(new_doc)
            if doc_rotation is not None:
                doc_rotator.inference(new_doc, fix_text_box_angle=fix_text_box_angle)
            if text_recognition is not None:
                text_recognizer.inference(new_doc, merge_text_box=merge_text_box)
            if kie_model is not None:
                kie.inference(new_doc)
        if vis:
            vis_dir = save_dir if save_dir!='' else upload_dir
            new_doc.visualize(vis_dir=vis_dir,
                              vis_name=file_name,
                              text_box=True,
                              key_value=True,
                              font_path=font_path)

        # print(res)
        end = time.time()
        print('kie_from_file. Time:', round(1000 * (end - begin)), 'ms')
        info = {'raw_data': new_doc.key_value, 'table_data': new_doc.table_data}
    except:
        msg_detail = traceback.format_exc()
        print(msg_detail)
        return 'engine_error', {}

    return None, info


def save_results(result_path, info):
    final_txt = ''
    for idx, key_value in enumerate(info['table_data']):
        line = key_value[0] + ': ' + key_value[1]
        final_txt += line + '\n'
    with open(result_path, 'w', encoding='utf-8') as f:
        f.write(final_txt)


if __name__ == '__main__':

    # print(metadata)
    print('main')
    type = 'travel_reserve' #GPLX TTTB
    save_dir =''
    if type =='BHYT':
        file_dir = '/home/duycuong/home_data/vvn/OCR_MAFC/MAFC_poc/uploads_63_1526_03062021/BHYT'
        #file_dir ='/home/duycuong/home_data/vvn/OCR_MAFC/BHYT/normal/pdf'
        file_dir = '/home/duycuong/home_data/vvn/OCR_MAFC/MAFC_poc/uploads_63_1526_03062021/BHYT'
        file_path = '/home/duycuong/home_data/vvn/OCR_MAFC/MAFC_poc/uploads_63_1526_03062021/BHYT/20210602-154145_vvn-test_1687843_1683494_BHYT_0.pdf'
        save_dir ='/home/duycuong/home_data/vvn/OCR_MAFC/MAFC_poc/uploads_63_1526_03062021/res_v0.3/BHYT'
    elif type =='travel_reserve':
        file_dir = '/home/duycuong/home_data/vvn/PTI/vemaybay/travel_reservation'
        file_dir = '/home/duycuong/home_data/vvn/PTI'
        file_path = '/home/duycuong/home_data/vvn/PTI/vemaybay/travel_reservation/Travel Reservation December 26 for MR VAN HUYNH NGUYEN.pdf'
    elif type =='GPLX':
        file_dir = '/home/duycuong/home_data/vvn/PTI/vemaybay/travel_reservation'
        file_path = '/home/duycuong/home_data/vvn/OCR_MAFC/GPLX/pdf/1935996_1931122_GPLX_0.pdf'
    elif type =='TTTB':
        file_dir = '/home/duycuong/home_data/vvn/OCR_MAFC/MAFC_poc/uploads_63_1526_03062021/TTTB'
        file_path = '/home/duycuong/home_data/vvn/OCR_MAFC/MAFC_poc/uploads_63_1526_03062021/TTTB/20210602-043233_1137887_34_51480_1137887_1135772_TTTB_0.pdf'
        save_dir ='/home/duycuong/home_data/vvn/OCR_MAFC/MAFC_poc/uploads_63_1526_03062021/res_v0.4'


    file_path = ''
    if file_path == '':
        list_files = get_list_file_in_folder(file_dir, ext=['pdf', 'PDF', 'jpg', 'png', 'JPG', 'PNG'])
    else:
        list_files = [file_path]
    list_files = sorted(list_files)

    for idx, f in enumerate(list_files):
        if idx < 0:
            continue
        if file_path == '':
            f = os.path.join(file_dir, f)
        print(''.ljust(200, '-'))
        print(idx, f)
        info = kie_from_file(f,
                             doc_type=type,
                             debug=False,
                             save_dir =save_dir,
                             vis=True,
                             max_pages =2)

        # save_results ()
