# -*- coding: utf-8 -*-
import os, time
import datetime
from flask import Blueprint, jsonify, render_template
from flask import request
from functools import wraps

from main_app.utils import settings
from main_app.config.general import *
from main_app.log.log import debug_log, app_log
from main_app.app import utils
import main_app.main as main
# from main_app.main_flow.kie.form import vpbank_motaikhoan, pvcombank_forms
from main_app.utils.pdf_processing.extract_pdf_image import get_num_pages
import unidecode

root = os.path.dirname(os.path.realpath(__file__))

API_KEY = settings.API_KEY
ocr_blueprint = Blueprint('ocr', __name__)

vvn_message = "Copyright©2020-2021 Công Ty VVN Phát Triển. All rights reserved. Version 2021-01-27"
UPLOAD_DIR=os.path.join(root, '../../uploads')
if not os.path.isdir(UPLOAD_DIR):
    os.mkdir(UPLOAD_DIR)


def require_appkey(view_function):
    @wraps(view_function)
    def decorated_function(*args, **kwargs):
        if request.headers.get('api-key') and request.headers.get('api-key') in API_KEY:
            return view_function(*args, **kwargs)
        else:
            res = {"result_code": 401, 'message': "api-key error"}
            return jsonify(res)
    return decorated_function


@ocr_blueprint.route("/vvn_kie/version")
def homepage():
    return vvn_message


@ocr_blueprint.route('/vvn_kie/read_document', methods=['POST'])
@require_appkey
def read_document():
    if request.method == 'GET':
        return vvn_message
    result = {
        'result_code': 500,
    }

    app_log.info('vvn')
    license = utils.check_license()
    # license = 'OK'
    if license != 'OK':
        debug_log.info("LICENSE NOT ACTIVE")
        result = {
            'result_code': 501,
        }
        return jsonify(result)

    request_id = 'vvn-test'
    doc_type = ''
    if 'document' in request.files:
        data = request.values
        if 'request_id' in data:
            request_id = data['request_id']

        if 'document_type' in data:
            doc_type = data['document_type']

        tmp_file_path = os.path.join(root, '../../uploads', '{}_{}_{}'.format(datetime.datetime.now().strftime("%Y%m%d-%H%M%S"),
                                                                               request_id, request.files['document'].filename))
        with open(tmp_file_path, 'wb') as fo:
            ct = request.files['document'].read()
            fo.write(ct)
    else:
        data = request.get_json()
        result = {
            'result_code': 500,
            'msg': 'There is no input pdf file. Only support FileForm'
        }
        return jsonify(result)

    try:
        error, res  = main.kie_from_file(tmp_file_path, doc_type=doc_type, max_pages=max_page_per_pdf)
        if error is None:
            result = res
            result['result_code'] = 200
        else:
            if error == 'input_error':
                result['table_data'] = [('Error','Loi file dau vao')]
            elif error == 'engine_error':
                result['table_data'] = [('Error','OCR that bai')]

    except:
        msg_detail = traceback.format_exc()
        debug_log.error(msg_detail)
        result['msg'] = msg_detail
        print(msg_detail)

    return jsonify(result)

@ocr_blueprint.route('/recognition', methods=['POST'])
@require_appkey
def read_freeform():
    core_time_begin = time.time()
    if request.method == 'GET':
        return vvn_message
    result = {
        'result_code': 501,
    }

    app_log.info('vvn')
    license = utils.check_license()
    # license = 'OK'
    if license != 'OK':
        debug_log.info("LICENSE NOT ACTIVE")
        result = {
            'result_code': 501,
        }
        return jsonify(result)

    request_id = 'vvn-test'
    doc_type = ''
    input_OK =True
    if 'noiDungFile' in request.files:
        # xu ly dau vao pdf, jpg, png
        file_path = request.files['noiDungFile'].filename
        filename, file_extension = os.path.splitext(file_path)
        if file_extension.lower() in ['.pdf','.jpg','.png']:

            data = request.values
            if 'request_id' in data:
                request_id = data['request_id']

            if 'maGiayTo' in data:
                doc_type = data['maGiayTo']
                if doc_type not in ['GPLX', 'TTTB', 'BHYT']:
                    input_OK = False
                    result = {
                        'result_code': 401,
                        'msg': 'maGiayTo: '+doc_type+ ' not in GPLX, BHYT or TTTB'  # MAFC gui sai form
                    }
            else:
                input_OK = False
                result = {
                    'result_code': 401,
                    'msg': 'Please provide maGiayTo (GPLX, BHYT or TTTB)'  # MAFC gui sai form
                }


            # Luu file de debug
            tmp_file_path = os.path.join(upload_dir, '{}_{}_{}'.format(datetime.datetime.now().strftime("%Y%m%d-%H%M%S"),
                                                                                   request_id, unidecode.unidecode(request.files['noiDungFile'].filename)))
            with open(tmp_file_path, 'wb') as fo:
                ct = request.files['noiDungFile'].read()
                fo.write(ct)


        else:
            input_OK = False
            result = {
                'result_code': 401,
                'msg': 'Not pdf, jpg or png file!' #MAFC gui sai form
            }

    else:
        result = {
            'result_code': 401,
            'msg': 'Please provide "noiDungFile". Only support FileForm'
        }
        return jsonify(result)

    if input_OK:
        try:
            #check input file
            input_OK = False
            if file_extension.lower() == '.pdf':
                num_page = get_num_pages(tmp_file_path)
                if num_page>0:
                    if num_page> max_page_per_pdf:
                        result['result_code'] = 401
                        result['msg'] = 'file pdf co nhieu hon {} trang'.format(max_page_per_pdf)
                    else:
                        input_OK = True
                else:
                        result['result_code'] = 401
                        result['msg'] = 'Loi file pdf dau vao'
            else:
                input_OK = True

            # do recogntion
            if input_OK:
                error, res = main.kie_from_file(tmp_file_path, doc_type=doc_type, max_pages=max_page_per_pdf)
                if error is None:
                    res = res['raw_data']
                    core_time_end = time.time()
                    result['result'] = res
                    result['maGiayTo'] = doc_type
                    result['elapsed_time'] = round(1000*(core_time_end-core_time_begin))
                    result['server_name'] = ''
                    result['server_ver'] = server_version

                    engine_ok = True
                    if 'dcx' not in result['result'].keys():
                        engine_ok =False
                    elif result['result']['dcx']<0.7:
                        engine_ok =False
                    from main_app.main_flow.kie.rules import count_NA_field

                    if doc_type in ['TTTB']:
                        if res['hoVaTen']=='N/A' or res['namSinh']=='N/A' or res['soGiayTo']=='N/A':
                            engine_ok = False
                    if doc_type in ['GPLX', 'BHYT'] and count_NA_field(res)>0:
                        engine_ok = False

                    if engine_ok:
                        result['result_code'] = 200
                        result['msg'] = 'OCR thanh cong'
                    else:
                        result['result_code'] = 400
                        result['msg'] = 'OCR that bai'
                else:
                    core_time_end = time.time()
                    result['maGiayTo'] = doc_type
                    result['elapsed_time'] = round(1000*(core_time_end-core_time_begin))
                    result['server_name'] = ''
                    result['server_ver'] = server_version
                    if error =='input_error':
                        result['msg'] = 'Loi file dau vao'
                        result['result_code'] = 401
                    elif error =='engine_error':
                        result['msg'] = 'OCR that bai'
                        result['result_code'] = 400
        except:
            msg_detail = traceback.format_exc()
            debug_log.error(msg_detail)
            result['msg'] = 'OCR that bai. Chi tiet loi: '+ msg_detail
            print(msg_detail)

    return jsonify(result)


@ocr_blueprint.route('/vvn_kie/read_pvcombank_documents', methods=['POST'])
@require_appkey
def read_pvcombank_documents():
    if request.method == 'GET':
        return vvn_message
    result = {
        'result_code': 500,
    }

    app_log.info('vvn')
    license = utils.check_license()
    # license = 'OK'
    if license != 'OK':
        debug_log.info("LICENSE NOT ACTIVE")
        result = {
            'result_code': 501,
        }
        return jsonify(result)

    # get data
    data = request.values
    if data is None:
        data = request.get_json()
    if 'request_id' in data:
        request_id = data['request_id']
    else:
        request_id = "test"
    save_input_data_dir = os.path.join(root, '../../uploads', '{}_{}'.format(datetime.datetime.now().strftime("%Y%m%d-%H%M%S"),
                                                                             request_id))
    if not os.path.isdir(save_input_data_dir):
        os.mkdir(save_input_data_dir)
    list_input_data = {"document_gpdkkd": '',
                       "document_mst": '',
                       "document_visa": '',
                       "document_ID": []}
    for k in list_input_data.keys():
        if k == 'document_ID':
            for x in range(10):
                k1 = '{}{}'.format(k, x)
                if k1 in request.files:
                    tmp_file_path = os.path.join(save_input_data_dir, request.files[k1].filename)
                    with open(tmp_file_path, 'wb') as fo:
                        ct = request.files[k1].read()
                        fo.write(ct)
                    list_input_data[k].append(tmp_file_path)
        else:
            if k in request.files:
                tmp_file_path = os.path.join(save_input_data_dir, request.files[k].filename)
                with open(tmp_file_path, 'wb') as fo:
                    ct = request.files[k].read()
                    fo.write(ct)
                list_input_data[k] = tmp_file_path

    try:
        result = pvcombank_forms.read_all_documents(list_input_data)
        result['result_code'] = 200
    except:
        msg_detail = traceback.format_exc()
        debug_log.error(msg_detail)
        result['msg'] = msg_detail
        print(msg_detail)

    return jsonify(result)


def require_appkey(view_function):
    @wraps(view_function)
    def decorated_function(*args, **kwargs):
        if request.headers.get('api-key') and request.headers.get('api-key') in API_KEY:
            return view_function(*args, **kwargs)
        else:
            res = {"result_code": 401}
            return jsonify(res)
    return decorated_function


@ocr_blueprint.route("/vpbank")
def index_vpbank():
    return render_template("index_vpbank.html")

@ocr_blueprint.route("/pvcombank")
def index_pvcombank():
    return render_template("index_pvcombank.html")


@ocr_blueprint.route("/gpdkkd")
def index_vgpdkkd():
    return render_template("index_gpdkkd.html")


@ocr_blueprint.route("/sale_contract")
def index_sale_contract():
    return render_template("index_sale_contract.html")


@ocr_blueprint.route("/giay_dkmst")
def index_giay_dkmst():
    return render_template("index_giay_dkmst.html")

@ocr_blueprint.route("/BHYT")
def index_BHYT():
    return render_template("index_BHYT.html")

@ocr_blueprint.route("/giayLC")
def index_giayLC():
    return render_template("index_giayLC.html")

@ocr_blueprint.route("/TTTB")
def index_TTTB():
    return render_template("index_TTTB.html")

@ocr_blueprint.route("/travel_reserve")
def index_travel_reserve():
    return render_template("index_travel_reserve.html")

@ocr_blueprint.route("/general")
def index_general():
    return render_template("index_general.html")

@ocr_blueprint.route("/demo")
def index_demo():
    return render_template("index.html")

import traceback
try:
    print('Test pass. Config', use_case)
except:
    print(traceback.format_exc())

print("GOGO!!!")