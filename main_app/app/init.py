import os

from flask import Flask
from flask_cors import CORS
from main_app.config.general import USING_LM
from main_app.config.general import USING_GPU

# if not USING_GPU:
#     os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"  # see issue #152
#     os.environ["CUDA_VISIBLE_DEVICES"] = "-1"


def create_app():

    # instantiate the app
    app = Flask(__name__)

    # enable CORS
    CORS(app)

    if USING_LM:
        from app.utils import limiter
        limiter.init_app(app)

    # register blueprints
    from app.ocr.views import ocr_blueprint
    app.register_blueprint(ocr_blueprint)

    try:
        from app.ocr.uploads import upload_blueprint
        app.register_blueprint(upload_blueprint)
    except :
        pass
        

    return app
