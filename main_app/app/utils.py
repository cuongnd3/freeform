# -*- coding: utf-8 -*-
from main_app.config.general import LM_CONFIG,USING_LM
if USING_LM:
    from flask_limiter import Limiter
    from flask_limiter.util import get_remote_address
    limiter = Limiter(
        key_func=get_remote_address,
        default_limits=["{} per month".format(LM_CONFIG)])

print('Copyright©2018-2019 Công Ty VVN Phát Triển. All rights reserved')

import os
from cryptography.fernet import Fernet
import datetime
import uuid
import subprocess

root = os.path.dirname(os.path.realpath(__file__))

#Fernet.generate_key()

PRIVATE_KEY = b'-c2Ho8eM2BXhVexToMK6EWdtNKBQOf7aulbeEVKvIDY='
cipher_suite = Fernet(PRIVATE_KEY)

def gen_key(data):
    print(data)
    
    key = cipher_suite.encrypt(data.encode()).decode()
    with open('key.bin','w') as f:
        f.write(key)
        f.close()
    _,fromdate,_ = data.split(' ')
    print(fromdate)
    key_date = cipher_suite.encrypt(fromdate.encode()).decode()
    with open('date.bin','w') as f:
        f.write(key_date)
        f.close()

def get_id():
    proc = subprocess.Popen(["cat /sys/devices/virtual/dmi/id/product_uuid"],stdout=subprocess.PIPE, shell=True)
    outs, errs = proc.communicate()
    return outs.rstrip().decode()

global tmp_file 
tmp_file = None

def check_license():
    return 'OK'
    global tmp_file
    if tmp_file is None :
        result = parse_license()
        if result == 'OK' :
            now = get_datetime().strftime("%Y-%m-%d")
            if not parse_date():
                return 'NOW NOT RIGHT'
            if not os.path.exists(os.path.join(root,'tmp')) :
                os.mkdir(os.path.join(root,'tmp'))
            tmp_file = os.path.join(root,'tmp',"{}_{}.bin".format(now,str(uuid.uuid4())))
            save_date(now)
            return 'OK'
        else :
            return result
    else:
        date = os.path.basename(tmp_file).split('_')[0]
        now = get_datetime().strftime("%Y-%m-%d")
        if now == date :
            return 'OK'
        else:
            result = parse_license()
            if result == 'OK' :
                now = get_datetime().strftime("%Y-%m-%d")
                if not parse_date():
                    return 'NOW NOT RIGHT'
                if not os.path.exists(os.path.join(root,'tmp')) :
                    os.mkdir(os.path.join(root,'tmp'))
                tmp_file = os.path.join(root,'tmp',"{}_{}.bin".format(now,str(uuid.uuid4())))
                save_date(now)
                return 'OK'
            else :
                return result

def save_date(data):
    key_date = cipher_suite.encrypt(data.encode()).decode()
    with open(os.path.join(root,'tmp','date.bin'),'w') as f:
        f.write(key_date)
        f.close()

def parse_date():
    file_path = os.path.join(root,'tmp','date.bin')
    if not os.path.exists(file_path) :
        return False
    with open(file_path,'r') as f:
        key = f.read()
        f.close()
    try:
        data = cipher_suite.decrypt(key.encode()).decode()
        data = datetime.datetime.strptime(data,"%Y-%m-%d")
        now = get_datetime()
        if now < data :
            return False
    except :
        return False
    return True

def parse_license():
    file_path = os.path.join(root,'tmp','key.bin')
    if not os.path.exists(file_path) :
        return 'KEY NOT EXISTS'
    with open(file_path,'r') as f:
        key = f.read()
        f.close()

    data = cipher_suite.decrypt(key.encode()).decode()
    try:
        hw,fromdate,todate = data.split(' ')
        fromdate=datetime.datetime.strptime(fromdate,"%Y-%m-%d")
        if todate != '*' :
            todate=datetime.datetime.strptime(todate,"%Y-%m-%d")
    except :
        return 'PARSE KEY ERROR'
    now = get_datetime()
    uuid_path = "/sys/devices/virtual/dmi/id/product_uuid"
    if not os.path.exists(uuid_path) :
        return 'UUID NOT EXISTS'
    uid = get_id()
    if todate != '*' :
        if hw != uid or now < fromdate or now > todate:
            return 'LICENSE NOT RIGHT'
    else:
        if hw != uid or now < fromdate:
            return 'LICENSE NOT RIGHT'
    return 'OK'
    

def get_datetime():
    now = datetime.datetime.now()
    return datetime.datetime(now.year,now.month,now.day)

if __name__ == "__main__":
    key = "00000000-0000-0000-0000-309C23A7B3E5 2019-05-28 *"
    key=key.upper()
    #gen_key(key)