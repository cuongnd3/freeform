"use strict";

//const SERVER_URL = "http://123.24.142.99:9001/";
const SERVER_URL = "http://0.0.0.0:9100/";
var originalHeight;
var formData;
var request_id = "id1606926441300";
var total_file = 4;
var number_files = 0;

function show_res_table(data){
    console.log("start make table");
    console.log(data);
    data = data.table_data

    var table_res = document.getElementById('table-test');
    if (document.getElementById('table-body'))
    {
        var old_body = document.getElementById('table-body');
        old_body.remove();
    };
    var tbdy = document.createElement('tbody');
    tbdy.id = 'table-body'
    // add table row
    for (var row_id in data)
    {
        var tr = document.createElement('tr');
        var row = data[row_id]
        console.log(row_id);
        console.log(row);
        if (row.length == 2)
        {
            var td = document.createElement('td');
            td.innerHTML  = row_id;
            tr.appendChild(td);
        }
        if (row[0] == "")
        {
                var td = document.createElement('td');
                td.className = "text-left";
                td.innerHTML  = row[1];
                td.setAttribute("colspan", "3");
                tr.appendChild(td)
        }
        else
        {
            for (var col_id=0; col_id < row.length; col_id++)
            {
                var td = document.createElement('td');
                td.className = "text-left";
                td.innerHTML  = row[col_id]
                tr.appendChild(td)
            }
        }
        tbdy.appendChild(tr)
    }
    table_res.appendChild(tbdy);
}


function show_pvcombank(data)
{
    console.log("start make table");
    var table_res = document.getElementById('table-test');
    if (document.getElementById('table-body'))
    {
        var old_body = document.getElementById('table-body');
        old_body.remove();
    };
    var tbdy = document.createElement('tbody');
    tbdy.id = 'table-body'
    var table_names = ["Kiểm tra chéo thông tin", "Giấy phép đăng ký kinh doanh", "Giấy chứng nhận mã số thuế", "Visa",
     "Giấy tờ tùy thân_00", "Giấy tờ tùy thân_01", "Giấy tờ tùy thân_02", "Giấy tờ tùy thân_03", "Giấy tờ tùy thân_04", "Giấy tờ tùy thân_05"]
    // add table row
    for (var tn_id in table_names)
    {
        var tn = table_names[tn_id]
        if (!(tn in data))
        {
            continue;
        }
        var table_data = data[tn]['table_data']

        // print 1 row for group infor (1 kind of document) - highlight by yellow background
        var tr = document.createElement('tr');
        var td = document.createElement('td');
        td.setAttribute('colspan', 3);
        td.setAttribute('bgcolor', "#ffff00");
        td.innerHTML  = tn;
        td.className = "text-left";
        tr.appendChild(td)
        tbdy.appendChild(tr)
        // print information of 1 document
        for (var row_id in table_data)
        {
            var tr = document.createElement('tr');
            var row = table_data[row_id]
            console.log(row_id);
            console.log(row);
            if (row.length == 2)
            {
                var td = document.createElement('td');
                td.innerHTML  = row_id;
                tr.appendChild(td);
            }
            if (row[0] == "")
            {
                    var td = document.createElement('td');
                    td.className = "text-left";
                    td.innerHTML  = row[1];
                    td.setAttribute("colspan", "3");
                    tr.appendChild(td)
            }
            else
            {
                for (var col_id=0; col_id < row.length; col_id++)
                {
                    var td = document.createElement('td');
                    td.className = "text-left";
                    td.innerHTML  = row[col_id]
                    tr.appendChild(td)
                }
            }
            tbdy.appendChild(tr)
        }
    }
    table_res.appendChild(tbdy);
}

function read_vpbank_motaikhoan(e) {
    console.log("start get vpbank");
    $('#info-loader').show();
    $.ajax({
            type:'POST',
            beforeSend: function(request) {
                request.setRequestHeader("api-key", '7c8ba773-64cd-4ba5-a9bd-f035f06d0149');
            },
            url: SERVER_URL + '/vvn_kie/vpbank_motaikhoan',
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
                show_res_table(data);
                $('#info-loader').hide();
                    },
            error: function(data){
                console.log("error");
                console.log(data);
                $('#info-loader').hide();
            }
        });
}

function read_GPLX(){
    console.log("start get GPLX");
    $('#info-loader').show();
    $.ajax({
            type:'POST',
            beforeSend: function(request) {
                request.setRequestHeader("key", 'Z9Gsi9B0sOK1QVZLayoAMZ2wJPi89Es9');
            },
            url: 'https://api.cloudekyc.com/v3.2/ocr/recognition',
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
                console.log(data);
                showDriverData(data);
                $('#info-loader').hide();
            },
            error: function(data){
                console.log("error");
                console.log(data);
                $('#info-loader').hide();
            }
        });
    }

function read_contract(e) {
    console.log("start get bctc");
    formData.append("document_type", 'sale_contract');
    $('#info-loader').show();
    $.ajax({
            type:'POST',
            beforeSend: function(request) {
                request.setRequestHeader("api-key", '7c8ba773-64cd-4ba5-a9bd-f035f06d0149');
            },
            url: SERVER_URL + '/vvn_kie/read_document',
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
                show_res_table(data);
                $('#info-loader').hide();
                    },
            error: function(data){
                console.log("error");
                console.log(data);
                $('#info-loader').hide();
            }
        });
}

function read_MST(e) {
    console.log("start get MST");
    formData.append("document_type", 'giay_dkmst');
    $('#info-loader').show();
    $.ajax({
            type:'POST',
            beforeSend: function(request) {
                request.setRequestHeader("api-key", '7c8ba773-64cd-4ba5-a9bd-f035f06d0149');
            },
            url: SERVER_URL + '/vvn_kie/read_document',
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
                show_res_table(data);
                $('#info-loader').hide();
                    },
            error: function(data){
                console.log("error");
                console.log(data);
                $('#info-loader').hide();
            }
        });
}

function read_BHYT(e) {
    console.log("start get BHYT");
    formData.append("document_type", 'BHYT');
    $('#info-loader').show();
    $.ajax({
            type:'POST',
            beforeSend: function(request) {
                request.setRequestHeader("api-key", '7c8ba773-64cd-4ba5-a9bd-f035f06d0149');
            },
            url: SERVER_URL + '/vvn_kie/read_document',
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
                show_res_table(data);
                $('#info-loader').hide();
                    },
            error: function(data){
                console.log("error");
                console.log(data);
                $('#info-loader').hide();
            }
        });
}

function read_giayLC(e) {
    console.log("start get giayLC");
    formData.append("document_type", 'giayLC');
	console.log(formData);
    $('#info-loader').show();
    $.ajax({
            type:'POST',
            beforeSend: function(request) {
                request.setRequestHeader("api-key", '7c8ba773-64cd-4ba5-a9bd-f035f06d0149');
            },
            url: SERVER_URL + '/vvn_kie/read_document',
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
                show_res_table(data);
                $('#info-loader').hide();
                    },
            error: function(data){
                console.log("error");
                console.log(data);
                $('#info-loader').hide();
            }
        });
}

function read_giay_ky_quy(e) {
    console.log("start get giay_ky_quy");
    formData.append("document_type", 'giay_ky_quy');
    $('#info-loader').show();
    $.ajax({
            type:'POST',
            beforeSend: function(request) {
                request.setRequestHeader("api-key", '7c8ba773-64cd-4ba5-a9bd-f035f06d0149');
            },
            url: SERVER_URL + '/vvn_kie/read_document',
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
                show_res_table(data);
                $('#info-loader').hide();
                    },
            error: function(data){
                console.log("error");
                console.log(data);
                $('#info-loader').hide();
            }
        });
}

function read_TTTB(e) {
    console.log("start get TTTB");
    formData.append("document_type", 'TTTB');
    $('#info-loader').show();
    $.ajax({
            type:'POST',
            beforeSend: function(request) {
                request.setRequestHeader("api-key", '7c8ba773-64cd-4ba5-a9bd-f035f06d0149');
            },
            url: SERVER_URL + '/vvn_kie/read_document',
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
                show_res_table(data);
                $('#info-loader').hide();
                    },
            error: function(data){
                console.log("error");
                console.log(data);
                $('#info-loader').hide();
            }
        });
}

function read_travel_reserve(e) {
    console.log("start get TTTB");
    formData.append("document_type", 'travel_reserve');
    $('#info-loader').show();
    $.ajax({
            type:'POST',
            beforeSend: function(request) {
                request.setRequestHeader("api-key", '7c8ba773-64cd-4ba5-a9bd-f035f06d0149');
            },
            url: SERVER_URL + '/vvn_kie/read_document',
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
                show_res_table(data);
                $('#info-loader').hide();
                    },
            error: function(data){
                console.log("error");
                console.log(data);
                $('#info-loader').hide();
            }
        });
}


function read_general(e) {
    console.log("start get general");
    formData.append("document_type", 'general');
    $('#info-loader').show();
    $.ajax({
            type:'POST',
            beforeSend: function(request) {
                request.setRequestHeader("api-key", '7c8ba773-64cd-4ba5-a9bd-f035f06d0149');
            },
            url: SERVER_URL + '/vvn_kie/read_document',
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
                show_res_table(data);
                $('#info-loader').hide();
                    },
            error: function(data){
                console.log("error");
                console.log(data);
                $('#info-loader').hide();
            }
        });
}



function read_gpdkkd(e) {
    console.log("start get gpdkkd");
    formData.append("document_type", 'business_registration');
    $('#info-loader').show();
    $.ajax({
            type:'POST',
            beforeSend: function(request) {
                request.setRequestHeader("api-key", '7c8ba773-64cd-4ba5-a9bd-f035f06d0149');
            },
            url: SERVER_URL + '/vvn_kie/read_document',
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
                show_res_table(data);
                $('#info-loader').hide();
                    },
            error: function(data){
                console.log("error");
                console.log(data);
                $('#info-loader').hide();
            }
        });
}


function read_pvcombank(e) {
    console.log("start get pvcombank");
    $('#info-loader').show();
    $.ajax({
            type:'POST',
            beforeSend: function(request) {
                request.setRequestHeader("api-key", '7c8ba773-64cd-4ba5-a9bd-f035f06d0149');
            },
            url: SERVER_URL + '/vvn_kie/read_pvcombank_documents',
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
                show_pvcombank(data);
                $('#info-loader').hide();
                    },
            error: function(data){
                console.log("error");
                console.log(data);
                $('#info-loader').hide();
            }
        });
//    number_files = 0;
}


$(document).ready(function() {
//    $('#submit_pvcombank').attr('disabled','disabled');
    $("#document_gpdkkd").change(function(e) {
        console.log("Upload document_gpdkkd");
        number_files += 1;
//        if (total_file == number_files)
//        {
//            $('#submit_pvcombank').removeAttr('disabled');
//        }
        if (number_files == 1)
        {
            formData = new FormData();
            request_id = 'id' + (new Date()).getTime();
            formData.append("request_id", request_id);
        }
        if (formData.has("document_gpdkkd"))
        {
            formData.set("document_gpdkkd", e.originalEvent.srcElement.files[0]);
        }
        else
        {
            formData.append("document_gpdkkd", e.originalEvent.srcElement.files[0]);
        }
    });

    $("#document_mst").change(function(e) {
        console.log("Upload document_mst");
        number_files += 1;
//        if (total_file == number_files)
//        {
//            $('#submit_pvcombank').removeAttr('disabled');
//        }
        if (number_files == 1)
        {
            formData = new FormData();
            request_id = 'id' + (new Date()).getTime();
            formData.append("request_id", request_id);
        }
        if (formData.has("document_mst"))
        {
            formData.set("document_mst", e.originalEvent.srcElement.files[0]);
        }
        else
        {
            formData.append("document_mst", e.originalEvent.srcElement.files[0]);
        }
    });

    $("#document_visa").change(function(e) {
        console.log("Upload document_visa");
        number_files += 1;
//        if (total_file == number_files)
//        {
//            $('#submit_pvcombank').removeAttr('disabled');
//        }
        if (number_files == 1)
        {
            formData = new FormData();
            request_id = 'id' + (new Date()).getTime();
            formData.append("request_id", request_id);
        }
        if (formData.has("document_visa"))
        {
            formData.set("document_visa", e.originalEvent.srcElement.files[0]);
        }
        else
        {
            formData.append("document_visa", e.originalEvent.srcElement.files[0]);
        }
    });

    $("#document_ID").change(function(e) {
        console.log("Upload document_ID");
        number_files += 1;
//        if (total_file == number_files)
//        {
//            $('#submit_pvcombank').removeAttr('disabled');
//        }
        if (number_files == 1)
        {
            formData = new FormData();
            request_id = 'id' + (new Date()).getTime();
            formData.append("request_id", request_id);
        }
        var ins = document.getElementById('document_ID').files.length;
        for (var x = 0; x < ins; x++) {
            if (formData.has("document_ID"+x))
            {
                formData.delete("document_ID"+x);
            }
        }
        for (var x = 0; x < ins; x++) {
            console.log('add file' + x);
            console.log(document.getElementById('document_ID').files[x]);
            formData.append("document_ID"+x, document.getElementById('document_ID').files[x]);
        }

    });

    $("#document").change(function(e) {
        console.log("Upload file");
        formData = new FormData();
        request_id = 'id' + (new Date()).getTime();
        formData.append("document", e.originalEvent.srcElement.files[0]);
        formData.append("request_id", request_id);
    });

    $("#submit_pvcombank").on("click submit", read_pvcombank);
    $("#submit").on("click submit", read_vpbank_motaikhoan);
    $("#submit_GPLX").on("click submit", read_GPLX);
    $("#submit_gpdkkd").on("click submit", read_gpdkkd);
    $("#submit_contract").on("click submit", read_contract);
    $("#submit_giay_dkmst").on("click submit", read_MST);
    $("#submit_BHYT").on("click submit", read_BHYT);
    $("#submit_giayLC").on("click submit", read_giayLC);
    $("#submit_giay_ky_quy").on("click submit", read_giay_ky_quy);
    $("#submit_TTTB").on("click submit", read_TTTB);
    $("#submit_travel_reserve").on("click submit", read_travel_reserve);
    $("#submit_general").on("click submit", read_general);

    originalHeight = $("#result").prop("scrollHeight");
});
