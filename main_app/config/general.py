import os
PROJ_ROOT =  os.path.dirname(__file__)+'/..'

metadata_dir = os.path.join(PROJ_ROOT,'data/metadata')
font_path = os.path.join(PROJ_ROOT,'utils/arial.ttf')
upload_dir = os.path.join(PROJ_ROOT,'uploads')

DEBUG_MODE = True
DEBUG_EXCEL = False
DEBUG_CORRECT = False
DEBUG_PATH = os.path.join(PROJ_ROOT, 'debug') + '/'
DEBUG_PRINT = True
DEBUG_SHOW_TIME = False
DEBUG_SHOW_TIME_ALL = (not DEBUG_SHOW_TIME) and False
USING_LM = False
LM_CONFIG = 1000000
USING_UPLOAD_FILE = True
USING_PDF = False
USING_GPU = False
GPU_FRACTION_TF_MODEL = 0.6
defined_excel_paths = {
        'TT133/2016-BTC': 'format/formatTT133-2016.xlsx',
        'TT200/2014-BTC': 'format/formatTT200-2014.xlsx'}


supported_doc_type = ['BHYT','GPLX','TTTB','gpdkkd','giayLC','sale_contract', 'finance_invoice','travel_reserve']
api_doc_type = ['GPLX']
config_by_type = ['BHYT', 'GPLX', 'TTTB', 'travel_reserve']
max_page_per_pdf = 9999
server_version ='VVN AI v0.1'

#text detection
text_detection = 'paddle_v11'

#doc rotation
doc_rotation = 'mobilenetv3'
box_rectify_ckpt_path = os.path.join(PROJ_ROOT,'main_flow/doc_rot/mobilenet/weights/mobilenetv3-Epoch-487-Loss-0.03-Acc-0.99.pth')
fix_text_box_angle = False

#text recognition
merge_text_box = True
text_recognition = 'paddle_v11'

#kie
kie_model = 'rule_base'

use_case = 'VNA' #all, MAFC , 'VNA
if use_case =='MAFC':
        server_version ='MAFC v0.4'
        max_page_per_pdf =2
        supported_doc_type = ['BHYT', 'GPLX', 'TTTB']
        api_doc_type = ['GPLX']
        text_detection = 'paddle_v11'
        doc_rotation = 'mobilenetv3'
        text_recognition = 'paddle_v11'
        merge_text_box = True
        kie_model = 'rule_base'
elif use_case =='VNA':
        server_version ='VNA v0.1'
        max_page_per_pdf =1
        supported_doc_type = ['travel_reserve']
        api_doc_type = []
        text_detection = 'paddle_v11'
        doc_rotation = 'mobilenetv3'
        text_recognition = 'paddle_v11'
        merge_text_box = True
        kie_model = 'rule_base'
        # kie_model = None