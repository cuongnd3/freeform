import requests, json, os, time
from main_app.utils.common import get_list_file_in_folder


def ocr_mafc(api_url, api_key, image_path, type='TTTB'):
    files = {'noiDungFile': open(image_path, 'rb')}
    values = {'maGiayTo': type}

    try:
        resp = requests.post(url=api_url, files=files, data=values, headers={'key': api_key})
        res = json.loads(resp.text)
    except:
        res = {}
    print(res)
    return res


def benchmark_ocr_mafc_api(test_dir, type='TTTB'):
    api_url = 'https://api.cloudekyc.com/mafc/recognition'
    api_key = '8mq1AbRut79K19MILqj38hl8wW1Oisdw'

    list_files = get_list_file_in_folder(test_dir, ext=['pdf'])
    list_files = sorted(list_files)

    ocr_time = 0
    total_time = 0
    count=0
    for idx, f in enumerate(list_files):
        if idx < 0:
            continue
        print()
        print(idx, f)
        img_path = os.path.join(test_dir, f)
        begin = time.time()
        res = ocr_mafc(api_url, api_key, img_path, type=type)
        end = time.time()
        if 'elapsed_time' in res.keys():
            count+=1
            ocr_time += res["elapsed_time"]
            total_time += round(1000 * (end - begin))
            print('ocr time:', res["elapsed_time"], 'ms', '. Average:', ocr_time / count)
            print('total time:', round(1000 * (end - begin)), 'ms', '. Average:', total_time / count)


def ocr(api_url, api_key, image_path, type='TTTB'):
    files = {'document': open(image_path, 'rb')}
    values = {'document_type': type}

    try:
        resp = requests.post(url=api_url, files=files, data=values, headers={'api-key': api_key})
        res = json.loads(resp.text)
    except:
        res = {}
    print(res)
    return res


def benchmark_ocr_api(test_dir):
    api_url = 'http://103.143.207.63:9000/vvn_kie/read_document'
    api_key = '7c8ba773-64cd-4ba5-a9bd-f035f06d0149'

    list_files = get_list_file_in_folder(test_dir, ext=['pdf'])
    list_files = sorted(list_files)
    for idx, f in enumerate(list_files):
        if idx < 0:
            continue
        print(idx, f)
        img_path = os.path.join(test_dir, f)
        begin = time.time()
        res = ocr(api_url, api_key, img_path, type='TTTB')
        end = time.time()
        print('total time', 1000 * (end - begin), 'ms')
        kk = 1


if __name__ == '__main__':
    test_dir = '/home/duycuong/home_data/vvn/OCR_MAFC/GPLX/pdf'
    benchmark_ocr_mafc_api(test_dir, type='GPLX')
