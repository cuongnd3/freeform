import cv2, os
from main_app.utils.pdf_processing import extract_pdf_image
from main_app.tools import test_paddle_ocr
from main_app.utils.common import get_list_file_in_folder, write_icdar_result

if __name__ == '__main__':
    visualize = True
    write_icdar_txt = True
    file_dir = '/home/duycuong/home_data/vvn/OCR_MAFC/BHYT/web/imgs'
    file_path = '/data_backup/cuongnd/finance_invoice/finance_invoices_PICK/images/'
    # file_path = 'data/sale_contract/pdf/5 - AS INC.pdf'
    file_path = ''
    if file_path == '':
        list_files = get_list_file_in_folder(file_dir, ext=['pdf', 'jpg', 'png', 'JPG', 'PNG'])
    else:
        list_files = [file_path]
    list_files = sorted(list_files)

    # save_dir = file_dir
    save_dir = '/home/duycuong/Pictures/test/'
    for idx, file_name in enumerate(list_files):
        if idx < 0:
            continue
        print(idx, file_name)
        if file_path == '':
            file_name = os.path.join(file_dir, file_name)
        if 'pdf' in file_name:
            list_img, save_dir, list_img_path = extract_pdf_image.get_list_img_from_pdf(file_name)
        else:
            list_img_path = [file_name]
        for img_path in list_img_path:
            img = cv2.imread(img_path)
            det_res, txts, scores, res_img = test_paddle_ocr.ocr(img, vis=False, rotate_img=False,
                                                                 rotate_and_extend_box=True)
            if visualize:
                save_path =  file_name.replace(file_dir,save_dir ).split('.')[0] + '_res.png'
                cv2.imwrite(save_path, res_img)
            if write_icdar_txt:
                icdar_path = file_name.replace(file_dir,save_dir ).split('.')[0] + '.txt'
                write_icdar_result(det_res=det_res,
                                   txts=txts,
                                   icdar_path=icdar_path,
                                   default_val=None)
    print('Done')
