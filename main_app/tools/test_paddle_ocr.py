import os
import cv2
from PIL import Image
import numpy as np
from main_app.config.general import DEBUG_PRINT, font_path
from main_app.main_flow.text_det.text_det_class  import TextDet
from main_app.main_flow.text_det.paddle_v11.tools.infer.utility import draw_ocr_box_txt
from main_app.main_flow.text_rec.paddle_v11 import text_recognition_paddle
from main_app.utils.image_processing import rotate_and_crop
from main_app.main_flow.doc_rot.rotation_correction import rotate_image_and_boxes_to_horizontal

this_dir = os.path.dirname(__file__)

def group_text_box_to_line(img, polys, txts, scores, slope_ths=0.5, ycenter_ths=0.6, height_ths=0.7, width_ths=1.0,
                           add_margin=0.05, vis=False, offset_y=None):
    """
    :param polys:
    :param slope_ths:
    :param ycenter_ths: ycenter nho -> khi merge hon
    :param height_ths: height_ths nho --> kho merge hon - so sanh chieu cao cua box
    chech lech giua 2 box < height_ths * height(box1) --> merge
    :param width_ths: distance of 2 box (in X axis) < width_ths * chracter with --> merge
    :param add_margin:
    :return: [line1, line2,...]
    linei = [box0, box1,...]
    box1 = [x1, y1, x2, y2, cy, h, text, score]
    """
    if len(txts) == 0:
        return [], img
    # poly top-left, top-right, low-right, low-left
    horizontal_list, free_list, combined_list, merged_list = [], [], [], []

    for i, poly in enumerate(polys):
        slope_up = (poly[3] - poly[1]) / np.maximum(10, (poly[2] - poly[0]))
        slope_down = (poly[5] - poly[7]) / np.maximum(10, (poly[4] - poly[6]))
        if max(abs(slope_up), abs(slope_down)) < slope_ths:
            x_max = max([poly[0], poly[2], poly[4], poly[6]])
            x_min = min([poly[0], poly[2], poly[4], poly[6]])
            y_max = max([poly[1], poly[3], poly[5], poly[7]])
            y_min = min([poly[1], poly[3], poly[5], poly[7]])
            horizontal_list.append(
                [x_min, x_max, y_min, y_max, 0.5 * (y_min + y_max), y_max - y_min, txts[i], scores[i]])
        else:
            height = np.linalg.norm([poly[6] - poly[0], poly[7] - poly[1]])
            margin = int(1.44 * add_margin * height)

            theta13 = abs(np.arctan((poly[1] - poly[5]) / np.maximum(10, (poly[0] - poly[4]))))
            theta24 = abs(np.arctan((poly[3] - poly[7]) / np.maximum(10, (poly[2] - poly[6]))))
            # do I need to clip minimum, maximum value here?
            x1 = poly[0] - np.cos(theta13) * margin
            y1 = poly[1] - np.sin(theta13) * margin
            x2 = poly[2] + np.cos(theta24) * margin
            y2 = poly[3] - np.sin(theta24) * margin
            x3 = poly[4] + np.cos(theta13) * margin
            y3 = poly[5] + np.sin(theta13) * margin
            x4 = poly[6] - np.cos(theta24) * margin
            y4 = poly[7] + np.sin(theta24) * margin

            free_list.append([[x1, y1], [x2, y2], [x3, y3], [x4, y4]])
    horizontal_list = sorted(horizontal_list, key=lambda item: item[4])
    if len(horizontal_list) > 0:
        character_width = min([poly[1] - poly[0] for poly in horizontal_list if poly[1] - poly[0] != 0])
    else:
        character_width = 0
    # combine box by "x" location and height

    # combine box in same line
    new_box = []
    for poly in horizontal_list:
        if len(new_box) == 0:
            b_height = [poly[5]]
            b_ycenter = [poly[4]]
            new_box.append(poly)
        else:
            # comparable height and comparable y_center level up to ths*height
            if (abs(np.mean(b_height) - poly[5]) < height_ths * np.mean(b_height)) and (
                    abs(np.mean(b_ycenter) - poly[4]) < ycenter_ths * np.mean(b_height)):
                b_height.append(poly[5])
                b_ycenter.append(poly[4])
                new_box.append(poly)
            else:
                b_height = [poly[5]]
                b_ycenter = [poly[4]]
                combined_list.append(new_box)
                if vis:
                    cbimg = img.copy()
                    for b in new_box:
                        cv2.rectangle(cbimg, (b[0], b[2]), (b[1], b[3]), (0, 255, 0), 3)
                    cv2.namedWindow('cbimg', cv2.WINDOW_NORMAL)
                    cv2.resizeWindow('cbimg', 1280, 720)
                    cv2.imshow('cbimg', cbimg)
                    cv2.waitKey(1)
                new_box = [poly]
    combined_list.append(new_box)

    if vis:
        colors = [(0, 255, 0), (0, 0, 255)]
        for i, list_box in enumerate(combined_list):
            x1 = min([box[0] for box in list_box])
            x2 = max([box[1] for box in list_box])
            y1 = min([box[2] for box in list_box])
            y2 = max([box[3] for box in list_box])
            cv2.rectangle(img, (x1, y1), (x2, y2), colors[i % 2], 2)

        cv2.namedWindow('line', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('line', 1280, 720)
        cv2.imshow('line', img)
        cv2.waitKey(1)
    # return box in format box = [x1, y1, x2, y2, cy, h, text, score] from [x1, x2, y1, y2, cy, h, text, score]
    for i in range(len(combined_list)):
        for j in range(len(combined_list[i])):
            combined_list[i][j] = [combined_list[i][j][0], combined_list[i][j][2], combined_list[i][j][1],
                                   combined_list[i][j][3]] + combined_list[i][j][4:]

    combined_list = sorted(combined_list, key=lambda k: k[0][4])
    for i, line in enumerate(combined_list):
        combined_list[i] = sorted(line, key=lambda k: k[0])

    #add offset by number of page
    if offset_y is not None:
        for i, line in enumerate(combined_list):
            for j, pol in enumerate(line):
                pol[1]+=offset_y
                pol[3]+=offset_y
                pol[4]+=offset_y

    return combined_list, img


def assign_line(img, det_res, txts, scores):
    combined_list = group_text_box_to_line(img, det_res, txts, scores)
    # calculate space pixel
    total_character = 0
    total_width = 0
    for i in range(len(det_res)):
        bbox = det_res[i]
        x1 = int(min([bbox[i] for i in [0, 2, 4, 6]]))
        x2 = int(max([bbox[i] for i in [0, 2, 4, 6]]))
        y1 = int(min([bbox[i] for i in [1, 3, 5, 7]]))
        y2 = int(max([bbox[i] for i in [1, 3, 5, 7]]))
        total_character += len(txts[i])
        total_width += x2 - x1
    sp_px = total_width / total_character

    lines = [[] for i in range(int(img.shape[0] / sp_px))]
    for i in range(len(combined_list)):
        n = int(combined_list[i][0][4] / sp_px)
        lines[n] = combined_list[i]

    list_line_text = []
    for line in lines:
        line = [[0, 0, 0, 0, 0, 0, '']] + line
        line = sorted(line, key=lambda k: k[0])
        txt = line[0][-1]
        for i in range(1, len(line)):
            empty_width = line[i][0] - line[i - 1][1]
            n = int(empty_width / sp_px) + 1
            if n < 0: n = 0
            txt += ' ' * n + line[i][-1]
        list_line_text.append(txt)
    # for txt in list_line_text:
    #     print(txt)
    return list_line_text


def get_box_data_from_detector(img, det_res):
    list_roi_img = []
    boxes = []
    for bbox in det_res:
        x1 = int(min([bbox[i] for i in [0, 2, 4, 6]]))
        x2 = int(max([bbox[i] for i in [0, 2, 4, 6]]))
        y1 = int(min([bbox[i] for i in [1, 3, 5, 7]]))
        y2 = int(max([bbox[i] for i in [1, 3, 5, 7]]))
        if y2 <= y1 or x2 <= x1:
            continue
        roi_img = img[y1:y2, x1:x2, :]
        if roi_img.mean() < 145:
            roi_img = 255 - roi_img
        # cv2.imshow('black', roi_img)
        # cv2.waitKey(0)

        list_roi_img.append(roi_img)
        boxes.append([(int(bbox[0]), int(bbox[1])),
                      (int(bbox[2]), int(bbox[3])),
                      (int(bbox[4]), int(bbox[5])),
                      (int(bbox[6]), int(bbox[7]))])
    return boxes, list_roi_img

def get_boxes_data(img_data, boxes, extend_box=True,
                   extend_y_ratio=0.05,
                   min_extend_y=1,
                   extend_x_ratio=0.05,
                   min_extend_x=2):
    boxes_data = []
    for box_loc in boxes:
        box_loc = np.array(box_loc).astype(np.int32).reshape(-1, 1, 2)
        box_data = rotate_and_crop(img_data, box_loc, debug=True, extend=extend_box,
                                   extend_x_ratio=extend_x_ratio, extend_y_ratio=extend_y_ratio,
                                   min_extend_y=min_extend_y, min_extend_x=min_extend_x)
        boxes_data.append(box_data)
    return boxes_data

def get_rotate_extend_box_data_from_detector(img, det_res):
    boxes_list = []
    boxes = []
    for bbox in det_res:
        boxes_list.append([int(bbox[0]),
                           int(bbox[1]),
                           int(bbox[2]),
                           int(bbox[3]),
                           int(bbox[4]),
                           int(bbox[5]),
                           int(bbox[6]),
                           int(bbox[7])])
        boxes.append([(int(bbox[0]), int(bbox[1])),
                      (int(bbox[2]), int(bbox[3])),
                      (int(bbox[4]), int(bbox[5])),
                      (int(bbox[6]), int(bbox[7]))])
    list_roi_img = get_boxes_data(img, boxes_list,
                                  extend_box=True,
                                  min_extend_x=1,
                                  extend_x_ratio=0.1,
                                  min_extend_y=1,
                                  extend_y_ratio=0.1)
    return boxes, list_roi_img

def run_ocr(doc, vis=False):


    text_detector = TextDet()
    text_detector.inference(doc)





def ocr(img, list_detected_checkbox=None, drop_score=0.6, vis=False, rotate_img = False, rotate_and_extend_box = False):
    '''

    :param img:
    :param list_detected_checkbox:
    :param drop_score:
    :param vis:
    :param rotate_and_extend_box: To fix bounding box of detector --> better ocr
    :return:
    '''
    list_poly = text_detection_paddle.detect_text(img, vis=vis)

    # if list_detected_checkbox is not None:
    #     for i in range(len(det_res)):
    #         bbox = det_res[i]
    #         bx1 = int(min([bbox[i] for i in [0, 2, 4, 6]]))
    #         bx2 = int(max([bbox[i] for i in [0, 2, 4, 6]]))
    #         by1 = int(min([bbox[i] for i in [1, 3, 5, 7]]))
    #         by2 = int(max([bbox[i] for i in [1, 3, 5, 7]]))
    #         list_checkbox = []
    #         for x, y, w, h, _ in list_detected_checkbox:
    #             x1, y1, x2, y2 = x, y, x + w, y + h
    #             ix1 = max(x1, bx1)
    #             iy1 = max(y1, by1)
    #             ix2 = min(x2, bx2)
    #             iy2 = min(y2, by2)
    #             if ix2 > ix1 and (ix2 - ix1) * (iy2 - iy1) / (w * h) > 0.5:
    #                 list_checkbox.append([x1, y1, x2, y2])
    #         if len(list_checkbox) > 0:
    #             list_new_det = []
    #             list_checkbox.append([bx1, 0, 0, 0])
    #             list_checkbox.append([bx2, 0, 0, 0])
    #             list_checkbox = sorted(list_checkbox, key=lambda k: k[0])
    #             for j in range(len(list_checkbox) - 1):
    #                 nx1 = list_checkbox[j][0]
    #                 nx2 = list_checkbox[j + 1][0]
    #                 list_new_det.append([nx1, by1, nx2, by1, nx2, by2, nx1, by2])
    #             det_res[i] = list_new_det[0]
    #             det_res += list_new_det[1:]

    #add rotation of img and boxes
    if rotate_img:
        boxes_list=[]
        for bbox in det_res:
            boxes_list.append({'coors': bbox, 'data': ''})

        img, boxes_list = rotate_image_and_boxes_to_horizontal(img, boxes_list, debug=False)
        det_res =[]
        for bbox in boxes_list:
            det_res.append(np.array(bbox['coors']))

    if not rotate_and_extend_box:
        boxes, list_roi_img = get_box_data_from_detector(img, det_res)
    else:
        boxes, list_roi_img = get_rotate_extend_box_data_from_detector(img, det_res)



    rec_res, rec_time = text_recognition_paddle.text_recognizer_vn(list_roi_img)
    if DEBUG_PRINT: print("Recognition time: %s (ms):" % (rec_time * 1000))
    txts = [rec_res[i][0] for i in range(len(rec_res))]
    scores = [rec_res[i][1] for i in range(len(rec_res))]

    # remove text with low score and text only contain special character
    sc = ",./;'[]=-\!@#$%^&*()_+{}|:?><"
    kept_idx = [i for i in range(len(scores)) if scores[i] > drop_score and not txts[i] in sc]
    boxes = [boxes[i] for i in kept_idx]
    det_res = [[box[0][0], box[0][1],
                box[1][0], box[1][1],
                box[2][0], box[2][1],
                box[3][0], box[3][1]] for box in boxes]
    txts = [txts[i] for i in kept_idx]
    scores = [scores[i] for i in kept_idx]

    image = Image.fromarray(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
    draw_img = draw_ocr_box_txt(
        image,
        boxes,
        txts,
        scores,
        drop_score=drop_score,
        font_path=font_path)
    res_img = draw_img[:, :, ::-1]
    # save result to ICDAR format

    if vis:
        cv2.namedWindow('res', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('res', 1280, 720)
        cv2.imshow('res', res_img)
        cv2.waitKey(1)
    return det_res, txts, scores, res_img
