#!/usr/bin/env python3
import os, sys
# from flask_migrate import MigrateCommand
# from flask_script import Manager, Server

from main_app.app.init import create_app

app = create_app()

if __name__ == '__main__':
    app.run(port=9100, host='0.0.0.0')