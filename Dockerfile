#FROM nvidia/cuda:10.1-cudnn7-devel-ubuntu18.04
FROM ubuntu:18.04

## for apt to be noninteractive
ENV DEBIAN_FRONTEND noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN true

# ==================================================================
# apache
# ------------------------------------------------------------------
RUN apt-get update
RUN apt-get install -y wget bash zip rsync git curl build-essential software-properties-common \
                apt-utils
RUN apt-get update && apt-get install -y --fix-missing \
    build-essential \
    ca-certificates \
    cmake \
    wget \
    net-tools \
    telnet \
    vim

ENV BLASLDFLAGS /usr/lib/libopenblas.so.0
ENV PYTHONIOENCODING UTF-8


# ==================================================================
# python and pip
# ------------------------------------------------------------------
RUN apt-get update
RUN apt-get install python3-pip -y
RUN apt-get install python3-setuptools -y
RUN ln -s /usr/bin/pip3 /usr/bin/pip
#RUN rm /usr/bin/python
RUN ln -s /usr/bin/python3.6 /usr/bin/python

RUN python -m pip install -U pip

RUN PIP_INSTALL="pip --no-cache-dir install --upgrade" && \
$PIP_INSTALL setuptools

RUN PIP_INSTALL="pip --no-cache-dir install --upgrade" && \
$PIP_INSTALL cryptography

# ==================================================================
# base packages
# ------------------------------------------------------------------
RUN PIP_INSTALL="pip --no-cache-dir install --upgrade" && \
$PIP_INSTALL \
        numpy \
        scipy \
        Cython \
        psutil

# ==================================================================
# demo_app pacckage
# ------------------------------------------------------------------
RUN PIP_INSTALL="pip --no-cache-dir install --upgrade" && \
$PIP_INSTALL \
        pdf2img \
        Cython \
        pymongo \
        psutil \
        fitz \
        pdf2image \
        imgaug \
        pyclipper \
        unidecode \
        pyxdameraulevenshtein \
        flask_cors \
        Multi-Template-Matching \
        -U PyMuPDF \
        scikit-learn \
        paddlepaddle==2.0.0 \
        tqdm \
        torch==1.5.0 \
        torchvision==0.6.0 \
        torchsummary


# ==================================================================
# fix bug of PyMuPDF: uninstall and install again :(
# ------------------------------------------------------------------

RUN pip uninstall -y PyMuPDF
RUN PIP_INSTALL="pip --no-cache-dir install --upgrade" && \
$PIP_INSTALL \
        -U PyMuPDF

# ==================================================================
# opencv
# ------------------------------------------------------------------
RUN PIP_INSTALL="pip --no-cache-dir install --upgrade" && \
$PIP_INSTALL \
        opencv-contrib-python==4.5.1.48

RUN apt-get update
RUN apt-get install ffmpeg libsm6 libxext6 -y

# ==================================================================
# FLASK lib
# ------------------------------------------------------------------
RUN PIP_INSTALL="pip --no-cache-dir install --upgrade" && \
PIP_INSTALL="pip --no-cache-dir install --upgrade" && \
    $PIP_INSTALL bcrypt \
    Flask \
    Flask-Cors \
    gunicorn

# ==================================================================
# config & cleanup
# ------------------------------------------------------------------
RUN ldconfig && \
    apt-get clean && \
    apt-get autoremove && \
    rm -rf /var/lib/apt/lists/* /tmp/* ~/*

# ==================================================================
# Add source code
# ------------------------------------------------------------------

RUN mkdir -p /workspace
ADD . /workspace
WORKDIR /workspace

RUN chmod 777 -R /workspace
RUN chmod 777 -R /workspace/demo_app/log/
RUN chmod +x /workspace/run_server.sh
RUN mkdir -p /workspace/demo_app/app/tmp
RUN mkdir -p /workspace/log
RUN rm -rf /workspace/Dockerfile

WORKDIR /workspace

RUN pip install -e .

#------------------------------------------------------
# Compile python code
#-----------------------------------------------------
#RUN python compile.py build_ext --inplace
#RUN rm -rf /workspace/.git

# ==================================================================
# remove redundancy file
# ------------------------------------------------------------------
#RUN cd /workspace && rm *.c
#WORKDIR /workspace


EXPOSE 80
VOLUME ["/workspace"]
#ENTRYPOINT [ "./run_server.sh" ]
